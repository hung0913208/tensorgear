from tensorgear import Convert

import numpy as np
import cv2


class AlterSpeed(Convert):
    def __init__(self, name, minimize_speed, maximize_speed):
        super(AlterSpeed, self).__init__(name)
        self._minimize_speed = minimize_speed
        self._maximize_speed = maximize_speed

    def convert(self, resource):
        rate, signals, label = resource
        speed_rate = np.random.uniform(self._minimize_speed, 
                                       self._maximize_speed)
        signals = cv2.resize(signals, (1, int(len(signals) * speed_rate))).squeeze()
        return rate, signals, label

    def define(self, minimize_speed, maximize_speed, **kwargs):
        self._minimize_speed = minimize_speed
        self._maximize_speed = maximize_speed
        return self
