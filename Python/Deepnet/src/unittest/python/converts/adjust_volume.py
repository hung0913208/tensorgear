from tensorgear import Convert
import numpy as np


class AdjustVolume(Convert):
    def __init__(self, name, minimize_volume, maximize_volume):
        super(AdjustVolume, self).__init__(name)
        self._minimize_volume = minimize_volume
        self._maximize_volume = maximize_volume

    def convert(self, resource):
        rate, signals, label = resource
        signals = signals*np.random.uniform(self._minimize_volume, 
                                            self._maximize_volume)
        return rate, signals, label

    def define(self, minimize_volume, maximize_volume, **kwargs):
        self._minimize_volume = minimize_volume
        self._maximize_volume = maximize_volume
        return self
