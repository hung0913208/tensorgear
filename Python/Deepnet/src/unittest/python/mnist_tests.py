from tensorgear import *
from tensorflow.examples.tutorials.mnist import input_data

import tensorflow as tf
import unittest

class Dataset(Compose):
    def __init__(self, root):
        super(Dataset, self).__init__('mnist')
        self._mnist = input_data.read_data_sets(root, one_hot=False)
        self._dataset = 'train'

    def use_dataset(self, name):
        self._dataset = name

    def compose(self):
        if self._dataset == 'train':
            return self._mnist.train.next_batch(1) 
        else:
            return self._mnist.test.next_batch(1)

class MNIST(unittest.TestCase):
    def test_with_rnn(self):
        def index2onehot(label):
            return to_categorical(label, num_classes=10)

        with Gear(backend='tensorflow'):
            estimator = Estimator('classification', name='mnist-rnn', optimizer='adam',
                                  provider=[Dataset('/tmp/data/'), Decode(decoders=[None, index2onehot])],
                                  loss_function=CategoricalCrossEntropy(ground_truth=Input(dtype=tf.float32, 
                                                                        name='label',
                                                                        shape=(10,),
                                                                        gate=1)),
                                  logits=[
                                      Input(dtype=tf.float32, shape=[None, 28], gate=0),
                                      RNN(num_units=200),
                                      Dense(units=10, name='label')
                                  ])
            estimator.fit(epochs=5, steps_per_epoch=100, batch_size=128,
                          checkpoint='./mnist_rnn', summaries_dir='./mnist_rnn',
                          validate_steps_per_epoch=100, validate_batch_size=128,
                          learning_rate=0.0001)
            estimator.freeze('mnist_rnn.pb')

    def test_with_cnn(self):
        def index2onehot(label):
            return to_categorical(label, num_classes=10)

        with Gear(backend='tensorflow'):
            estimator = Estimator('classification', name='mnist-cnn', optimizer='adam',
                                  provider=[Dataset('/tmp/data/'), Decode(decoders=[None, index2onehot])],
                                  loss_function=CategoricalCrossEntropy(ground_truth=Input(dtype=tf.float32, 
                                                                        name='label',
                                                                        shape=(10,),
                                                                        gate=1)),
                                  logits=[
                                      Input(dtype=tf.float32, shape=[28, 28, 1], gate=0),
                                      Conv2D(filters=64, kernels=[8, 8]),
                                      Flatten(),
                                      Dense(units=10, name='label')
                                  ])
            estimator.fit(epochs=10, steps_per_epoch=100, batch_size=128,
                          checkpoint='./mnist_cnn', summaries_dir='./mnist_cnn',
                          validate_steps_per_epoch=100, validate_batch_size=128,
                          learning_rate=0.0001)

if __name__ == '__main__':
    unittest.main(verbosity=2)
