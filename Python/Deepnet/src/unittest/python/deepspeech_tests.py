from tensorgear import *
from decoders.mfcc import Mfcc
from decoders.spectrogram import Spectrogram
from composers.speech_command import SpeechCommand
from composers.vivos_dataset import VIVOSDataset
import tensorflow as tf
import numpy as np
import unittest


class TextFeaturizer(object):
    def __init__(self, vocal_list):
        self.token_to_index = {}
        self.index_to_token = {}
        self.count = len(vocal_list)

        for index, token in enumerate(vocal_list):
            self.token_to_index[token] = index
            self.index_to_token[index] = token

class Deepspeech(unittest.TestCase):
    def test_simple_rnn_layers(self):
        progress = Progress()
        fetcher = SpeechCommand(progress=progress,
                                labels=['one', 'two', 'three', 'four', 'five', 'six'])

        def index_to_recognize(label):
            return to_categorical(fetcher.labels.index(label) + 1,
                                  num_classes=len(fetcher.labels) + 1)

        with Gear('tensorflow'):
            layer = Input(shape=[None, 40], dtype=tf.float32)
            layer = LSTM(num_units=100)(layer)
            logits = Dense(units=len(fetcher.labels) + 1, name='number')(layer)

            estimator = Estimator('classification', name='simple_rnn',
                                  provider=[
                                      fetcher,
                                      Mfcc(session=Backend.session()),
                                      Decode([None, index_to_recognize])
                                  ],
                                  logits=logits,
                                  loss_function=CategoricalCrossEntropy(
                                      ground_truth=Input(name='number', dtype=tf.float32,
                                                         shape=[1 + len(fetcher.labels)])),
                                  optimizer='adam')
            # estimator.summary.visualize('debug', 'localhost:6064')
            estimator.summary.visualize('tracking', Histogram(estimator))
            estimator.fit(epoch=10, steps_per_epoch=100, batch_size=128,
                          validate_steps_per_epoch=100,
                          checkpoint='./simple_rnn',
                          summaries_dir='./simple_rnn',
                          learning_rate=0.0001)

    def test_simple_cnn_rnn_layers(self):
        progress = Progress()
        fetcher = SpeechCommand(progress=progress,
                                labels=['one', 'two', 'three', 'four', 'five', 'six'])

        def index_to_recognize(label):
            return to_categorical(fetcher.labels.index(label) + 1,
                                  num_classes=len(fetcher.labels) + 1)

        with Gear('tensorflow'):
            layer = Input(shape=[None, 40, 1], dtype=tf.float32)
            layer = Conv2D(64, kernels=(20, 8))(layer)
            layer = BatchNorm(momentum=0.997, epsilon=1e-5, fused=True)(layer)
            layer = Dropout()(layer)
            layer = MaxPool2D()(layer)
            layer = Conv2D(64, kernels=(10, 4))(layer)
            layer = Dropout()(layer)
            layer = BatchNorm(momentum=0.997, epsilon=1e-5, fused=True)(layer)
            layer = Reshape(shape=[-1, 20*64])(layer)
            layer = LSTM(num_units=100)(layer)
            logits = Dense(units=len(fetcher.labels) + 1, name='number')(layer)

            estimator = Estimator('classification', name='simple_cnn_rnn',
                                  provider=[
                                      fetcher,
                                      Mfcc(session=Backend.session()),
                                      Decode([None, index_to_recognize])
                                  ],
                                  logits=logits,
                                  loss_function=CategoricalCrossEntropy(
                                      ground_truth=Input(name='number', dtype=tf.float32,
                                                         shape=[1 + len(fetcher.labels)])),
                                  optimizer='adam')
            # estimator.summary.visualize('debug', 'localhost:6064')
            estimator.summary.visualize('tracking', Histogram(estimator))
            estimator.fit(epoch=10, steps_per_epoch=100, batch_size=128,
                          validate_steps_per_epoch=100,
                          checkpoint='./simple_cnn_rnn',
                          summaries_dir='./simple_cnn_rnn',
                          learning_rate=0.0001)

    def test_single_words_with_ctc(self):
        progress = Progress()
        fetcher = VIVOSDataset(progress=progress)
        lang = TextFeaturizer(fetcher.characters())

        def generate_token_index(label):
            return [lang.token_to_index[token] for token in list(label.strip().lower())]

        with Gear('tensorflow'):
            inputs = Input(shape=[None, 257, 1], dtype=tf.float32, name='features')
            layer = Pad(paddings=[[0, 0], [20, 20], [5, 5], [0, 0]])(inputs)
            layer = Conv2D(32, kernels=(41, 11), strides=(2, 2), padding='valid',
                           activation='relu6', bias_initializer=None)(layer)
            layer = BatchNorm(momentum=0.997, epsilon=1e-5, fused=True)(layer)
            layer = Pad(paddings=[[0, 0], [10, 10], [5, 5], [0, 0]])(layer)
            layer = Conv2D(32, kernels=(21, 11), strides=(2, 1), padding='valid',
                           activation='relu6', bias_initializer=None)(layer)
            layer = BatchNorm(momentum=0.997, epsilon=1e-5, fused=True)(layer)
            layer = Reshape(shape=[-1, 32*129])(layer)

            for layer_counter in range(1):
                # @NOTE: implement rnn layer with specific a batch normalize

                layer = GRU(800, select_outputs=None,
                            adapter='bidirection')(layer)
                layer = Add()(layer)
                layer = BatchNorm(momentum=0.997, epsilon=1e-5, fused=True)(layer)
            else:
                logits = Dense(lang.count, activation='softmax', name='sentence')(layer)

            loss_function = CTCLoss(ground_truth=Input(name='sentence', dtype=tf.int64,
                                                       shape=[None]))
            estimator = Estimator('sequence', name='simple_rnn_with_ctc',
                                  provider=[
                                      fetcher,
                                      Spectrogram(session=Backend.session()),
                                      Decode([None, generate_token_index])
                                  ],
                                  loss_function=loss_function,
                                  logits=logits,
                                  optimizer='adam')
            #estimator.summary.visualize('debug', 'localhost:6064')
            estimator.fit(epoch=10, steps_per_epoch=1000, batch_size=32,
                          validate_steps_per_epoch=1,
                          checkpoint='./deepspeech_conv2d_rnn',
                          summaries_dir='./deepspeech_conv2d_rnn',
                          learning_rate=0.0001, hooks={'train': [loss_function._debug]})

    def test_listen_speech_attention(self):
        pass

if __name__ == '__main__':
    unittest.main(verbosity=2)
