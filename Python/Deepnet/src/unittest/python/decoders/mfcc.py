from tensorflow.contrib.framework.python.ops import audio_ops as contrib_audio
from tensorgear import *

import tensorflow as tf
import numpy as np


class Mfcc(Convert):
    def __init__(self, session, desired_samples=None,
                 window_size_samples=480, window_stride_samples=160,
                 fingerprint_width=40):
        super(Mfcc, self).__init__('mfcc')
        self._wav_data, self._fetch = Mfcc.create_mfcc_layer(window_size_samples, window_stride_samples,
                                                             fingerprint_width, desired_samples)
        self._session = session
        self._window_stride_samples = window_stride_samples
        self._window_size_samples = window_size_samples
        self._fingerprint_width = fingerprint_width
        self._desired_samples = desired_samples

    def shape(self):
        length_minus_window = (self._desired_samples - self._window_size_samples)
        if length_minus_window < 0:
            spectrogram_length = 0
        else:
            spectrogram_length = 1 + int(length_minus_window/self._window_stride_samples)
        return [(self._fingerprint_width * spectrogram_length,), 1]

    def freeze(self, previous):
        _, calc_mfcc = Mfcc.create_mfcc_layer(self._window_size_samples,
                                              self._window_stride_samples,
                                              self._fingerprint_width,
                                              self._desired_samples)
        return tf.reshape(calc_mfcc, shape=(-1, 98, 40, 1))

    @staticmethod
    def create_mfcc_layer(window_size_samples, window_stride_samples,
                          fingerprint_width, desired_samples=None):
        # @NOTE: please notice that we usually provide dataset with wav files when
        # training is On, so we must use different placeholder here to working with
        # this

        if desired_samples is None:
            placeholders = tf.placeholder(tf.string, [], name='wav_data')
            decoded_sample_data = contrib_audio.decode_wav(placeholders,
                                                           desired_channels=1,
                                                           name='decoded_sample_data')
        else:
            placeholders = tf.placeholder(tf.string, [], name='wav_data')
            decoded_sample_data = contrib_audio.decode_wav(placeholders,
                                                           desired_channels=1,
                                                           desired_samples=desired_samples,
                                                           name='decoded_sample_data')

        spectrogram = contrib_audio.audio_spectrogram(decoded_sample_data.audio,
                                                      window_size=window_size_samples,
                                                      stride=window_stride_samples,
                                                      magnitude_squared=True,
                                                      name='audio_spectrogram')
        fingerprint_input = contrib_audio.mfcc(spectrogram, decoded_sample_data.sample_rate,
                                               dct_coefficient_count=fingerprint_width)

        if desired_samples is None:
            # @NOTE: since we don't want to limit the size of input, we will calculate
            # everything at the time when placeholder is filled

            fingerprint_input_shape = tf.shape(fingerprint_input, name='get_shape')
            fingerprint_size = tf.multiply(
                fingerprint_input_shape[1], fingerprint_input_shape[2], name='mult')
        else:
            # @NOTE: calculate spectrogram_length

            length_minus_window = (desired_samples - window_size_samples)
            if length_minus_window < 0:
                spectrogram_length = 0
            else:
                spectrogram_length = 1 + int(length_minus_window / window_stride_samples)

            fingerprint_size = fingerprint_width * spectrogram_length

        # @NOTE: now everything is okey to deliver we can produce our inputs and outputs
        return placeholders, tf.reshape(fingerprint_input, [fingerprint_size],
                                        name='fingerprint')

    def convert(self, resource):
        import math

        if len(resource) != 2:
            raise AssertionError('resource must contain exactly 2 parameters')

        wave_data, index = resource

        while True:
            try:
                mfcc = self._session.run(self._fetch,
                                         feed_dict={ self._wav_data: wave_data })
                return mfcc, index
            except RuntimeError:
                self._session = Backend.session()
                self._wav_data, self._fetch = Mfcc.create_mfcc_layer(self._window_size_samples,
                                                                     self._window_stride_samples,
                                                                     self._fingerprint_width,
                                                                     self._desired_samples)

