from tensorflow.contrib.framework.python.ops import audio_ops as contrib_audio
from tensorgear import *

import tensorflow as tf
import numpy as np

class Spectrogram(Convert):
    def __init__(self, session, desired_samples=None,
                 window_size_samples=400, window_stride_samples=160):
        super(Spectrogram, self).__init__('spectrogram')
        self._wav_data, self._fetch = \
                Spectrogram.create_spectrogram_layer(window_size_samples, 
                                                     window_stride_samples,
                                                     desired_samples)
        self._session = session
        self._window_stride_samples = window_stride_samples
        self._window_size_samples = window_size_samples
        self._desired_samples = desired_samples

    def shape(self):
        return None

    def freeze(self, previous):
        _, calc_spectrogram = \
                Spectrogram.create_spectrogram_layer(self._window_size_samples, 
                                                     self._window_stride_samples,
                                                     self._desired_samples)
        return tf.reshape(calc_spectrogram, shape=(-1, 98, 257, 1))

    @staticmethod
    def create_spectrogram_layer(window_size_samples, window_stride_samples, desired_samples):
        # @NOTE: please notice that we usually provide dataset with wav files when
        # training is On, so we must use different placeholder here to working with
        # this

        placeholders = tf.placeholder(tf.string, [], name='wav_data')

        if desired_samples is None:
            decoded_sample_data = contrib_audio.decode_wav(placeholders,
                                                           desired_channels=1,
                                                           desired_samples=desired_samples,
                                                           name='decoded_sample_data')
        else:
            decoded_sample_data = contrib_audio.decode_wav(placeholders,
                                                           desired_channels=1,
                                                           desired_samples=desired_samples,
                                                           name='decoded_sample_data')
        spectrogram = contrib_audio.audio_spectrogram(decoded_sample_data.audio,
                                                      window_size=window_size_samples,
                                                      stride=window_stride_samples,
                                                      magnitude_squared=True,
                                                      name='audio_spectrogram')
        # @NOTE: now everything is okey to deliver we can produce our inputs and outputs
        return placeholders, tf.reshape(spectrogram, [-1], name='fingerprint')

    def convert(self, resource):
        import math

        if len(resource) != 2:
            raise AssertionError('resource must contain exactly 2 parameters')

        wave_data, index = resource

        while True:
            try:
                spectrogram = self._session.run(self._fetch, 
                                                feed_dict={ self._wav_data: wave_data })
                return spectrogram, index
            except RuntimeError:
                self._session = Backend.session()
                self._wav_data, self._fetch = \
                        Spectrogram.create_spectrogram_layer(self._window_size_samples, 
                                                             self._window_stride_samples,
                                                             self._desired_samples)

