from tensorgear import *
from composers.dek_dataset import DekAudioDataset
from composers.speech_command import SpeechCommand
from converts.adjust_volume import AdjustVolume
from converts.alter_speech import AlterSpeed
from decoders.spectrogram import Spectrogram
from decoders.mfcc import Mfcc

import tensorflow as tf
import numpy as np
import unittest
import os


LOCAL_DIR = 'dataset'
ACCOUNT = 'testing'
NODE = 'tensorflow'

class KeyworkSpotting(unittest.TestCase):
    def test_with_mfcc_legal_dataset(self):
        def learning_rate(step):
            if step < 400:
                return 0.0001
            else:
                return 0.00001

        def index_to_recognize(label):
            return fetcher.labels.index(label) + 2

        with Gear(backend='tensorflow'):
            progress = Progress()
            fetcher = SpeechCommand(progress=progress,
                                    labels=['yes', 'no', 'up', 'down',
                                            'left', 'right', 'on', 'off',
                                            'stop', 'go'])


            # @NOTE: everything done, create a new estimator to working with our
            # resource that would be defined above
            estimator = Estimator('classification', name='speech_command', optimizer='adam',
                                  provider=[
                                        fetcher,
                                        Mfcc(session=Backend.session(), desired_samples=16000),
                                        Decode([None, [index_to_recognize]])
                                  ],
                                  loss_function=SpareSoftmax(ground_truth=Input(dtype=tf.int64, gate=1,
                                                                                name='recognize_classifer')),
                                  logits=[
                                        Input(name='mfcc', gate=0, dtype=tf.float32, shape=[98, 40, 1]),
                                        Conv2D(filters=64, kernels=[20, 8],
                                               kernel_initializer={'name': 'truncated_normal', 'stddev': 0.01},
                                               bias_initializer='zeros'),
                                        Dropout(),
                                        MaxPool2D(),
                                        Conv2D(filters=64, kernels=[10, 4],
                                               kernel_initializer={'name': 'truncated_normal', 'stddev': 0.01},
                                               bias_initializer='zeros'),
                                        Dropout(),
                                        Flatten(),
                                        Dense(name='recognize_classifer',
                                              units=len(fetcher.labels) + 2, bias_initializer='zeros',
                                              kernel_initializer={'name': 'truncated_normal', 'stddev': 0.01})
                                  ])
            estimator.summary.visualize('verify', confusion_matrix([fetcher.labels],
                                                                   tensor_name='recognize',
                                                                   index=0))
            estimator.finetune([{
                'title': 'train recognize',
                'epochs': 18,
                'learning_rate': learning_rate,
                'steps_per_epoch': 100,
                'batch_size': 100,
                'summaries_dir': 'google',
                'validate_steps_per_epoch': 100,
                'validate_batch_size': 32,
            }])

            estimator.freeze('google.pb', export_names={'recognize_classifer': 'labels_softmax'})

            # @NOTE: export label's files
            mapping = [
                {'subset': 'recognize', 'labels': fetcher.labels}
            ]

            fetcher.upload_artifact(mapping, 
                                    username=ACCOUNT, node=NODE, model='google',
                                    artifact='%s/%s.pb' % (os.getcwd(), ACCOUNT))

    def test_with_mfcc_customer_dataset(self):
        def learning_rate(step):
            if step < 400:
                return 0.0001
            else:
                return 0.00001

        def index_to_recognize(label):
            return fetcher.labels.index(label) + 2

        with Gear(backend='tensorflow'):
            progress = Progress()
            fetcher = DekAudioDataset(ACCOUNT, progress, NODE)

            # @NOTE: everything done, create a new estimator to working with our
            # resource that would be defined above
            estimator = Estimator('classification', name='speech_command_custom', optimizer='adam',
                                  provider=[
                                        fetcher,
                                        Mfcc(session=Backend.session(), desired_samples=16000),
                                        Decode(decoders=[None, [index_to_recognize]])
                                  ],
                                  loss_function=SpareSoftmax(ground_truth=Input(dtype=tf.int64, gate=1,
                                                                                name='recognize_classifer')),
                                  logits=[
                                        Input(name='mfcc', gate=0, dtype=tf.float32, shape=[98, 40, 1]),
                                        Conv2D(filters=64, kernels=[20, 8],
                                               kernel_initializer={'name': 'truncated_normal', 'stddev': 0.01},
                                               bias_initializer='zeros'),
                                        Dropout(),
                                        MaxPool2D(),
                                        Conv2D(filters=64, kernels=[10, 4],
                                               kernel_initializer={'name': 'truncated_normal', 'stddev': 0.01},
                                               bias_initializer='zeros'),
                                        Dropout(),
                                        Flatten(),
                                        Dense(name='recognize_classifer',
                                              units=len(fetcher.labels) + 2, bias_initializer='zeros',
                                              kernel_initializer={'name': 'truncated_normal', 'stddev': 0.01})
                                  ])
            estimator.summary.visualize('verify', confusion_matrix([fetcher.labels],
                                                                   tensor_name='recognize',
                                                                   index=0))
            estimator.finetune([{
                'title': 'train recognize',
                'epochs': 4,
                'learning_rate': learning_rate,
                'steps_per_epoch': 100,
                'batch_size': 16,
                'summaries_dir': 'google',
                'validate_steps_per_epoch': 100,
                'validate_batch_size': 32,
            }])
            estimator.freeze('google.pb', export_names={'recognize_classifer': 'labels_softmax'})

            # @NOTE: export label's files
            mapping = [
                {'subset': 'recognize', 'labels': fetcher.labels}
            ]

            fetcher.upload_artifact(mapping,
                                    model='google',
                                    artifact='%s/%s.pb' % (os.getcwd(), ACCOUNT))

    def test_with_mfcc_customer_dataset_on_local(self):
        def learning_rate(step):
            if step < 400:
                return 0.0001
            else:
                return 0.00001

        def index_to_recognize(label):
            return fetcher.labels.index(label) + 2

        with Gear(backend='tensorflow'):
            progress = Progress()
            fetcher = SpeechCommand(progress, local_dir=LOCAL_DIR)

            # @NOTE: everything done, create a new estimator to working with our
            # resource that would be defined above
            estimator = Estimator('classification', name='speech_command_custom', optimizer='adam',
                                  provider=[
                                        fetcher,
                                        Mfcc(session=Backend.session(), desired_samples=16000),
                                        Decode(decoders=[None, [index_to_recognize]])
                                  ],
                                  loss_function=SpareSoftmax(ground_truth=Input(dtype=tf.int64, gate=1,
                                                                                name='recognize_classifer')),
                                  logits=[
                                        Input(name='mfcc', gate=0, dtype=tf.float32, shape=[98, 40, 1]),
                                        Conv2D(filters=64, kernels=[20, 8],
                                               kernel_initializer={'name': 'truncated_normal', 'stddev': 0.01},
                                               bias_initializer='zeros'),
                                        Dropout(),
                                        MaxPool2D(),
                                        Conv2D(filters=64, kernels=[10, 4],
                                               kernel_initializer={'name': 'truncated_normal', 'stddev': 0.01},
                                               bias_initializer='zeros'),
                                        Dropout(),
                                        Flatten(),
                                        Dense(name='recognize_classifer',
                                              units=len(fetcher.labels) + 2, bias_initializer='zeros',
                                              kernel_initializer={'name': 'truncated_normal', 'stddev': 0.01})
                                  ])
            estimator.summary.visualize('verify', confusion_matrix([fetcher.labels],
                                                                   tensor_name='recognize',
                                                                   index=0))
            estimator.finetune([{
                'title': 'train recognize',
                'epochs': 18,
                'learning_rate': learning_rate,
                'steps_per_epoch': 100,
                'batch_size': 16,
                'summaries_dir': 'google',
                'validate_steps_per_epoch': 100,
                'validate_batch_size': 32,
            }])
            estimator.freeze('google.pb', export_names={'recognize_classifer': 'labels_softmax'})

            # @NOTE: export label's files
            mapping = [
                {'subset': 'recognize', 'labels': fetcher.labels}
            ]

            fetcher.upload_artifact(mapping,
                                    username=ACCOUNT, node=NODE, model='google',
                                    artifact='%s/%s.pb' % (os.getcwd(), ACCOUNT))

    def test_with_mfcc_finetune_multigate(self):
        def learning_rate(step):
            if step < 400:
                return 0.0001
            else:
                return 0.00001

        def index_to_recognize(label):
            return fetcher.labels.index(label) + 2

        with Gear(backend='tensorflow'):
            progress = Progress()
            fetcher = DekAudioDataset(username=ACCOUNT, node=NODE, progress=progress)

            config_of_recognize_dataset = fetcher.get_dataset_config(subsets='recognize')
            if 'count_samples' in config_of_recognize_dataset:
                labels_of_recognize = list(config_of_recognize_dataset['count_samples'].keys())
            else:
                labels_of_recognize = config_of_recognize_dataset['labels']

            config_of_command_dataset = fetcher.get_dataset_config(subsets='command')
            if 'count_samples' in config_of_command_dataset:
                labels_of_command = list(config_of_command_dataset['count_samples'].keys())
            else:
                labels_of_command = config_of_command_dataset['labels']

            config_of_contact_dataset = fetcher.get_dataset_config(subsets='contact')
            if 'count_samples' in config_of_contact_dataset:
                labels_of_contact = list(config_of_contact_dataset['count_samples'].keys())
            else:
                labels_of_contact = config_of_contact_dataset['labels']

            def index_to_recognize(label):
                return fetcher.labels.index(label) + 2

            def index_to_contact(label):
                if label in labels_of_contact:
                    return labels_of_contact.index(label) + 2
                else:
                    return 1

            def index_to_command(label):
                if label in labels_of_command:
                    return labels_of_command.index(label) +     2
                else:
                    return 1

            # @NOTE: everything done, create a new estimator to working with our
            # resource that would be defined above
            estimator = Estimator('classification', name='speech_command_custom', optimizer='adam',
                                  provider=[
                                        fetcher,
                                        Mfcc(session=Backend.session(), desired_samples=16000),
                                        Decode(decoders=[None, [index_to_recognize, index_to_command, index_to_contact]])
                                  ],
                                  loss_function=[
                                    Pack([SpareSoftmax(ground_truth=Input(dtype=tf.int64, gate=1, name='recognize_classifer'))],
                                         [SpareSoftmax(ground_truth=Input(dtype=tf.int64, gate=2, name='command_classifer'))],
                                         [SpareSoftmax(ground_truth=Input(dtype=tf.int64, gate=3, name='contact_classifer'))]),
                                  ],
                                  logits=[
                                        Input(name='mfcc', gate=0, dtype=tf.float32, shape=[98, 40, 1]),
                                        Conv2D(filters=64, kernels=[20, 8],
                                               kernel_initializer={'name': 'truncated_normal', 'stddev': 0.01},
                                               bias_initializer='zeros',
                                               name='conv2d_1'),
                                        Dropout(dropout_prob=0.5),
                                        MaxPool2D(),
                                        Conv2D(filters=64, kernels=[10, 4],
                                               kernel_initializer={'name': 'truncated_normal', 'stddev': 0.01},
                                               bias_initializer='zeros',
                                               name='conv2d_2'),
                                        Dropout(dropout_prob=0.5),
                                        Flatten(),
                                        Share(
                                            [Dense(name='recognize_classifer',
                                                  units=len(fetcher.labels) + 2, bias_initializer='zeros',
                                                  kernel_initializer={'name': 'truncated_normal', 'stddev': 0.01})],
                                            [Dense(name='command_classifer',
                                                  units=len(labels_of_command) + 2, bias_initializer='zeros',
                                                  kernel_initializer={'name': 'truncated_normal', 'stddev': 0.01})],
                                            [Dense(name='contact_classifer',
                                                  units=len(labels_of_contact) + 2, bias_initializer='zeros',
                                                  kernel_initializer={'name': 'truncated_normal', 'stddev': 0.01})]
                                        )
                                  ])

            estimator.finetune([{
                'title': 'train recognize',
                'epochs': 4,
                'learning_rate': learning_rate,
                'steps_per_epoch': 100,
                'batch_size': 16,
                'summaries_dir': 'google',
                'validate_steps_per_epoch': 100,
                'validate_batch_size': 32,
                'freezed_layers': [
                    'command_classifer', 'contact_classifer'
                ]
            },
            {
                'title': 'command recognize',
                'epochs': 4,
                'learning_rate': learning_rate,
                'steps_per_epoch': 100,
                'batch_size': 16,
                'summaries_dir': 'google',
                'validate_steps_per_epoch': 100,
                'validate_batch_size': 32,
                'freezed_layers': [ 
                    'conv2d_1', 'conv2d_2', 'recognize_classifer',
                    'contact_classifer'
                ]
            },
            {
                'title': 'contact recognize',
                'epochs': 4,
                'learning_rate': learning_rate,
                'steps_per_epoch': 100,
                'batch_size': 16,
                'summaries_dir': 'google',
                'validate_steps_per_epoch': 100,
                'validate_batch_size': 32,
                'freezed_layers': [ 
                    'conv2d_1', 'conv2d_2', 'recognize_classifer', 
                    'command_classifer' 
                ]
            }])
            estimator.freeze('%s.pb' % ACCOUNT,
                             export_names={
                                'recognize_classifer': 'labels_softmax',
                                'command_classifer': 'command',
                                'contact_classifer': 'contact'
                            })
            
            # @NOTE: export label's files
            mapping = [
                {'subset': 'recognize', 'labels': fetcher.labels},
                {'subset': 'command', 'labels': labels_of_command},
                {'subset': 'contact', 'labels': labels_of_contact},
            ]

            fetcher.upload_artifact(mapping,
                                    model='google',
                                    artifact='%s/%s.pb' % (os.getcwd(), ACCOUNT))

    def test_with_mfcc_modify_multigate(self):
        def learning_rate(step):
            if step < 400:
                return 0.0001
            else:
                return 0.00001

        def index_to_recognize(label):
            return fetcher.labels.index(label) + 2

        with Gear(backend='tensorflow'):
            progress = Progress()
            fetcher = DekAudioDataset(username=ACCOUNT, node=NODE, progress=progress)

            config_of_recognize_dataset = fetcher.get_dataset_config(subsets='recognize')
            if 'count_samples' in config_of_recognize_dataset:
                labels_of_recognize = list(config_of_recognize_dataset['count_samples'].keys())
            else:
                labels_of_recognize = config_of_recognize_dataset['labels']

            config_of_command_dataset = fetcher.get_dataset_config(subsets='command')
            if 'count_samples' in config_of_command_dataset:
                labels_of_command = list(config_of_command_dataset['count_samples'].keys())
            else:
                labels_of_command = config_of_command_dataset['labels']

            config_of_contact_dataset = fetcher.get_dataset_config(subsets='contact')
            if 'count_samples' in config_of_contact_dataset:
                labels_of_contact = list(config_of_contact_dataset['count_samples'].keys())
            else:
                labels_of_contact = config_of_contact_dataset['labels']

            def index_to_recognize(label):
                return fetcher.labels.index(label) + 2

            def index_to_contact(label):
                if label in labels_of_contact:
                    return labels_of_contact.index(label) + 2
                else:
                    return 1

            def index_to_command(label):
                if label in labels_of_command:
                    return labels_of_command.index(label) +     2
                else:
                    return 1

            # @NOTE: everything done, create a new estimator to working with our
            # resource that would be defined above
            estimator = Estimator('classification', name='speech_command_custom', optimizer='adam',
                                  provider=[
                                        fetcher,
                                        Mfcc(session=Backend.session(), desired_samples=16000),
                                        Decode(decoders=[None, [index_to_recognize, index_to_command, index_to_contact]])
                                  ],
                                  loss_function=SpareSoftmax(ground_truth=Input(dtype=tf.int64, gate=1, name='recognize_classifer')),
                                  logits=[
                                        Input(name='mfcc', gate=0, dtype=tf.float32, shape=[98, 40, 1]),
                                        Conv2D(filters=64, kernels=[20, 8],
                                               kernel_initializer={'name': 'truncated_normal', 'stddev': 0.01},
                                               bias_initializer='zeros',
                                               name='conv2d_1'),
                                        Dropout(dropout_prob=0.5),
                                        MaxPool2D(),
                                        Conv2D(filters=64, kernels=[10, 4],
                                               kernel_initializer={'name': 'truncated_normal', 'stddev': 0.01},
                                               bias_initializer='zeros',
                                               name='conv2d_2'),
                                        Dropout(dropout_prob=0.5),
                                        Flatten(),
                                        Dense(name='recognize_classifer',
                                              units=len(fetcher.labels) + 2, bias_initializer='zeros',
                                              kernel_initializer={'name': 'truncated_normal', 'stddev': 0.01})
                                  ])

            estimator.finetune([{
                'epochs': 4,
                'learning_rate': learning_rate,
                'steps_per_epoch': 100,
                'batch_size': 16,
                'summaries_dir': 'google',
                'validate_steps_per_epoch': 100,
                'validate_batch_size': 32
            },
            {
                'epochs': 4,
                'action': 'modify',
                'learning_rate': learning_rate,
                'steps_per_epoch': 100,
                'batch_size': 16,
                'summaries_dir': 'google',
                'validate_steps_per_epoch': 100,
                'validate_batch_size': 32,
                'logits':[Share(
                  [Dense(name='command_classifer',
                         units=len(labels_of_command) + 2, bias_initializer='zeros',
                         kernel_initializer={'name': 'truncated_normal', 'stddev': 0.01})],
                  [Dense(name='contact_classifer',
                         units=len(labels_of_contact) + 2, bias_initializer='zeros',
                         kernel_initializer={'name': 'truncated_normal', 'stddev': 0.01})])],
                'loss_function': [
                  Pack([SpareSoftmax(ground_truth=Input(dtype=tf.int64, gate=2, name='command_classifer'))],
                       [SpareSoftmax(ground_truth=Input(dtype=tf.int64, gate=3, name='contact_classifer'))])
                ],
                'freezed_layers': [ 
                    'conv2d_1', 'conv2d_2', 'recognize_classifer',
                    'contact_classifer'
                ]
            },
            {
                'epochs': 4,
                'learning_rate': learning_rate,
                'steps_per_epoch': 100,
                'batch_size': 16,
                'summaries_dir': 'google',
                'validate_steps_per_epoch': 100,
                'validate_batch_size': 32,
                'freezed_layers': [ 
                    'conv2d_1', 'conv2d_2', 'recognize_classifer', 
                    'command_classifer' 
                ]
            }])
            estimator.freeze('%s.pb' % ACCOUNT,
                             export_names={
                                'recognize_classifer': 'labels_softmax',
                                'command_classifer': 'command',
                                'contact_classifer': 'contact'
                            })
            
            # @NOTE: export label's files
            mapping = [
                {'subset': 'recognize', 'labels': fetcher.labels},
                {'subset': 'command', 'labels': labels_of_command},
                {'subset': 'contact', 'labels': labels_of_contact},
            ]

            fetcher.upload_artifact(mapping,
                                    model='google',
                                    artifact='%s/%s.pb' % (os.getcwd(), ACCOUNT))

    def test_with_mfcc_multigate(self):
        logits = {
            'mfcc': {
                'class': 'input',
                    'parameters': {
                    'dtype': tf.float32,
                    'shape': [98, 40, 1],
                    'gate': 0
                }
            },

            # @NOTE: recognize branch
            'conv2d_0': {
                'class': 'conv2d',
                'parameters': {
                    'filters': 64,
                    'kernels': [20, 8],
                    'activation': 'relu',
                    'padding': 'same',
                    'kernel_initializer': 'xavier',
                    'bias_initializer':'zeros',
                    'name_of_variables':{
                        'kernel': 'weight_0',
                        'bias': 'bias_0'
                    }
                },
                'inputs': ['mfcc']
            },
            'dropout_0': {
                'class': 'dropout',
                'parameters':{ 'dropout_prob': 0.5 },
                'inputs': ['conv2d_0']
            },
            'maxpool_0': {
                'class': 'maxpool',
                'inputs': ['dropout_0']
            },
            'conv2d_1': {
                'class': 'conv2d',
                'parameters': {
                    'filters': 64,
                    'kernels': [10, 4],
                    'activation': 'relu',
                    'padding': 'same',
                    'kernel_initializer': 'xavier',
                    'bias_initializer':'zeros',
                    'name_of_variables':{
                        'kernel': 'weight_1',
                        'bias': 'bias_1'
                    }
                },
                'inputs': ['maxpool_0']
            },
            'dropout_1': {
                'class': 'dropout',
                'parameters':{ 'dropout_prob': 0.5 },
                'inputs': ['conv2d_1']
            },
            'flatten_0': {
                'class': 'flatten',
                'inputs': ['dropout_1']
            },

            # @NOTE: output gates
            'recognize_classifer': {
                'class': 'dense',
                'parameters': {
                    'units': 19,
                    'kernel_initializer': {'name': 'truncated_normal', 'stddev': 0.01},
                    'bias_initializer':'zeros'
                },
                'inputs': ['flatten_0'],
                'output': 0
            },
            'command_classifer': {
                'class': 'dense',
                'parameters': {
                    'units': 19,
                    'kernel_initializer': {'name': 'truncated_normal', 'stddev': 0.01},
                    'bias_initializer':'zeros'
                },
                'inputs': ['flatten_0'],
                'output': 1
            },
            'contact_classifer': {
                'class': 'dense',
                'parameters': {
                    'units': 19,
                    'kernel_initializer': {'name': 'truncated_normal', 'stddev': 0.01},
                    'bias_initializer':'zeros'
                },
                'inputs': ['flatten_0'],
                'output': 2
            }
        }

        loss_function = {
            'recognize_classifer': {
                'class': 'input',
                'parameters':{
                    'dtype': tf.int64,
                    'gate': 1
                }
            },
            'command_classifer': {
                'class': 'input',
                'parameters':{
                    'dtype': tf.int64,
                    'gate': 2
                }
            },
            'contact_classifer': {
                'class': 'input',
                'parameters':{
                    'dtype': tf.int64,
                    'gate': 3
                }
            },
            'cross_entropy_0': {
                'class': 'spare_softmax',
                'parameters': {
                    'epsilon': 0.0
                },
                'inputs': ['recognize_classifer'],
            },
            'cross_entropy_1': {
                'class': 'spare_softmax',
                'parameters': {
                    'epsilon': 0.0
                },
                'inputs': ['command_classifer'],
            },
            'cross_entropy_2': {
                'class': 'spare_softmax',
                'parameters': {
                    'epsilon': 0.0
                },
                'inputs': ['contact_classifer'],
            },
            'reduce_sum':{
                'class': 'reduce_sum',
                'inputs': ['cross_entropy_0', 'cross_entropy_1', 'cross_entropy_2',]
            }
        }

        with Gear(backend='tensorflow'):
            progress = Progress()
            fetcher = DekAudioDataset(username=ACCOUNT, node=NODE, progress=progress)

            config_of_recognize_dataset = fetcher.get_dataset_config(subsets='recognize')
            if 'count_samples' in config_of_recognize_dataset:
                labels_of_recognize = list(config_of_recognize_dataset['count_samples'].keys())
            else:
                labels_of_recognize = config_of_recognize_dataset['labels']

            config_of_command_dataset = fetcher.get_dataset_config(subsets='command')
            if 'count_samples' in config_of_command_dataset:
                labels_of_command = list(config_of_command_dataset['count_samples'].keys())
            else:
                labels_of_command = config_of_command_dataset['labels']

            config_of_contact_dataset = fetcher.get_dataset_config(subsets='contact')
            if 'count_samples' in config_of_contact_dataset:
                labels_of_contact = list(config_of_contact_dataset['count_samples'].keys())
            else:
                labels_of_contact = config_of_contact_dataset['labels']

            def index_to_recognize(label):
                return fetcher.labels.index(label) + 2

            def index_to_contact(label):
                if label in labels_of_contact:
                    return labels_of_contact.index(label) + 2
                else:
                    return 1

            def index_to_command(label):
                if label in labels_of_command:
                    return labels_of_command.index(label) +     2
                else:
                    return 1

            def learning_rate(step):
                if step < 300:
                    return 0.0001
                else:
                    return 0.00001

            # @NOTE: create a new provider
            provider = Iterator([
                fetcher,
                Mfcc(session=Backend.session(), desired_samples=16000),
                Decode(decoders=[None, [index_to_recognize, index_to_command, index_to_contact]])
            ])

            # @NOTE: since these Dense.units are changed according how many labels
            # of each dataset, we must redefine them each time
            logits['recognize_classifer']['parameters']['units'] = \
                2 + len(labels_of_command) + len(labels_of_contact)
            logits['command_classifer']['parameters']['units'] = \
                2 + len(labels_of_command)
            logits['contact_classifer']['parameters']['units'] = \
                2 + len(labels_of_contact)

            # @NOTE: everything done, create a new estimator to working with our
            # resource that would be defined above
            estimator = Estimator('classification', name=ACCOUNT, progress=progress,
                                  optimizer='adam', loss_function=loss_function,
                                  provider=provider, logits=logits)

            labels = [[], ['_unknown_'], ['_unknown_']]
            labels[0].extend(labels_of_recognize)
            labels[1].extend(labels_of_command)
            labels[2].extend(labels_of_contact)

            estimator.summary.visualize('verify', 
                confusion_matrix(labels, index=0, tensor_name='recognize'))
            estimator.summary.visualize('verify',
                confusion_matrix(labels, index=1, tensor_name='command'))
            estimator.summary.visualize('verify',
                confusion_matrix(labels, index=2, tensor_name='contact'))
            estimator.summary.visualize('tracking', Histogram(estimator))
            estimator.fit(epochs=5, steps_per_epoch=100, batch_size=128,
                          checkpoint='./%s' % ACCOUNT, summaries_dir='./%s' % ACCOUNT, 
                          validate_steps_per_epoch=100,
                          validate_batch_size=128,
                          learning_rate=learning_rate)
            estimator.freeze('%s.pb' % ACCOUNT,
                             export_names={
                                'recognize_classifer': 'labels_softmax',
                                'command_classifer': 'command',
                                'contact_classifer': 'contact'
                            })

            # @NOTE: export label's files
            mapping = [
                {'subset': 'recognize', 'labels': fetcher.labels},
                {'subset': 'command', 'labels': labels_of_command},
                {'subset': 'contact', 'labels': labels_of_contact},
            ]

            fetcher.upload_artifact(mapping,
                                    model='google',
                                    artifact='%s/%s.pb' % (os.getcwd(), ACCOUNT))

    def test_with_spectrogram_legal_dataset(self):
        def learning_rate(step):
            if step < 400:
                return 0.0001
            else:
                return 0.00001

        def index_to_recognize(label):
            return fetcher.labels.index(label) + 2

        with Gear(backend='tensorflow'):
            progress = Progress()
            fetcher = SpeechCommand(progress=progress,
                                    labels=['yes', 'no', 'up', 'down',
                                            'left', 'right', 'on', 'off',
                                            'stop', 'go'])


            # @NOTE: everything done, create a new estimator to working with our
            # resource that would be defined above
            estimator = Estimator('classification', name='speech_command_spectrogram', optimizer='adam',
                                  provider=[
                                        fetcher,
                                        Spectrogram(session=Backend.session(), desired_samples=16000),
                                        Decode(decoders=[None, [index_to_recognize]])
                                  ],
                                  loss_function=SpareSoftmax(ground_truth=Input(dtype=tf.int64, gate=1,
                                                                                name='recognize_classifer')),
                                  logits=[
                                        Input(name='spectrogram', gate=0, dtype=tf.float32, shape=[98, 257, 1]),
                                        Conv2D(filters=64, kernels=[20, 8],
                                               kernel_initializer={'name': 'truncated_normal', 'stddev': 0.01},
                                               bias_initializer='zeros'),
                                        Dropout(),
                                        MaxPool2D(),
                                        Conv2D(filters=64, kernels=[10, 4],
                                               kernel_initializer={'name': 'truncated_normal', 'stddev': 0.01},
                                               bias_initializer='zeros'),
                                        Dropout(),
                                        Flatten(),
                                        Dense(name='recognize_classifer',
                                              units=len(fetcher.labels) + 2, bias_initializer='zeros',
                                              kernel_initializer={'name': 'truncated_normal', 'stddev': 0.01})
                                  ])
            estimator.summary.visualize('verify', confusion_matrix([fetcher.labels],
                                                                   tensor_name='recognize',
                                                                   index=0))
            estimator.finetune([{
                'title': 'train recognize',
                'epochs': 8,
                'learning_rate': learning_rate,
                'steps_per_epoch': 100,
                'batch_size': 100,
                'summaries_dir': 'google',
                'validate_steps_per_epoch': 100,
                'validate_batch_size': 32,
            }])

            estimator.freeze('google.pb', export_names={'recognize_classifer': 'labels_softmax'})

            # @NOTE: export label's files
            mapping = [
                {'subset': 'recognize', 'labels': fetcher.labels}
            ]

            fetcher.upload_artifact(mapping, 
                                    username=ACCOUNT, node=NODE, model='google',
                                    artifact='%s/%s.pb' % (os.getcwd(), ACCOUNT))

if __name__ == '__main__':
    import sys

    if len(sys.argv) > 1:
        ACCOUNT = sys.argv.pop()
        NODE = sys.argv.pop()
    unittest.main(verbosity=2)
