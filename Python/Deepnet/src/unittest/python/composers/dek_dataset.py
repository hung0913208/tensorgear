from requests import get, post
from tensorgear import *

import shutil
import os


class DekAudioDataset(Compose):
    HOST = 'https://spt.dektech.com.au:8080'

    def __init__(self, username, progress=None, node=None):
        import requests
        import hashlib

        requests.packages.urllib3.disable_warnings()
        super(DekAudioDataset, self).__init__(username)

        m = hashlib.md5()
        m.update(username.encode('utf-8'))

        self._method = 'train'
        self._progress = progress
        self._node = node
        self._uuid = m.hexdigest()
        self._subsets = None
        self._sample_indexs = []
        self._max_samples = []
        self._labels = []
        self._index = 0

        if os.path.exists(self._uuid) is False:
            os.mkdir(self._uuid)

    @property
    def labels(self):
        if len(self._labels) == 0:
            import random

            config = self.get_dataset_config()

            if 'count_samples' in config and len(config['count_samples']) > 0:
                self._labels = list(config['count_samples'].keys())
            elif 'labels' in config:
                self._labels = config['labels']
            else:
                raise AssertionError('dataset is empty')


            self._max_samples = list(config['count_samples'].values())
            if self.get_file_from_dataset() is False:
                raise AssertionError('Fetching dataset %s/recognize fail' \
                                     % self._uuid)
            else:
                self._sample_indexs = \
                    [random.randint(0, int(3*self._max_samples[i]/4)) \
                     for i in range(len(self._labels))]
                self._subsets = 'recognize'

        return self._labels

    def shape(self):
        return [0, -1, 1]

    def use_dataset(self, name):
        self._method = name

    def compose(self):
        import random

        if len(self._labels) == 0:
            config = self.get_dataset_config()

            if 'count_samples' in config and len(config['count_samples']) > 0:
                self._labels = list(config['count_samples'].keys())
                self._max_samples = list(config['count_samples'].values())

                if self.get_file_from_dataset() is False:
                    raise AssertionError('Fetching dataset %s/recognize fail' \
                                         % self._uuid)
                else:
                    self._sample_indexs = \
                        [random.randint(0, int(3*self._max_samples[i]/4)) \
                            for i in range(len(self._labels))]
                    self._subsets = 'recognize'
            else:
                raise AssertionError('There are nothing inside dataset '
                                     '%s/recognize' % self._uuid)

        self._index = random.randint(0, len(self._labels) - 1)
        if self._method == 'train':
            self._sample_indexs[self._index] = random.randint(0, int(3*self._max_samples[self._index]/4) - 1)
        else:
            self._sample_indexs[self._index] = random.randint(int(3*self._max_samples[self._index]/4),
                                                              self._max_samples[self._index] - 1)

        wav_data = self.get_sample_from_dataset(label=self._labels[self._index],
                                                     index=self._sample_indexs[self._index],
                                                     subsets=self._subsets)
        return (wav_data, self._labels[self._index])

    @property
    def uuid(self):
        return self._uuid

    @property
    def node(self):
        return self._node

    def root(self, subsets='recognize'):
        where_to_save = self._uuid

        if not subsets is None:
            if isinstance(subsets, str):
                where_to_save = '%s/%s/' % (where_to_save, subsets)

                if os.path.exists(where_to_save) is False:
                    os.mkdir(where_to_save)
            elif isinstance(subsets, list) or isinstance(subsets, tuple):
                for subset in subsets:
                    where_to_save = '%s/%s/' % (where_to_save, subset)

                    if os.path.exists(where_to_save) is False:
                        os.mkdir(where_to_save)
            else:
                raise AssertionError('subset must be \'str\', \'list\' or \'tuple\'')
        return where_to_save

    def get_dataset_config(self, subsets='recognize'):
        if self._node is None:
            ret = get('%s/dataset_config?dataset=%s&subsets=%s' % \
                                (self.HOST, self._uuid, subsets), verify=False)
        else:
            ret = get('%s/dataset_config?dataset=%s&subsets=%s&node=%s' % \
                                (self.HOST, self._uuid, subsets, self._node), verify=False)

        if ret.status_code == 200:
            return ret.json()
        else:
            return None

    def get_num_sample_per_label(self, label, subsets='recognize'):
        subsets = '.'.join(subsets) if isinstance(subsets, list) else subsets

        if self._node is None:
            link = '%s/count_sample_of_label?uid=%s&subsets=%s&label=%s' \
                                 % (self.HOST, self._uuid, subsets, label)
        else:
            link = '%s/count_sample_of_label?uid=%s&subsets=%s&label=%s&node=%s' \
                                 % (self.HOST, self._uuid, subsets, label, self._node)

        ret = get(link, verify=False)
        if ret.status_code == 200:
            return int(ret.text)
        else:
            return None

    def get_list_file_of_dataset(self, label, begin=0, end=-1, subsets='recognize'):
        if end < 0:
            end = self.get_num_sample_per_label(label)
        if end is None:
            return None

        subsets = '.'.join(subsets) if isinstance(subsets, list) else subsets
        if self._node is None:
            link = '%s/list_inputs?uid=%s&subsets=%s&label=%s&begin=%d&end=%d' \
                                 % (self.HOST, self._uuid, subsets, label, begin, end)
        else:
            link = '%s/list_inputs?uid=%s&subsets=%s&label=%s&begin=%d&end=%d&node=%s' \
                                 % (self.HOST, self._uuid, subsets, label, begin, end, self._node)

        ret = get(link, verify=False)
        if ret.status_code == 200:
            return ret.text.split('\n')[:]
        else:
            return None

    def get_file_from_dataset(self, label=None, file=None, subsets='recognize',
                              tracker=None):
        if label is None:
            config = self.get_dataset_config(subsets=subsets)

            if config is None or (not 'count_samples' in config):
                return False

            if not self._progress is None:
                total_file = 0

                for count in config['count_samples'].values():
                    total_file += count
                else:
                    task = self._progress.progress(total_file)
                    self._progress.reset(task)
                    self._progress.prefix(prefix='Download dataset')
                    self._progress.bar(length=30)
                    self._progress.percent()
                    self._progress.time_track()
                    self._progress.suffix(suffix='Completed')
            else:
                task = None

            for label in config['count_samples']:
                ret = self.get_file_from_dataset(label, None,
                                                 subsets=subsets, tracker=task)

                if ret is False:
                    return False
            return True
        elif file is None:
            list_files = self.get_list_file_of_dataset(label, subsets=subsets)

            if list_files is None or len(list_files) == 0:
                print('list_files of %s error: %s' % (label, list_files))
                return False

            for file in list_files:
                ret = self.get_file_from_dataset(label, file, subsets)

                if ret is False:
                    print('fail at file %s/%s' % (label, file))
                    return False
                elif not tracker is None:
                    tracker.inc()
            return True
        else:
            # @NOTE: download okey, save data to dataset
            where_to_save = self.root(subsets)
            where_to_save = '%s/%s/' % (where_to_save, label)

            if os.path.exists(where_to_save) is False:
                os.mkdir(where_to_save)

            where_to_save = '%s/%s' % (where_to_save, file)
            if os.path.exists(where_to_save) is True:
                return True

            # @NOTE: not found this file, try to download from server
            subsets = '.'.join(subsets) if isinstance(subsets, list) else subsets
            if self._node is None:
                link = '%s/download?uid=%s&label=%s&filename=%s&subsets=%s' % \
                                    (self.HOST, self._uuid, label, file, subsets)
            else:
                link = '%s/download?uid=%s&label=%s&filename=%s&subsets=%s&node=%s' % \
                                    (self.HOST, self._uuid, label, file, subsets, self._node)

            ret = get(link, stream=True, verify=False)
            if ret.status_code == 200:
                with open(where_to_save, 'wb') as fp:
                    shutil.copyfileobj(ret.raw, fp)
                return True
            else:
                return False

    def get_filepath_from_dataset(self, label, index, subsets='recognize'):
        import glob

        where_to_save = self._uuid
        filepath = ''

        if not subsets is None:
            if isinstance(subsets, str):
                where_to_save = '%s/%s' % (where_to_save, subsets)

                if os.path.exists(where_to_save) is False:
                    raise AssertionError(
                        'subset \'%s\' did\'t exist' % subsets)
                elif isinstance(subsets, list) or isinstance(subsets, tuple):
                    for subset in subsets:
                        where_to_save = '%s/%s/' % (where_to_save, subset)

                        if os.path.exists(where_to_save) is False:
                            raise AssertionError('subset \'%s\' did\'t exist' % subset)
                    else:
                        raise AssertionError(
                            'subset must be \'str\', \'list\' or \'tuple\'')

                if os.path.exists(where_to_save) is False:
                    raise AssertionError('label \'%s\' did\'t exist' % label)

                try:
                    filepath = glob.glob('%s/%s/*.wav' % (where_to_save, label))[index]
                except IndexError:
                    raise AssertionError('out of index with label \'%s\' (%d)' % (label, index))

        if len(filepath) > 0:
            return filepath
        else:
            raise AssertionError('out of index with label \'%s\' (%d)' % (label, index))

    def get_sample_from_dataset(self, label, index, subsets='recognize'):
        with open(self.get_filepath_from_dataset(label, index, subsets), 'rb') as fd:
            return fd.read()

    def upload_artifact(self, mapping, model, artifact, session=None):
        wordlist = ','.join(mapping[0]['labels'])
        uuid = self._uuid
        node = self._node

        files = {'file': open(artifact, 'rb')}
        if session is None:
            session = int(os.path.getmtime(artifact))

        if node is None:
            link = '%s/upload_artifact?uid=%s&session=%s&wordlist=%s&model=%s' % (self.HOST, uuid, session, wordlist, model)
        else:
            link = '%s/upload_artifact?uid=%s&session=%s&node=%s&wordlist=%s&model=%s' % (self.HOST, uuid, session, node, wordlist, model)

        ret = post(link, files=files, verify=False)
        if ret.status_code == 200:
            return True

        for index, gate_info in enumerate(mapping):
            subset = gate_info['subset']
            if index == 0:
                continue

            wordlist = ','.join(gate_info['labels'])
            if node is None:
                link = '%s/update_information?uid=%s&session=%s&wordlist=%s&model=%s&subsets=%s' % (host, uuid, session, wordlist, model, subset)
            else:
                link = '%s/update_information?uid=%s&session=%s&node=%s&wordlist=%s&model=%s&subsets=%s' % (host, uuid, session, node, wordlist, model, subset)

            ret = get(link, files=files, verify=False)
            if ret.status_code != 200:
                return False
