from tensorgear import *
from requests import get, post

import tarfile
import random
import urllib
import shutil
import glob
import os


class SpeechCommand(Compose):
    LINK = "http://download.tensorflow.org/data/speech_commands_v0.02.tar.gz"
    HOST = 'https://spt.dektech.com.au:8080'

    def __init__(self, progress, labels=None, local_dir=None):
        super(SpeechCommand, self).__init__('speech_command')

        progress.prefix(prefix='Download dataset')
        progress.bar(length=30)
        progress.percent()
        progress.suffix(suffix='Completed')

        def _progress(count, block_size, total_size):
            progress.render(percent=100.0*(count*block_size/total_size))

        self._labels = []
        self._samples = []
        self._counts = []
        self._indexes = []
        self._method = 'train'

        if local_dir is None:
            if os.path.exists('speech_command.tar.gz') is False:
                try:
                    urllib.request.urlretrieve(self.LINK,
                                               filename='speech_command.tar.gz',
                                               reporthook=_progress)
                except Exception as error:
                    raise error

            if os.path.exists('speech_command') is False:
                os.mkdir('speech_command', 0o777)
                tarfile.open('speech_command.tar.gz', 'r:gz').extractall('speech_command')

            if os.path.exists('dataset/_background_noise_'):
                shutil.rmtree('dataset/_background_noise_')
            local_dir = 'speech_command'

        for path in glob.glob('%s/*' % local_dir):
            if os.path.isdir(path):
                samples = glob.glob('%s/*.wav' % path)
                label = path.split('/')[-1]

                if labels is None or path.split('/')[-1] in labels:
                    self._labels.append(label)
                    self._samples.append(samples)
                    self._counts.append(len(samples))
                    self._indexes.append(random.randint(0, int(3.0*len(samples)/4.0)))
        else:
            self._pos = 0

    def use_dataset(self, name):
        self._method = name

    @property
    def labels(self):
        return self._labels

    def compose(self):
        import scipy.io.wavfile as wav

        while True:
            label = self._labels[self._pos]
            count = self._counts[self._pos]
            index = self._indexes[self._pos]

            if self._method == 'train':
                if index < int(3.0*count/4.0):
                    self._indexes[self._pos] += 1
                    path = self._samples[self._pos][index]
                else:
                    self._indexes[self._pos] = random.randint(0, int(3.0*count/4.0)) + 1
                    path = self._samples[self._pos][self._indexes[self._pos] - 1]
            else:
                if index < int(3.0*count/4.0) or index + 1 >= count:
                    self._indexes[self._pos] = random.randint(int(3.0*count/4.0), count)
                    path = self._samples[self._pos][self._indexes[self._pos] - 1]
                else:
                    self._indexes[self._pos] += 1
                    path = self._samples[self._pos][index]

            if self._pos < len(self._labels) - 1:
                self._pos += 1
            else:
                self._pos = 0

            try:
                with open(path, 'rb') as fd:
                    return (fd.read(), label)
            except Exception as error:
                continue

    def upload_artifact(self, mapping, node, username, model, artifact, session=None):
        import requests
        import hashlib

        requests.packages.urllib3.disable_warnings()

        m = hashlib.md5()
        m.update(username.encode('utf-8'))

        uuid = m.hexdigest()
        wordlist = ','.join(mapping[0]['labels'])
        files = {'file': open(artifact, 'rb')}

        if session is None:
            session = int(os.path.getmtime(artifact))

        if node is None:
            link = '%s/upload_artifact?uid=%s&session=%s&wordlist=%s&model=%s' % (self.HOST, uuid, session, wordlist, model)
        else:
            link = '%s/upload_artifact?uid=%s&session=%s&node=%s&wordlist=%s&model=%s' % (self.HOST, uuid, session, node, wordlist, model)

        ret = post(link, files=files, verify=False)
        if ret.status_code == 200:
            return True

        for index, gate_info in enumerate(mapping):
            subset = gate_info['subset']
            if index == 0:
                continue

            wordlist = ','.join(gate_info['labels'])
            if node is None:
                link = '%s/update_information?uid=%s&session=%s&wordlist=%s&model=%s&subsets=%s' % (host, uuid, session, wordlist, model, subset)
            else:
                link = '%s/update_information?uid=%s&session=%s&node=%s&wordlist=%s&model=%s&subsets=%s' % (host, uuid, session, node, wordlist, model, subset)

            ret = get(link, files=files, verify=False)
            if ret.status_code != 200:
                return False

    def characters(self):
        return ['', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i',
                'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
                's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '\'', '-']
