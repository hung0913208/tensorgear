from tensorgear import *
from requests import get, post

import tarfile
import random
import urllib
import shutil
import glob
import os


class VIVOSDataset(Compose):
    LINK = 'https://ailab.hcmus.edu.vn/assets/vivos.tar.gz'
    STORAGE = 'https://spt.dektech.com.au:8080'

    def __init__(self, progress, local_dir=None):
        super(VIVOSDataset, self).__init__('ailab_vivos')

        progress.prefix(prefix='Download dataset')
        progress.bar(length=30)
        progress.percent()
        progress.suffix(suffix='Completed')

        def _progress(count, block_size, total_size):
            progress.render(percent=100.0*(count*block_size/total_size))

        self._labels = {'train': [], 'test': []}
        self._samples = {'train': [], 'test': []}
        self._localdir = None
        self._method = 'train'

        if local_dir is None:
            if os.path.exists('vivos.tar.gz') is False:
                try:
                    urllib.request.urlretrieve(self.LINK,
                                               filename='vivos.tar.gz',
                                               reporthook=_progress)
                except Exception as error:
                    raise error

            if os.path.exists('vivos') is False:
                os.mkdir('vivos', 0o777)
                tarfile.open('vivos.tar.gz', 'r:gz').extractall('vivos')
            local_dir = 'vivos'

        # @NOTE: load dataset info to memory which is used as caching memory
        for method in ['train', 'test']:
            with open('%s/vivos/%s/prompts.txt' % (local_dir, method)) as fd:
                try:
                    while True:
                        line = next(fd)
                        shortpath, label = line.split(' ', 1)
                        user, _ = shortpath.split('_')
                        shortpath = '%s/%s.wav' % (user, shortpath)

                        self._samples[method].append(shortpath)
                        self._labels[method].append(label)
                except (StopIteration, RuntimeError) as error:
                    pass
        else:
            self._localdir = local_dir


    def use_dataset(self, name):
        self._method = name

    def compose(self):
        while True:
            index = random.randint(0, len(self._samples[self._method]))

            try:
                shortpath = self._samples[self._method][index]
                label = self._labels[self._method][index]

                with open('%s/vivos/%s/waves/%s' % (self._localdir, self._method, shortpath), 'rb') as fd:
                    return (fd.read(), label)
            except Exception as error:
                continue

    def characters(self):
        return [' ', 'a', 'ă', 'â', 'b', 'c', 'd', 'đ', 'e', 'ê', 'g', 'h', 'i',
                'k', 'l', 'm', 'n', 'o', 'ô', 'ơ', 'p', 'q', 'r', 's', 't', 'u',
                'ư', 'v', 'x', 'y', 'à', 'ằ', 'ầ', 'è', '', 'ề', 'ì', 'ò', 'ồ',
                'ờ', 'ù', 'ừ', 'ỳ', 'á', 'ắ', 'ấ', 'é', 'ế', 'í', 'ó', 'ố', 'ớ',
                'ú', 'ứ', 'ý', 'ả', 'ẳ', 'ẩ', 'ẻ', 'ể', 'ỉ', 'ỏ', 'ổ', 'ở', 'ủ',
                'ử', 'ỷ', 'ã', 'ẵ', 'ẫ', 'ẽ', 'ễ', 'ĩ', 'õ', 'ỗ', 'ỡ', 'ũ', 'ữ',
                'ỹ', 'ạ', 'ặ', 'ậ', 'ẹ', 'ệ', 'ị', 'ọ', 'ộ', 'ợ', 'ụ', 'ự', 'ỵ', 
                '0', '1', '2', '3', '4', '5', '6', '7', '8', '9']

