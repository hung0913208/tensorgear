from .parser import configure_to_layers, structure_to_layers
from .utils import is_macros, is_input_layer
from .layer import Layer
from .objective import Objective
from .layers import Input
from .backend import Backend

import numpy as np


class Logits(object):
    def __init__(self, name=None, configure=None, structure=None, mount=None):
        super(Logits, self).__init__()
        self._model_type = None
        self._compiled = False
        self._inputs = []
        self._outputs = []
        self._layers = []
        self._name = name

        if not mount is None:
            for layer in mount:
                if not layer in self._layers:
                    layer.mount(self)
        elif not configure is None:
            configure_to_layers(configure, context=self)
        elif not structure is None:
            structure_to_layers(structure, input=None, context=self)

    @property
    def name(self):
        return self._name

    @property
    def inputs(self):
        return self._inputs

    @property
    def outputs(self):
        return self._outputs

    @property
    def layers(self):
        return self._layers

    @property
    def model_type(self):
        return self._model_type

    @model_type.setter
    def model_type(self, value):
        self._model_type = value

    @property
    def compiled(self):
        if self._compiled is False:
            current_gate = 0

            for layer in self._layers:
                if is_input_layer(layer) and layer.gate < 0:
                    # @NOTE: it seems this layer is an input

                    for layer in self._layers:
                        if is_input_layer(layer) and layer.gate < 0:
                            is_okey_to_continue = True

                            # @NOTE: search if this gate has been assigned
                            while is_okey_to_continue:
                                for checker in self._layers:
                                    if current_gate == checker.gate:
                                        current_gate += 1
                                        break
                                else:
                                    is_okey_to_continue = False
                            else:
                                # @NOTE: okey, apply this gate now

                                layer.gate = current_gate
                                current_gate += 1

                # @NOTE: if this layer is still unmounted we must do it
                if layer.is_mounted is False:
                    try:
                        if layer.mount(self) is False:
                            return False
                    except AssertionError as error:
                        raise error

                # @NOTE: check if this layer is an I/O layers or not
                if len(layer.previous) == 0 and not layer in self._inputs:
                    # @NOTE: it's an input layers

                    if not layer in self._outputs:
                        self._inputs.append(layer)
                elif not layer in self._inputs and not layer in self._outputs:
                    # @NOTE: it's an output layers

                    if len(layer.next) == 0 and is_macros(layer) is False:
                        self._outputs.append(layer)
            else:
                self._compiled = True
        return self._compiled

    def weight(self, name, index=-1):
        for layer in self._layers:
            if layer.name == name:
                if index < 0:
                    return layer.weights
                elif index < len(layer.weights):
                    return layer.weights[index]
                else:
                    return None
        else:
            return None

    def add(self, layer):
        if layer in self._layers:
            raise AssertionError('duplicate layer %s' % layer.name)
        else:
            if layer.is_mounted is True:
                if layer in self._layers:
                    raise AssertionError('layer %s is mounted' % layer.name)
                else:
                    self._layers.append(layer)
            elif layer.mount(self):
                self._layers.append(layer)
            else:
                return False
        return True

    def feed(self, batch):
        result = {}

        # @NOTE: find inputs if it needs
        if len(self._inputs) == 0:

            # @NOTE: search inputs from the optimizer and logits
            for layer in self.layers:
                if is_input_layer(layer):
                    self._inputs.append(layer)
            else:
                self._inputs.sort(key=lambda input: input.gate)

        # @NOTE: build a feed_dict bases on list of inputs
        for input_layer in self._inputs:
            expected = input_layer.shape(0)
            index = input_layer.gate

            if len(expected) > 1:
                if len(expected) != len(batch[index].shape):
                    batch[index] = np.reshape(batch[index], expected)

            result[input_layer.outputs[0]] = batch[index]
        else:
            return result

    def reset(self):
        for layer in self._layers:
            layer.outputs = []

    def implement(self, batch_size=-1, use=None, input_variance_size=False, is_training=True):
        for layer in self._layers:
            layer.outputs = []

        if self.compiled is False:
            raise AssertionError('there was an error during compile '
                                 'this Logits')
        else:
            # @NOTE: freezing step may require to connect an exo-tensor or
            # an exo-layer to logits' inputs

            if not use is None:
                self.__connect_to_outside__(use)

            batch_size = batch_size if is_training else 1
            outputs = self.__solve_logit_instalation__(batch_size,
                                                       is_training=is_training)
            if input_variance_size is True:
                # @NOTE: solve the problem `input variance size` which
                # usually happens inside sequence problems

                if self.__solve_input_variance_size__() is False:
                    raise AssertionError('there was some problem during '
                                         'solving `input variance size` problem')
            return outputs

    def __connect_to_outsite__(self, outsite):
        index = 0

        # @NOTE: workaround to assign input directly to input layers
        for layer in self._inputs:
            layer.previous = []
            layer._outputs = []

        if isinstance(outsite, list) or isinstance(outsite, tuple):
            # @NOTE: when `outsite` is list or tuple, we deduce that each
            # element will match with each inputs

            for layer in self._inputs:
                if is_input_layer(layer):
                    if isinstance(outsite[index], Backend.type('tensor')):
                        if not outsite[index] in layer.outputs:
                            layer.outputs.append(outsite[index])
                    else:
                        if not outsite[index] in layer.previous:
                            layer.previous.append(outsite[index])
                    index += 1
        elif isinstance(outsite, Backend.type('tensor')):
            # @NOTE: `outsite` will be used as a global provider

            for layer in self._inputs:
                if is_input_layer(layer):
                    if not outsite in layer.outputs:
                        layer.outputs.append(ousite)
                index += 1
        else:
            # @NOTE: `outsite` will be used as a global provider

            for layer in self._inputs:
                if is_input_layer(layer):
                    if not outsite in layer.previous:
                        layer.previous.append(outsite)
                index += 1

    def __solve_input_variance_size__(self):
        shape = None

        for layer in self._layers:
            # @NOTE: it only adapts with implemented layers

            if layer.is_implemented:
                shape = layer.shape(tensor=shape, runtime=True)
        else:
            return True

    def __solve_logit_instalation__(self, batch_size, is_training=None):
        on_implementing = []
        on_waiting = []

        # @NOTE: define `is_training` if it isn't defined by default
        if is_training is None:
            is_training = not (batch_size < 0)

        # @NOTE: put input layers into `on_waiting` list
        for layer in self._inputs:
            on_waiting.append(layer)

        # @NOTE: loop throught from inputs down to layers and end to
        # outputs
        while len(on_waiting) > 0:
            on_waiting, on_implementing = on_implementing, on_waiting

            # @NOTE: we will invoke each layer from `on_implementing` list
            # and do implementation with them
            for layer in on_implementing:
                error = layer.implement(is_training=is_training,
                                        batch_size=batch_size)

                if isinstance(layer, Objective) or (error is None) \
                        or (error == 0) or (error is True):
                    # @NOTE: since we finish implement a new layer, we must
                    # add its next layer to 'on_waiting' list from now on

                    if layer.is_implemented is False and is_macros(layer) is False:
                        raise AssertionError('layer %s has done but '
                                             'we can\'t see any '
                                             'outputs' % layer.name)
                    else:
                        for next in layer.next:
                            if not next in on_waiting:
                                on_waiting.append(next)
                elif isinstance(error, str) and len(error) > 0:
                    raise AssertionError(error)
                elif isinstance(error, bool) and error == False:
                    raise AssertionError('there was an error during '
                                         'implementing layer `%s`' % layer.name)
                elif isinstance(error, int) and error != 0:
                    raise AssertionError('there was an error during '
                                         'implementing layer `%s`' % layer.name)
                else:
                    raise error
            else:
                on_implementing = []
        else:
            # @NOTE: after finish implementing everything from logits we
            # must group everything into a single one

            output_gates = []
            for layer in self._outputs:
                if len(layer.outputs) > 0:
                    output_gates.extend(layer.outputs)
                else:
                    raise AssertionError('layer `%s` must be implemented '
                                         'but not' % layer.name)
            else:
                if is_training is True:
                    return [output_gates, ['train', 'valid', 'test']]
                else:
                    return output_gates
