from ..backend import *
from ..objective import Objective
from ..layers import Input

class CategoricalCrossEntropy(Objective):
    def __init__(self, epsilon=1e-10, from_logits=True, axis=-1, **kwargs):
        super(CategoricalCrossEntropy, self).__init__(clsname='categorical_cross_entropy',
                                                      **kwargs)
        self._axis = axis
        self._epsilon = epsilon
        self._from_logits = from_logits

    def freeze(self, ypreds, name):
        if self._from_logits:
            return Backend.implement('softmax', logits=ypreds, name=name)
        else:
            return ypreds

    def estimate(self, ypreds, ground_truth):
        predicted_indices = Backend.implement('argmax', input=ypreds, axis=1)
        ground_truth = Backend.implement('argmax', input=ground_truth, axis=1)
        correct_prediction = Backend.implement('equal',
                                               left=predicted_indices,
                                               right=ground_truth)
        return correct_prediction, predicted_indices

    def __call__(self, ypreds):
        if self._estimator.model_type != 'classification':
            raise AssertionError('SpareSoftmax only support model_type `classification`')
        elif isinstance(self._ground_truth, Backend.type('tensor')):
            ground_truth = self._ground_truth
        elif isinstance(self._ground_truth, Input):
            ground_truth = self._ground_truth.outputs[0]
        else:
            raise AssertionError('CategoricalCrossEntropy only support Tensor '
                                 'or tensorgear.layers.Input')

        # @NOTE: we have 2 choice, if we use softmax layer and if not
        if self._from_logits:
            if isinstance(ypreds, Backend.type('tensor')):
                return Backend.implement('softmax_cross_entropy_with_logits_v2',
                                         labels=ground_truth,
                                         logits=ypreds)
            else:
                return Backend.implement('softmax_cross_entropy_with_logits_v2',
                                         labels=ground_truth,
                                         logits=ypreds[0])
        else:
            # @NOTE: scale preds so that the class probas of each sample sum to 1
            tmp = Backend.implement('reduce_sum', input=ypreds, axis=self._axis,
                                    keepdims=True)
            tmp = Backend.implement('div', left=ypreds, right=tmp)

            # @NOTE: manual computation of crossentropy
            tmp = Backend.implement('clip_by_value', t=tmp,
                                    clip_value_min=self._epsilon,
                                    clip_value_max=1.0 - self._epsilon)
            tmp = Backend.implement('log', x=tmp)
            tmp = Backend.implement('mult', left=self._ground_truth.outputs[0],
                                    right=tmp)
            tmp = Backend.implement('reduce_sum', input=tmp, axis=self._axis)

            return Backend.implement('negative', x=tmp)

