from ..backend import *
from ..objective import Objective
from ..layers import Input

class MeanSquare(Objective):
    def __init__(self, epsilon=10e-5, **kwargs):
        super(MeanSquare, self).__init__(clsname='mean_square', **kwargs)
        self._epsilon = epsilon

    def estimate(self, ypreds, ground_truth):
        predicted_indices = Backend.implement('argmax', input=ypreds, axis=1)
        ground_truth = Backend.implement('argmax', input=ground_truth, axis=1)
        correct_prediction = Backend.implement('equal',
                                               left=predicted_indices,
                                               right=ground_truth)
        return correct_prediction, predicted_indices

    def __call__(self, ypreds):
        if isinstance(self._ground_truth, Backend.type('tensor')):
            ground_truth = self._ground_truth
        elif isinstance(self._ground_truth, Input):
            ground_truth = self._ground_truth.outputs[0]
        else:
            raise AssertionError('MeanSquare only support `tf.Tensor` '
                                 'or tensorgear.layers.Input')

        # @NOTE: implement MeanSquare entropy
        if isinstance(ypreds, Backend.type('tensor')):
            return Backend.implement('mean_square_error',
                                     labels=ground_truth,
                                     logits=ypreds + self._epsilon)
        else:
            return Backend.implement('mean_square_error',
                                     labels=ground_truth,
                                     logits=ypreds[0] + self._epsilon)
