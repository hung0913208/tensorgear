from ..backend import *
from ..objective import Objective
from ..layers import Input

class SpareSoftmax(Objective):
    def __init__(self, epsilon=1e-10, **kwargs):
        super(SpareSoftmax, self).__init__(clsname='spare_softmax', **kwargs)
        self._epsilon = epsilon

    def freeze(self, tensor, freeze_name):
        return Backend.implement('softmax', logits=tensor, name=freeze_name)

    def estimate(self, ypreds, ground_truth):
        predicted_indices = Backend.implement('argmax', input=ypreds, axis=1)
        correct_prediction = Backend.implement('equal',
                                               left=predicted_indices,
                                               right=ground_truth)
        return correct_prediction, predicted_indices

    def __call__(self, ypreds):
        if self._estimator.model_type != 'classification':
            raise AssertionError('SpareSoftmax only support model_type `classification`')
        elif isinstance(self._ground_truth, Backend.type('tensor')):
            ground_truth = self._ground_truth
        elif isinstance(self._ground_truth, Input):
            ground_truth = self._ground_truth.outputs[0]
        else:
            raise AssertionError('SpareSoftmax only support `tf.Tensor` '
                                 'or tensorgear.layers.Input')

        # @NOTE: implement SpareSoftmax and at it to our
        if isinstance(ypreds, Backend.type('tensor')):
            return Backend.implement('sparse_softmax_cross_entropy',
                                     labels=ground_truth,
                                     logits=ypreds + self._epsilon)
        else:
            return Backend.implement('sparse_softmax_cross_entropy',
                                     labels=ground_truth,
                                     logits=ypreds[0] + self._epsilon)
