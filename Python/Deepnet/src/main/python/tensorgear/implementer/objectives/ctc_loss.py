from ..objective import Objective
from ..backend import Backend
from ..layer import Layer

import tensorflow as tf


class CTCLoss(Objective):
    def __init__(self, decoder='beam_search', **kwargs):
        super(CTCLoss, self).__init__(clsname='ctc_loss', **kwargs)
        self._decode_name = decoder

    def freeze(self, tensor, freeze_name):
        if self._decode_name == 'beam_search':
            y_pred = Backend.transpose(probs, perm=[1, 0, 2])
            result, _ = Backend.implement('ctc_beam_search_decoder',
                                          inputs=y_pred,
                                          input_length=ctc_input_length)
            result = Backend.implement('sparse_to_dense', input=y_pred)
            result = Backend.implement('identity', input=result, name=name)
        elif self._decode_name == 'greedy_search':
            y_pred = Backend.transpose(probs, perm=[1, 0, 2])
            result, _ = Backend.implement('ctc_greedy_decoder',
                                          inputs=y_pred,
                                          input_length=ctc_input_length)
            result = Backend.implement('sparse_to_dense', input=y_pred)
            result = Backend.implement('identity', input=result, name=name)
        else:
            result = Backend.implement('argmax', inputs=tensor, axis=1,
                                       name=freeze_name)
        return result

    def estimate(self, ypreds, ground_truth):
        # @NOTE: since we have no choice but to implement CER since this is
        # the possible error summary to be able to do on computing graph
        # recently

        truth = Backend.cast(ground_truth, type='int64')
        truth = Backend.implement('dense_to_sparse', input=truth)
        cer = Backend.implement('edit_distance', hypothesis=self._decoder[0],
                                truth=truth)
        return None, [Backend.implement('reduce_mean', input=cer)]

    def __call__(self, ypreds):
        # @NOTE: calculate ctc_input_length use the different between
        # input time steps and y_pred time step since we may use ConvNet
        # and it would cause big different between input and output shape

        if self._context is None:
            raise AssertionError('please mount `%s` before doing '
                                 'anything' % self._name)

        # @NOTE: extract `features` from logits
        if self._context.logits is None:
            raise AssertionError('apply `%s` without implementing any '
                                 'logits' % self._name)
        elif len(self._context.logits.inputs) != 1:
            raise AssertionError('`%s` don\'t known how to work with '
                                 'multi-inputs' % self._name)
        else:
            feature_layer = self._context.logits.inputs[0]
            features = feature_layer.outputs[0]
            features_count = 1

            for dim in feature_layer.shape(index=0)[1:]:
                if not dim is None:
                    features_count *= dim

        # @NOTE: extract `probs` from ypreds
        if isinstance(ypreds, Backend.type('tensor')):
            probs = ypreds
        else:
            probs = ypreds[0]

        # @NOTE: get input_length
        if feature_layer in self._estimator.seq_length:
            input_length = Backend.cast(self._estimator.seq_length[feature_layer],
                                        type='int32')
            input_length = Backend.implement('div', left=input_length,
                                             right=features_count)
        else:
            raise AssertionError('`%s` must be implemented as sequential '
                                 'input' % feature_layer.name)

        # @NOTE: extract layer `labels` which also is our ground_truth
        if isinstance(self._ground_truth, Layer):
            label_layer = self._ground_truth
            labels = Backend.cast(label_layer.outputs[0], 'int32')
        else:
            raise AssertionError('`%s`\'s ground truth must be a '
                                 'Layer' % self._name)

        # @NOTE: get label_length
        if label_layer in self._estimator.seq_length:
            label_length = self._estimator.seq_length[label_layer]
        else:
            raise AssertionError('`%s` must be implemented as sequential '
                                 'input' % label_layer.name)

        ctc_time_steps = Backend.shape(tensor=probs, runtime=True, dims=[1])
        max_time_steps = Backend.shape(tensor=features, runtime=True, dims=[1])
        max_time_steps = Backend.cast(max_time_steps, type='float')

        # @NOTE: calculate ctc_input_length
        ctc_input_length = Backend.implement('mult', left=input_length,
                                             right=ctc_time_steps)
        ctc_input_length = Backend.cast(ctc_input_length, type='float')
        ctc_input_length = Backend.implement('floordiv', left=ctc_input_length,
                                             right=max_time_steps)
        ctc_input_length = Backend.cast(ctc_input_length, type='int32')

        # @NOTE: convert dense to sparese which is used to apply to tf.ctc_loss
        label_length = Backend.implement('squeeze', input=label_length)
        label_length = Backend.cast(label_length, type='int32')

        ctc_input_length = Backend.implement('squeeze', input=ctc_input_length)
        ctc_input_length = Backend.cast(ctc_input_length, type='int32')

        sparse_labels = Backend.implement('ctc_label_dense_to_sparse',
                                          labels=labels,
                                          label_length=label_length)
        sparse_labels = Backend.cast(sparse_labels, type='int32')

        y_pred = Backend.transpose(probs, perm=[1, 0, 2])

        # @NOTE: implement decoder here since we can get the second loss from the decoder
        # which is very useful to verify the outcome of the prediction
        if self._decode_name == 'beam_search':
            self._decoder = Backend.implement('ctc_beam_search_decoder',
                                              inputs=y_pred,
                                              input_length=ctc_input_length)
        elif self._decode_name == 'greedy_search':
            self._decoder = Backend.implement('ctc_greedy_decoder',
                                              inputs=y_pred,
                                              input_length=ctc_input_length)
        else:
            self._decoder = Backend.implement('argmax', inputs=probs, axis=1), None

        y_pred = Backend.implement('log', x=y_pred, name='%s_log' % self._name)
        ctc_loss = Backend.implement('expand_dims', axis=1,
                                     input=Backend.implement('ctc_loss',
                                                             labels=sparse_labels, inputs=y_pred,
                                                             sequence_length=ctc_input_length))

        # @NOTE: everything is okey for now
        if self._decoder[1] is None:
            return Backend.reshape(ctc_loss, shape=[-1])
        else:
            return [Backend.reshape(ctc_loss, shape=[-1]),
                    Backend.reshape(self._decoder[1], shape=[-1])]
