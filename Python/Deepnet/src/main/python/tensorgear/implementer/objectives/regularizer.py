from ..objective import Objective
from ..backend import Backend


class Regularizer(Objective):
    def __init__(self, ground_truth=None, **kwargs):
        super(Regularizer, self).__init__(clsname='regularizer', **kwargs)

    def __call__(self, ypreds):
        return Backend.implement('regularization_loss')
