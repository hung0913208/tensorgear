from ..backend import Backend
from ..utils import is_input_layer, is_macros
from ..layer import Layer
from ..logits import Logits
from ..objective import Objective
from ..optimizer import Optimizer
from ..estimator import Estimator


class Histogram(object):
    def __init__(self, unit):
        self._unit = unit

    def __call__(self):
        pass

    def implement(self):
        if isinstance(self._unit, Layer):
            for tensor in self._unit.outputs:
                Backend.summaries('histogram', name)
        elif isinstance(self._unit, Logits):
            for layer in self._unit.layers:
                if is_input_layer(layer):
                    continue

                for tensor in layer.weights:
                    Backend.summaries('histogram', values=tensor)
        elif isinstance(self._unit, Objective):
            for result in self._unit.outputs:
                Backemd.summaries('scalar', input=result)
        elif isinstance(self._unit, Optimizer):
            for layer in self._unit.logits.layes:
                if is_input_layer(layer):
                    continue

                for tensor in layer.weights:
                    Backend.summaries('histogram', values=tensor)
            for result in self._unit.entropy.outputs:
                Backemd.summaries('scalar', input=result)
        elif isinstance(self._unit, Estimator):
            for layer in self._unit.optimizer.logits.layers:
                if is_input_layer(layer):
                    continue

                for tensor in layer.weights:
                    Backend.summaries('histogram', values=tensor)
            for result in self._unit.optimizer.entropy.outputs:
                Backemd.summaries('scalar', input=result)
