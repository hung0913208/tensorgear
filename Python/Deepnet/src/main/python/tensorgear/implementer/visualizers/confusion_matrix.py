from textwrap import wrap

import re
import itertools
import tfplot
import matplotlib
import numpy as np
import sklearn.metrics


def confusion_matrix(labels, index=0, title='Confusion matrix',
                     tensor_name='MyFigure/image',
                     normalize=False):
    '''
    Parameters:
        labels                          : This is a list of labels which will be used to display the axix labels
        title='Confusion matrix'        : Title for your matrix
        tensor_name = 'MyFigure/image'  : Name for the output summay tensor

    Returns:
        summary: TensorFlow summary

    Other itema to note:
        - Depending on the number of category and the data , you may have to modify the figzie, font sizes etc.
        - Currently, some of the ticks dont line up due to rotations.
    '''

    def wrapping(writer, ypreds, ytruths, global_step):
        cm = sklearn.metrics.confusion_matrix(ytruths[index], ypreds[index])

        if normalize:
            cm = cm.astype('float')*10 / cm.sum(axis=1)[:, np.newaxis]
            cm = np.nan_to_num(cm, copy=True)
            cm = cm.astype('int')

        np.set_printoptions(precision=2)
        ###fig, ax = matplotlib.figure.Figure()

        fig = matplotlib.figure.Figure(figsize=(7, 7),
                                       dpi=240,
                                       facecolor='w',
                                       edgecolor='k')

        ax = fig.add_subplot(1, 1, 1)
        im = ax.imshow(cm, cmap='Oranges')

        if not labels is None:
            classes = [re.sub(r'([a-z](?=[A-Z])|[A-Z](?=[A-Z][a-z]))', r'\1 ', x) for x in labels[index]]
            classes = ['\n'.join(wrap(l, 40)) for l in classes]

            tick_marks = np.arange(len(classes))

        ax.set_xlabel('Predicted', fontsize=15)
        if not labels is None:
            ax.set_xticks(tick_marks)
            ax.set_xticklabels(classes, fontsize=15, rotation=-90,  ha='center')

        ax.xaxis.set_label_position('bottom')
        ax.xaxis.tick_bottom()

        ax.set_ylabel('True Label', fontsize=15)
        if not labels is None:
            ax.set_yticks(tick_marks)
            ax.set_yticklabels(classes, fontsize=15, va ='center')
        ax.yaxis.set_label_position('left')
        ax.yaxis.tick_left()

        for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
            ax.text(j, i, format(cm[i, j], 'd') if cm[i,j]!=0 else '.',
                    horizontalalignment="center",
                    fontsize=18,
                    verticalalignment='center',
                    color= "black")
        else:
            fig.set_tight_layout(True)
            writer.add_summary(tfplot.figure.to_summary(fig, tag=tensor_name),
                               global_step=global_step)
    return wrapping
