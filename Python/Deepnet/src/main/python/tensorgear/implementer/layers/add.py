import numpy as np

from ..layer import Layer
from ..backend import Backend

class Add(Layer):
    def __init__(self, name=None, **kwargs):
        super(Add, self).__init__(name=name, clsname='add', **kwargs)

    def __shape__(self, index=None, runtime=False):
        if index > 0:
            raise IndexError('`index` is out of range')
        else:
            left = self._previous[0].shape(0, runtime=runtime)
            right = self._previous[1].shape(0, runtime=runtime)

            if runtime is True:
                return Backend.implement('maximum', left=left, right=right)
            else:
                return np.maximum(np.array(left), np.array(right)).tolist()

    def implement(self, is_training=False, batch_size=-1):
        if len(self._previous) == 0:
            raise AssertionError('please add input before doing anything')
        elif len(self._previous) == 1:
            if len(self._previous[0].outputs) != 2:
                raise AssertionError('only support layer with 2 outputs')
        elif len(self._previous) != 2:
            raise AssertionError('only support with 2 input')

        if len(self._previous) == 2:
            left_layer = self._previous[0]
            right_layer = self._previous[1]

            if isinstance(left_layer, Layer):
                left_tensor = left_layer.outputs[0]

            if isinstance(right_layer, Layer):
                right_tensor = right_layer.outputs[0]
        else:
            left_tensor, right_tensor = self._previous[0].outputs

        self._outputs = [Backend.implement('add', name=self._name,
                                           left=left_tensor,
                                           right=right_tensor)]
