from . import Adapter
from ...backend import Backend


class StaticAdapter(Adapter):
    def __init__(self):
        super(StaticAdapter, self).__init__()

    def __call__(self, cells, inputs, scope='rnn', sequence_length=None, **kwargs):
        return Backend.implement('static_rnn', cell=cells[0],
                                 sequence_length=sequence_length,
                                 inputs=inputs,
                                 scope=scope)
