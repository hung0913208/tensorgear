from ..layer import Layer
from ..backend import Backend

class Pad(Layer):
    def __init__(self, paddings=None, **kwargs):
        super(Pad, self).__init__(clsname='pad', **kwargs)
        self._paddings = paddings

    def __shape__(self, index=None, runtime=False):
        if not index is None and index > 0:
            raise IndexError('`index` is out of range')

        for _ in range(1):
            if len(self._outputs) == 0:
                if runtime is True:
                    self.implement(is_training=True)
                else:
                    return self._shape
            else:
                return Backend.shape(self._outputs[0], runtime)
        else:
            raise AssertionError('please, implement layer `%s` '
                                 'before get a runtime shape' % self.name)

    def implement(self, is_training=False, batch_size=-1):
        if len(self._previous) == 0:
            raise AssertionError('please add input before doing anything')
        elif len(self._previous) != 1:
            raise AssertionError('no support with more than one input')
        else:
            self._outputs = [Backend.implement('pad',
                                               input=self._previous[0].outputs[0],
                                               paddings=self._paddings)]
