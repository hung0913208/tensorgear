from ..layer import Layer
from ..backend import Backend

class ReduceSum(Layer):
    def __init__(self, name=None, **kwargs):
        super(ReduceSum, self).__init__(name=name, clsname='reduce_sum')

    def __shape__(self, index, runtime):
        if runtime:
            return Backend.cast([])
        else:
            return []

    def implement(self, is_training=False, batch_size=-1):
        group = []

        for layer in self._previous:
            if batch_size > 0:
                if layer.trainable:
                    group.append(Backend.reshape(layer.outputs[0],
                                             shape=[batch_size, -1]))
            else:
                if layer.trainable:
                    group.append(Backend.reshape(layer.outputs[0], shape=[-1]))
        else:
            concat = Backend.implement('concat', input=group,
                                       axis=1 if batch_size > 0 else 0)

            if concat is None:
                raise AssertionError('there was an error during '
                                     'implement `concat`')
            else:
                self._outputs = [Backend.implement('reduce_sum', name=self._name,
                                                   input=concat, 
                                                   axis=1 if batch_size > 0 else 0)]
