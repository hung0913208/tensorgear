from ..layer import Layer
from ..objective import Objective
from ..backend import Backend

class Numberic(Layer):
    def __init__(self, operator=None, name=None, **kwargs): 
        super(Numberic, self).__init__(name=name, clsname='numberic', **kwargs)
        self._operator = operator

    def implement(self, is_training=False, batch_size=-1):
        if not self._operator in ['add', 'mult', 'div', 'square', 'abs']:
            raise AssertionError('don\'t support operator %s' % self._operator)
        elif len(self._previous) == 0:
            raise AssertionError('please add input before doing anything')

        if self._operator in ['square', 'abs']:
            if len(self._previous) != 1:
                raise AssertionError('only support with 1 input')
            else:
                previous = self._previous[0]

            if isinstance(previous, Layer):
                x = previous.outputs[0]
            elif isinstance(previous, Objective):
                x = previous.outputs[0]
            else:
                x = previous

            self._outputs = [Backend.implement(self._operator, 
                                               name=self._name, x=x)]
        else:
            if len(self._previous) != 2:
                raise AssertionError('only support with 2 input')
            else:
                left_layer = self._previous[0]
                right_layer = self._previous[1]

            if isinstance(left_layer, Layer):
                left_tensor = left_layer.outputs[0]

            if isinstance(right_layer, Layer):
                right_tensor = right_layer.outputs[0]

            self._outputs = [Backend.implement(self._operator, name=self._name, 
                                               left=left_tensor, 
                                               right=right_tensor)]

