from ..parser import structure_to_layers
from ..layer import Layer
from ..backend import Backend


class Share(Layer):
    def __init__(self, *args, **kwargs):
        super(Share, self).__init__(clsname='share', **kwargs)
        Backend.update('macros', Share)
        self._sequences = args

    def on_link_to_next(self, layer):
        if self in layer.previous:
            del layer.previous[layer.previous.index(self)]
        layer.previous.extend(self._sequences)

    def on_link_to_previous(self, layers, context):
        converted = []

        if len(self._previous) > 1:
            raise AssertionError('`Share` only support with single one input')

        for sequence in self._sequences:
            converted.append(structure_to_layers(sequence, input=self._previous[0],
                                                 context=self._context or context))
        else:
            self._sequences = converted

    def implement(self, is_training=False, batch_size=-1):
        pass
