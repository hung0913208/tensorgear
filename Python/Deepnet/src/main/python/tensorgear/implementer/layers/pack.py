from ..parser import structure_to_layers
from ..layer import Layer
from ..backend import Backend

class Pack(Layer):
    def __init__(self, *args, **kwargs):
        super(Pack, self).__init__(clsname='pack', **kwargs)
        Backend.update('macros', Pack)
        self._sequences = args

    def on_link_to_next(self, layer):
        if self in layer.previous:
            del layer.previous[layer.previous.index(self)]
        layer.previous.extend(self._sequences)

        for dep in layer.previous:
            if not layer in dep.next:
                dep.next.append(layer)

    def on_link_to_previous(self, layers, context):
        converted = []

        if len(self._previous) > 1:
            raise AssertionError('`Share` only support with single one input')

        for index, sequence in enumerate(self._sequences):
            if len(self._previous) > index:
                if len(sequence) > 0:
                    result = structure_to_layers(sequence, input=self._previous[index],
                                                 context=self._context or context)
                else:
                    result = self._previous[index]
                converted.append(result)
            else:
                if len(sequence) > 0:
                    converted.append(structure_to_layers(sequence, input=None,
                                                         context=self._context or context))
                else:
                    raise AssertionError('`sequence` mustn\'t be empty')
        else:
            self._sequences = converted

    def implement(self, is_training=False, batch_size=-1):
        pass
