from ..layer import Layer
from ..backend import Backend


class Replicate(Layer):
    def __init__(self, name=None):
        super(Replicate, self).__init__(self, clsname='replicate', name=name)
        Backend.update('macros', Replicate)

    def implemenet(self, batch_size=-1, is_training=False):
        pass
