from .rnn import RNN
from ..backend import Backend


class LSTM(RNN):
    def __init__(self, num_units=0, cell_type='lstm', dtype=None, name=None, 
                 activation=None, **kwargs):
        super(LSTM, self).__init__(clsname='lstm', name=name, **kwargs)        
        if isinstance(activation, dict):
            self._activation = Backend.activation(**activation)
        else:
            self._activation = Backend.activation(activation)

        self._cell_type = cell_type
        self._cell_kwargs = kwargs
        self._dtype = dtype
        self._units = num_units

    def implement(self, is_training=False, batch_size=-1):
        # @NOTE: we don't reimplement a LSTM cell, we just warp around lstm
        # object from backend, if you need a customize RNN, please use RNN
        # layer since i decide every basic RNN will be derived from it

        if len(self._previous) == 0:
            raise AssertionError('please add input before doing anything')
        elif len(self._previous) != 1:
            raise AssertionError('no support with more than one input')
        else:
            input_tensor = self._previous[0].outputs[0]

        # @NOTE: we allow using input's dtype as default type of LSTM cells
        dtype = Backend.type(input_tensor) if self._dtype is None else self._dtype

        # @NOTE: implement cells according the requirement and apply them after 
        # finish successfully
        for _ in range(self.requirement):
            self._cells.append(Backend.implement(self._cell_type, num_units=self._units,
                                                 dtype=dtype, name=self._name,
                                                 activation=self._activation,
                                                 **self._cell_kwargs))
        else:
            self.apply()
