from ..layer import Layer
from ..backend import Backend

class Alias(Layer):
    def __init__(self):
        super(Pack, self).__init__(clsname='pack', **kwargs)

    def on_link_to_previous(self, layers, context):
        pass

    def implement(self, is_training=False, batch_size=-1):
        for layer in self._previous:
            self._outputs.extend(layer.outputs)
