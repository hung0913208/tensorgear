from ..layer import Layer
from ..backend import Backend
from .adapters import builtin_adapters

class RNN(Layer):
    def __init__(self, shape=None, transpose=None, adapter='dynamic',
                 select_outputs=-1, **kwargs):
        super(RNN, self).__init__(clsname=kwargs.get('clsname') or 'rnn')
        self._cells = []
        self._num_units = kwargs.get('num_units')
        self._transpose = transpose
        self._shape = shape
        self._adapter = adapter
        self._adapter_kwargs = kwargs
        self._select_outputs = select_outputs

    def __attend__(self, input):
        return input

    @property
    def num_units(self):
        return self._num_units

    def sequence_length(self):
        # @TODO: calculate how squence_length to specific data while training
        # or testing

        return None

    def merge(self, adapter_outputs):
        return adapter_outputs

    def unimplement(self):
        super(RNN, self).unimplement()
        self._cells = []

    @property
    def requirement(self):
        adapter = builtin_adapters(self._adapter)

        if adapter is None:
            raise AssertionError('don\'t support adapter %s' % self._adapter)
        else:
            return adapter.get('requirement') or 1

    def implement(self, is_training=False, batch_size=-1):
        if len(self._previous) == 0:
            raise AssertionError('please add input before doing anything')

        if len(self._cells) > 0:
            self.apply()
        else:
            # @NOTE: implement default rnn cell

            for _ in range(self.requirement):
                self._cells.append(Backend.implement('rnn', cell=self))
            else:
                self.apply()

    def apply(self):
        adapter = builtin_adapters(self._adapter)

        if len(self._previous) != 1:
            raise AssertionError('`rnn` require only 1 input but you have '
                                 '%d inputs' % len(self._previous))
        else:
            inputs = self._previous[0].outputs[0]

        # @NOTE: reshape automatically the input to match with rnn's requirement
        if not self._shape is None:
            dynamic = False
            shape = [-1]

            for index, dim in enumerate(self._shape):
                if dim < 0 and dynamic is False:
                    dynamic = True
                else:
                    raise AssertionError('only support dynamic shape with only '
                                         'one position')
                shape.append(dim)

            if not dynamic is None:
                # @NOTE: when dynamic is on, we will use the same dim of
                # input `batch_size` and reshape the output
                batch_size = tf.shape(inputs)[0]
                shape[0] = batch_size

            # @NOTE: okey now, we will reshape input with the calculated shape
            # we have done above
            inputs = tf.reshape(inputs, shape)

        # @NOTE: check and use the adapter
        if adapter is None:
            raise AssertionError('don\'t support adapter `%s`' % self._adapter)
        elif 'applier' in adapter and callable(adapter['applier']):
            # @NOTE: by default we must use [batchs, steps, features] but on
            # some application we can use [steps, batchs, features]

            if not self._transpose is None:
                inputs = Backend.transpose(inputs, perm=self._transpose)

            adapter_outputs, last_output = \
                    adapter['applier'](self._cells,
                                       inputs=inputs,
                                       scope=self._name,
                                       sequence_length=self.sequence_length(),
                                       **self._adapter_kwargs)
        else:
            raise AssertionError('adapter `%s` still underdevelopment, '
                                 'please use another instead' % self._adapter)

        # @NOTE: since adapter will be used as a lazyload function to adapt our
        # rnn definition so we now can collect weights
        for cell in self._cells:
            self._weights.extend(Backend.implement('variables', cell=cell))

        # @NOTE: after using adapter, it will produce a range of outputs, we
        # will use these output to produce self._outputs
        if isinstance(self._select_outputs, list):
            for index in self._select_outputs:
                if index < len(adapter_outputs):
                    # @NOTE: since adapter_outputs can produce list or Tensor
                    # we must implement them separatedly

                    if isinstance(adapter_outputs, list):
                        if isinstance(adapter_outputs[index], tuple):
                            self._outputs.append(self.__call__(adapter_outputs[index]))
                        else:
                            self._outputs.append(adapter_outputs[index])
                    else:
                        self._outputs.append(Backend.strip(adapter_outputs,
                                                           dim=1,
                                                           value=index))
                else:
                    raise AssertionError('you require input %d but len(adapter_outputs)'
                                         ' = %d ' % (index, len(adapter_outputs)))
        elif isinstance(self._select_outputs, dict):
            # @NOTE: define 'range' from specific parameters which aim to
            # used with JSON

            start = self._select_outputs.get('start') or 0
            step = self._select_outputs.get('step') or 1
            stop = self._select_outputs['stop']

            if stop < 0:
                raise AssertionError('`stop` must be positive')

            adapter_outputs = Backend.transpose(adapter_outputs, perm=[1, 0, 2])

            if not Backend.shape(adapter_outputs)[0] is None:
                # @NOTE: verify our parameters

                if start >= len(adapter_outputs):
                    raise AssertionError('start{%d} must lower than '
                                         '%d' % (start, len(adapter_output)))
                elif stop >= len(adapter_outputs):
                    raise AssertionError('stop{%d} must lower or equal '
                                         '%d' % (start, len(adapter_output)))

            for index in range(start, stop, step):
                if isinstance(adapter_outputs, list):
                    if isinstance(adapter_outputs[index], tuple):
                        self._outputs.append(self.__call__(adapter_outputs[index]))
                    else:
                        self._outputs.append(adapter_outputs[index])
                else:
                    self._outputs.append(Backend.strip(adapter_outputs,
                                                       dim=1,
                                                       value=index))
        elif isinstance(self._select_outputs, int):
            # @NOTE: select a specific output

            if isinstance(last_output, tuple) or self._select_outputs != -1:
                self._outputs = \
                    [Backend.transpose(adapter_outputs, perm=[1, 0, 2])[-1]]
            else:
                self._outputs = [last_output]
        else:
            # @NOTE: if  self._select_outputs is None, it would indicate that user
            # would like to deal with the outputs manually

            if isinstance(adapter_outputs, Backend.type('tensor')):
                self._outputs = [adapter_outputs]
            else:
                # @NOTE: since some adapters will produce a list of outputs
                # so we must deal with this case carefully

                self._outputs = list(adapter_outputs)
