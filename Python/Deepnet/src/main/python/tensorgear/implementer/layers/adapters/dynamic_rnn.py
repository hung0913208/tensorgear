from . import Adapter
from ...backend import Backend


class DynamicAdapter(Adapter):
    def __init__(self):
        super(DynamicAdapter, self).__init__()

    def __call__(self, cells, inputs, scope=None, sequence_length=None, **kwargs):
        return Backend.implement('dynamic_rnn', cell=cells[0],
                                 sequence_length=sequence_length,
                                 inputs=inputs,
                                 scope=scope)
