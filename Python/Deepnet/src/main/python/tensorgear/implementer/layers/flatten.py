import numpy as np

from ..layer import Layer
from ..backend import Backend

class Flatten(Layer):
    def __init__(self, name=None, **kwargs):
        super(Flatten, self).__init__(name=name, clsname='flatten', **kwargs)

    def __shape__(self, index=None, runtime=False):
        if index > 0:
            raise IndexError('`index` is out of range')

        flatten_dim = 1
        for dim in self._previous[0].shape(0, runtime=False):
            if not dim is None:
                flatten_dim *= dim
        else:
            if runtime is True:
                input_shape = self._previous[0].shape(0, runtime=False)

                if isinstance(input_shape, Backend.type('tensor')):
                    reduce_prod = Backend.implement('reduce_prod', input=input_shape)

                    return [Backend.implement('div', left=reduce_prod, right=flatten_dim),
                            Backend.cast(flatten_dim)]
                else:
                    batch_size = None
                    for dim in input_shape:
                        if batch_size is None:
                            batch_size = dim
                        else:
                            batch_size = Backend.implement('multiply',
                                                           left=batch_size, right=dim)
                    return [batch_size, Backend.cast(flatten_dim)]
            else:
                return [None, flatten_dim]

    def implement(self, is_training=False, batch_size=-1):
        if len(self._previous) == 0:
            raise AssertionError('please add input before doing anything')
        elif len(self._previous) != 1:
            raise AssertionError('no support with more than one input')

        previous_layer = self._previous[0]
        flatten_dim = 1

        if isinstance(previous_layer, Layer):
            input_tensor = previous_layer.outputs[0]
        else:
            raise AssertionError('only support type `Layer`')

        for dim in Backend.shape(input_tensor):
            if not dim is None:
                flatten_dim *= dim
        else:
            self._outputs = [Backend.reshape(input_tensor,
                                             shape=[-1, flatten_dim])]
