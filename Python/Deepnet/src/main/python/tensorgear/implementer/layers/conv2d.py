from ..layer import Layer
from ..backend import Backend

class Conv2D(Layer):
    def __init__(self, filters=None, kernels=None, name=None,
                 dilations=[1, 1], strides=[1, 1], padding='same',
                 actvation='relu', kernel_initializer='xavier',
                 bias_initializer='zeros', data_format='channels_last',
                 kernel_regularizer=None, bias_regularizer=None,
                 name_of_variables=None, **kwargs):
        super(Conv2D, self).__init__(name=name, clsname='conv2d', **kwargs)

        if isinstance(kernel_initializer, str):
            self._kernel_initializer = Backend.initializer(kernel_initializer)
        elif isinstance(kernel_initializer, dict):
            self._kernel_initializer = Backend.initializer(**kernel_initializer)
        else:
            self._kernel_initializer = None

        if isinstance(bias_initializer, str):
            self._bias_initializer = Backend.initializer(bias_initializer)
        elif isinstance(bias_initializer, dict):
            self._bias_initializer = Backend.initializer(**bias_initializer)
        else:
            self._bias_initializer = None

        if isinstance(kernel_regularizer, str):
            self._kernel_regularizer = {'op_name': kernel_regularizer}
        elif isinstance(kernel_regularizer, dict):
            self._kernel_regularizer = kernel_regularizer
        else:
            self._kernel_regularizer = None

        if isinstance(bias_regularizer, str):
            self._bias_regularizer = {'op_name': bias_regularizer}
        elif isinstance(bias_regularizer, dict):
            self._bias_regularizer = bias_regularizer
        else:
            self._bias_regularizer = None

        self._activation = Backend.activation(actvation)
        self._name_of_variables = name_of_variables
        self._wrap_around = name_of_variables is None
        self._channel_position = 3 if data_format == 'channels_last' else 0
        self._filters = filters
        self._strides = strides
        self._kernels = kernels
        self._dilations = dilations
        self._padding = padding

        if kernels is None:
            pass
        elif len(kernels) == 2:
            self._kernels = kernels
        else:
            raise AssertionError('len(kernels) must be 2 but you provide '
                                 'a list/tuple with len is %d' % (len(kernels)))

        if filters is None:
            pass
        elif isinstance(filters, int):
            if filters <= 0:
                raise AssertionError('filters must greater than 0')

            self._filters = filters
        else:
            raise AssertionError('filters must be an int value')

    def implement(self, is_training=False, batch_size=-1):
        if len(self._previous) == 0:
            raise AssertionError('please add input before doing anything')
        elif len(self._previous) != 1:
            raise AssertionError('no support with more than one input')
        else:
            self._weights = []
            self._outputs = []

        if self._wrap_around is True:
            with Backend.scope(self._name or self._clsname, reuse=True):
                self.__build__(is_training, batch_size)
        else:
            self.__build__(is_training, batch_size)

    def __shape__(self, index=None, runtime=False):
        if index > 0:
            raise IndexError('`index` is out of range')
        elif len(self._outputs) == 0:
            output_c = self._filters
            fixed = 0 if self._padding.lower() == 'same' else 1
            input_h, input_w = self._previous[0].shape(0, dims=[1, 2],
                                                       runtime=runtime)
            effective_filter_h = (self._filters[0] - 1) * self._dilations[0]
            effective_filter_w = (self._filters[1] - 1) * self._dilations[1]

            if runtime is True:
                output_h = Backend.cast(input_h, np.float32)
                output_w = Backend.cast(input_w, np.float32)
                output_h = Backend.implement('add', left=output_h,
                                             right=Backend.cast(self._strides[0] - 1))
                output_w = Backend.implement('add', left=output_w,
                                             right=Backend.cast(self._strides[1] - 1))

                if fixed != 0:
                    output_h = Backend.implement('sub', left=output_h,
                                                 right=Backend.cast(effective_filter_h))
                    output_w = Backend.implement('sub', left=output_w,
                                                 right=Backend.cast(effective_filter_w))
            else:
                output_h = (input_h - fixed*effective_filter_h + strides_h - 1)
                output_w = (input_w - fixed*effective_filter_w + strides_w - 1)

            if self._channel_position == 3:
                if runtime is True:
                    return [self._previous[0].shape(index=0,
                                                    dims=[0],
                                                    runtime=runtime),
                            output_h, output_w,
                            Backend.cast(output_c)]
                else:
                    return [None, output_h, output_w, output_c]
            else:
                if runtime is True:
                    return [self._previous[0].shape(index=0,
                                                    dims=[0],
                                                    runtime=runtime),
                            Backend.cast(output_c),
                            output_h, output_w]
                else:
                    return [None, output_h, output_w, output_c]
        else:
            return Backend.shape(self._outputs[0], runtime)

    def __build__(self, is_training, batch_size):
        previous_layer = self._previous[0]

        # @NOTE: fetching input_tensor from output of previous layer
        if isinstance(previous_layer, Layer):
            input_tensor = previous_layer.outputs[0]
            input_shape = previous_layer.shape(0)
        else:
            raise AssertionError('`previous_layer` must be a Layer object')

        # @NOTE: get channel valuue from input_tensor
        if len(input_shape) == 4:
            if input_shape[self._channel_position] is None:
                raise AssertionError('auto calculate shape at the time of '
                                     'training is still under development')
            else:
                channel = input_shape[self._channel_position]
        else:
            raise AssertionError('Conv2d `%s` requires input with shape==4 but '
                                 'you provide input with shape==%d' % \
                                     (self._name, len(input_shape)))

        # @NOTE: calculate variable `kernel`
        filters = (self._kernels[0], self._kernels[1], channel, self._filters)
        strides = (1, self._strides[0], self._strides[1], 1)
        dilations = (1, self._dilations[0], self._dilations[1], 1)
        padding = self._padding.upper()
        data_format = 'NHWC' if self._channel_position == 3 else 'NCHW'

        # @NOTE: generate a variable to use as 'kernel'
        if not self._kernel_regularizer is None and is_training and self.trainable:
            kernel_regularizer = Backend.regularizer(**self._kernel_regularizer)
        else:
            kernel_regularizer = None

        if self._name_of_variables is None or not 'kernel' in self._name_of_variables:
            kernel = Backend.variable(name='kernel_of_%s' % self._name,
                                      initializer=self._kernel_initializer(),
                                      regularizer=kernel_regularizer,
                                      shape=filters,
                                      trainable=self._trainable,
                                      dtype=Backend.dtype(input_tensor))
        else:
            kernel = Backend.variable(name=self._name_of_variables['kernel'],
                                      initializer=self._kernel_initializer(),
                                      regularizer=kernel_regularizer,
                                      shape=filters,
                                      trainable=self._trainable,
                                      dtype=Backend.dtype(input_tensor))
        self._weights.append(kernel)

        if self._bias_initializer is None and self._activation is None and self._wrap_around is False:
            with Backend.scope(self._name or self._clsname, reuse=True):
                conv2d = Backend.implement('conv2d',
                                            input=input_tensor,
                                            filter=kernel, strides=strides,
                                            padding=padding, dilations=dilations,
                                            data_format=data_format)
        else:
            conv2d = Backend.implement('conv2d',
                                       input=input_tensor,
                                       filter=kernel, strides=strides,
                                       padding=padding, dilations=dilations,
                                       data_format=data_format)

        # @NOTE: calculate variable `bias`
        if not self._bias_initializer is None:
            bias_shape = [self._filters]

            # @NOTE: generate a variable to use as 'bias'
            if not self._bias_regularizer is None and is_training and self.trainable:
                bias_regularizer = Backend.regularizer(**self._bias_regularizer)
            else:
                bias_regularizer = None


            if self._name_of_variables is None or not 'bias' in self._name_of_variables:
                bias = Backend.variable(name='bias_of_%s' % self._name,
                                        initializer=self._bias_initializer(),
                                        regularizer=bias_regularizer,
                                        shape=bias_shape,
                                        trainable=self._trainable,
                                        dtype=Backend.dtype(input_tensor))
            else:
                bias = Backend.variable(name=self._name_of_variables['bias'],
                                        initializer=self._bias_initializer(),
                                        regularizer=bias_regularizer,
                                        shape=bias_shape,
                                        trainable=self._trainable,
                                        dtype=Backend.dtype(input_tensor))
            self._weights.append(bias)

            if self._wrap_around is False:
                with Backend.scope(self._name or self._clsname, reuse=True):
                    add = Backend.implement('add', left=conv2d, right=bias)

                    if self._activation is None:
                        output = add
                    else:
                        output = self._activation(add)
            else:
                add = Backend.implement('add',left=conv2d, right=bias)

                if self._activation is None:
                    output = add
                else:
                    output = self._activation(add)
        else:
            if self._wrap_around is False:
                with Backend.scope(self._name or self._clsname, reuse=True):
                    if self._activation is None:
                        output = conv2d
                    else:
                        output = self._activation(conv2d)
            else:
                if self._activation is None:
                    output = conv2d
                else:
                    output = self._activation(conv2d)
        self._outputs = [output]
