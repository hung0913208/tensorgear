from .rnn import RNN
from ..backend import Backend

class GRU(RNN):
    def __init__(self, num_units=0, cell_type='gru', dtype=None, name=None, **kwargs):
        super(GRU, self).__init__(clsname='gru', name=name, **kwargs)
        self._cell_type = cell_type
        self._cell_kwargs = kwargs
        self._dtype = dtype
        self._units = num_units

    def implement(self, is_training=False, batch_size=-1):
        # @NOTE: we don't reimplement a GRU cell, we just warp around gru
        # object from backend, if you need a customize RNN, please use RNN
        # layer since i decide every basic RNN will be derived from it

        if len(self._previous) == 0:
            raise AssertionError('please add input before doing anything')
        elif len(self._previous) != 1:
            raise AssertionError('no support with more than one input')
        else:
            input_tensor = self._previous[0].outputs[0]

        # @NOTE: we allow using input's dtype as default type of LSTM cells
        dtype = Backend.type(input_tensor) if self._dtype is None else self._dtype

        # @NOTE: implement cells according the requirement and apply them after
        # finish successfully
        for _ in range(self.requirement):
            self._cells.append(Backend.implement(self._cell_type, num_units=self._units,
                                                 dtype=dtype, name=self._name,
                                                 **self._cell_kwargs))
        else:
            self.apply()
