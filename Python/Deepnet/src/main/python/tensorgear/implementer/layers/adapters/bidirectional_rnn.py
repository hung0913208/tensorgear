from . import Adapter
from ...backend import Backend


class BidirectionalAdapter(Adapter):
    def __init__(self):
        super(BidirectionalAdapter, self).__init__()

    def get(self, name):
        if name == 'requirement':
            return 2
        else:
            return None

    def __call__(self, cells, inputs, scope=None, sequence_length=None,
                 use_dynamic=True, **kwargs):
        if use_dynamic:
            return Backend.implement('bidirectional_dynamic_rnn',
                                     sequence_length=sequence_length,
                                     forward=cells[0],
                                     backward=cells[1],
                                     inputs=inputs,
                                     scope=scope)
        else:
            return Backend.implement('bidirectional_static_rnn',
                                     sequence_length=sequence_length,
                                     forward=cells[0],
                                     backward=cells[1],
                                     inputs=inputs,
                                     scope=scope)
