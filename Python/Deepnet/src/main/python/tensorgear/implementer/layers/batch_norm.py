from ..layer import Layer
from ..backend import Backend

class BatchNorm(Layer):
    def __init__(self, name=None,
                 axis=-1, momentum=0.99, epsilon=0.001, center=True, scale=True,
                 beta_initializer='zeros', gamma_initializer='ones',
                 moving_mean_initializer='zeros',
                 moving_variance_initializer='zeros',
                 beta_regularizer=None, gamma_regularizer=None,
                 beta_constraint=None, gamma_constraint=None,
                 renorm=False, renorm_clipping=None, renorm_momentum=0.99,
                 fused=None, virtual_batch_size=None, adjustment=None,
                 **kwargs):
        super(BatchNorm, self).__init__(name=name, clsname='batch_norm', **kwargs)
        self._axis = axis
        self._momentum = momentum
        self._epsilon = epsilon
        self._center = center
        self._scale = scale
        self._beta_initializer = Backend.initializer(beta_initializer)
        self._gamma_initializer = Backend.initializer(gamma_initializer)
        self._moving_mean_initializer = Backend.initializer(moving_mean_initializer)
        self._moving_variance_initializer = Backend.initializer(moving_variance_initializer)
        self._beta_regularizer = beta_regularizer
        self._gamma_regularizer = gamma_regularizer
        self._beta_constraint = beta_constraint
        self._gamma_constraint = gamma_constraint
        self._renorm = renorm
        self._renorm_clipping = renorm_clipping
        self._renorm_momentum = renorm_momentum
        self._fused = fused
        self._virtual_batch_size = virtual_batch_size
        self._adjustment = adjustment

    def __shape__(self, index=None, runtime=False):
        if index > 0:
            raise IndexError('`index` is out of range')
        else:
            return self._previous[0].shape(0, runtime=runtime)

    def implement(self, is_training=False, batch_size=-1):
        if len(self._previous) == 0:
            raise AssertionError('please add input before doing anything')
        elif len(self._previous) != 1:
            raise AssertionError('no support with more than one input')
        else:
            input_tensor = self._previous[0].outputs[0]

        with Backend.scope(self._name or self._clsname):
            self._outputs.append(Backend.implement('batch_norm',
                                                   inputs=input_tensor,
                                                   axis=self._axis,
                                                   momentum=self._momentum,
                                                   epsilon=self._epsilon,
                                                   center=self._center,
                                                   scale=self._scale,
                                                   beta_initializer=self._beta_initializer,
                                                   gamma_initializer=self._gamma_initializer,
                                                   moving_mean_initializer=self._moving_mean_initializer,
                                                   moving_variance_initializer=self._moving_variance_initializer,
                                                   beta_regularizer=self._beta_regularizer,
                                                   gamma_regularizer=self._gamma_regularizer,
                                                   beta_constraint=self._beta_constraint,
                                                   gamma_constraint=self._gamma_constraint,
                                                   training=is_training,
                                                   trainable=self._trainable,
                                                   name=self._name,
                                                   renorm=self._renorm,
                                                   renorm_clipping=self._renorm_clipping,
                                                   renorm_momentum=self._renorm_momentum,
                                                   fused=self._fused,
                                                   virtual_batch_size=self._virtual_batch_size,
                                                   adjustment=self._adjustment))
