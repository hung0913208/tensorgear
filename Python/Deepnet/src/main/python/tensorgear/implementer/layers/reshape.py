from ..layer import Layer
from ..backend import Backend

class Reshape(Layer):
    def __init__(self, shape=None, name=None, **kwargs):
        super(Reshape, self).__init__(name=name, clsname='reshape', **kwargs)
        self._expected_output_shape = shape

    def __shape__(self, index=None, runtime=False):
        # @TODO: consider the specific cases when we use Reshape with runtime
        # shape, not with constance

        if runtime:
            result = []
            flatten_shape = 1

            for dim in self._expected_output_shape:
                flatten_shape *= dim
                result.append(dim)
            else:
                input_shape = self._previous[0].shape(0, runtime=True)
                batch_size = Backend.implement('reduce_prod', input=input_shape)
                batch_size = Backend.implement('div', left=batch_size,
                                               right=Backend.cast(flatten_shape))
                result.insert(batch_size, 0)
                return result
        else:
            result = [None]

            for dim in self._expected_output_shape:
                if isinstance(dim, Backend.type('shape')) or dim < 0:
                    result.append(None)
                else:
                    result.append(dim)
            else:
                return result

    def implement(self, is_training=False, batch_size=-1):
        # @NOTE: reshape would have many more specific way to use since we allow
        # using shape on runtime, it would mean we can control the output's shape
        # more flexible

        if len(self._previous) == 0:
            raise AssertionError('please add input before doing anything')
        elif len(self._previous) != 1:
            raise AssertionError('only support with 1 input')
        else:
            previous_layer = self._previous[0]

        if isinstance(previous_layer, Layer):
            input_tensor = previous_layer.outputs[0]
            input_shape = Backend.shape(input_tensor)

        for index, def_dim in enumerate(self._expected_output_shape):
            # @NOTE: since we will support the way to implement any neural network with
            # JSON files, we must define a simple way to handle the case when we use
            # another layer's dims as a part of the shape we would like to perform

            if isinstance(def_dim, list) or isinstance(def_dim, tuple):
                outputs_index = def_dim[1]
                dim_index = def_dim[2]
                target = def_dim[0]
            elif isinstance(def_dim, str):
                parsed = def_dim.split(':')

                if len(parsed) != 3:
                    raise AssertionError('wrong definition of `shape`')
                else:
                    outputs_index = int(parsed[1])
                    dim_index = int(parsed[2])
                    target = parsed[0]
            else:
                continue

            # @NOTE: search the target's shape and slice it to get our expected dims
            for layer in self._context.layers:
                if target == layer or layer.name == target:
                    if layer.is_implemented is False:
                        raise AssertionError('found a loopback, this network may be broken')
                    elif outputs_index < len(layer.outputs):
                        how_many_dim = len(Backend.shape(layer.outputs[outputs_index]))

                        if how_many_dim > dim_index:
                            raise IndexError('`dim_index` must lower than %d' % how_many_dim)
                        else:
                            self._expected_output_shape[index] = \
                                    Backend.shape(layer.outputs[outputs_index], dims=[dim_index],
                                                  runtimes=True)
                        break
                    else:
                        raise IndexError('`outputs_index` must lower than %d' % len(layer.outputs))

        if self._expected_output_shape is None:
            # @NOTE: since we don't have any requirement about the specific shape
            # of output, it would mean we must use batch_size here to perform
            # reshape. At this point, Reshape would act as Flatten

            used_batch_size = False
            total_size = 1

            for dim in input_shape:
                if dim is None:
                    if used_batch_size is False:
                        used_batch_size = True
                    else:
                        raise AssertionError('don\'t support shape with'
                                             'more than 1 None')
                else:
                    total_size *= dim
            else:
                self._outputs = [Backend.reshape(input_tensor,
                                                 shape=(batch_size, total_size))]
        else:
            # @NOTE: check and verify our shape, by default we will define our
            # shape as the way keras usually use: (batch_size,) + target_shape

            shape = [-1]
            for index, dim in enumerate(self._expected_output_shape):
                if dim < 0:
                    if shape[0] == -1:
                        shape[0] = Backend.shape(dims=[0], tensor=input_tensor, runtime=True)
                        shape.append(-1)
                    else:
                        raise AssertionError('reshape with none-compatible '
                                             'shape %s' % self._expected_output_shape)
                else:
                    shape.append(dim)
            self._outputs = [Backend.reshape(input_tensor, shape=shape)]
