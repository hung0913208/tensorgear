from ..layer import Layer
from ..backend import Backend

import numpy as np


class Dropout(Layer):
    def __init__(self, dropout_prob=0.25, name=None, **kwargs):
        super(Dropout, self).__init__(name=name, clsname='dropout', **kwargs)
        self._dropout_prob = dropout_prob
        self._dropout_placholder = None

    def __shape__(self, index=None, runtime=False):
        if index > 0:
            raise IndexError('`index` is out of range')
        else:
            return self._previous[0].shape(0, runtime=runtime)

    def switch(self, mode):
        if mode == 'train':
            return {self._dropout_placholder: self._dropout_prob}
        else:
            return {self._dropout_placholder: 0.0}

    def implement(self, is_training=False, batch_size=-1):
        if is_training is False:
            self._outputs = self._previous[0].outputs
        else:
            if len(self._previous) == 0:
                raise AssertionError('please add input before doing anything')
            elif len(self._previous) != 1:
                raise AssertionError('no support with more than one input')
            else:
                previous_layer = self._previous[0]

            if isinstance(previous_layer, Layer):
                input_tensor = previous_layer.outputs[0]
            elif isinstance(previous_layer, tf.Tensor):
                input_tensor = previous_layer

            self._dropout_placholder = Backend.implement('placeholder', 
                                                         default_value=1.0, 
                                                         shape=())
            self._outputs = [Backend.implement('dropout', name=self._name,
                                               input=input_tensor,
                                               keep_prob=self._dropout_placholder)]

