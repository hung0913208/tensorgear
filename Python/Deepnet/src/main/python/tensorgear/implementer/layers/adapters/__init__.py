class Adapter(object):
    def __init__(self):
        super(Adapter, self).__init__()
        pass

    def __call__(self, **kwargs):
        raise AssertionError('this is a virtual class')

from .static_rnn import StaticAdapter
from .dynamic_rnn import DynamicAdapter
from .bidirectional_rnn import BidirectionalAdapter

def builtin_adapters(adapter_name):
    mapping = {
        'dynamic': DynamicAdapter,
        'static': StaticAdapter,
        'bidirection': BidirectionalAdapter
    }

    requirements = {
        'dynamic': 1,
        'static': 1,
        'bidirection': 2
    }

    return {
        'applier': mapping.get(adapter_name)(),
        'requirement': requirements.get(adapter_name)
    }


__all__ = ['builtin_adapters']
