from ..layer import Layer
from ..backend import Backend

class Softmax(Layer):
    def __init__(self, name=None, **kwargs):
        super(Softmax, self).__init__(name=name, clsname='softmax', **kwargs)

    def implement(self, is_training=False, batch_size=-1):
        if len(self._previous) == 0:
            raise AssertionError('please add input before doing anything')
        elif len(self._previous) != 1:
            raise AssertionError('only support with 1 input')
        else:
            previous_layer = self._previous[0]

        if isinstance(previous_layer, Layer):
            input_tensor = previous_layer.outputs[0]

        self._outputs = [Backend.implement('softmax', inputs=input_tensor)]
