from ..layer import Layer
from ..backend import Backend

class Input(Layer):
    def __init__(self, dtype=None, shape=None, gate=None, name=None,
                 only_for_optimize=False, **kwargs):
        super(Input, self).__init__(name=name, clsname='input', **kwargs)
        self._gate = 0 if gate is None else gate
        self._only_for_optimize = only_for_optimize
        self._batch_size = None
        self._dtype = dtype
        self._shape = shape

    def __shape__(self, index=None, runtime=False):
        if not index is None and index > 0:
            raise IndexError('`index` is out of range')

        for _ in range(1):
            if len(self._outputs) == 0:
                if runtime is True:
                    self.implement(is_training=True)
                else:
                    return self._shape
            else:
                return Backend.shape(self._outputs[0], runtime)
        else:
            raise AssertionError('please, implement layer `%s` '
                                 'before get a runtime shape' % self.name)

    @property
    def gate(self):
        return self._gate

    @gate.setter
    def gate(self, value):
        self._gate = value

    @property
    def only_for_optimize(self):
        return self._only_for_optimize

    @only_for_optimize.setter
    def only_for_optimize(self, value):
        self._only_for_optimize = value

    def implement(self, is_training=False, batch_size=-1):
        if len(self._previous) > 0:
            self._outputs = self._previous[0].outputs
        elif len(self._outputs) > 0:
            pass
        else:
            shape = [None]

            if batch_size <= 0 and self._only_for_optimize is False:
                raise AssertionError('`batch_size` must not be lower or equal 0')

            if self._shape is None:
                # @NOTE: the shape is None mean we must define a scalar input

                self._outputs = [Backend.placeholder(dtype=self._dtype,
                                                     shape=[None,],
                                                     name=self._name)]
            else:
                # @NOTE: since the input's shape has been defined we must verify and
                # extract to the full shape that is commonly used on deeplearning

                for dim in self._shape:
                    if not dim is None or shape[0] is None:
                        shape.append(dim)

                        if dim is None:
                            # @NOTE: by default we always define dim `batch_size` to be
                            # None, however if user define other `None` dim we must
                            # assume that they will use specific `bacth_size`

                            if batch_size > 0:
                                shape[0] = batch_size
                            elif is_training:
                                shape[0] = 1
                            else:
                                del shape[0]
                    else:
                        raise AssertionError('`batch_size` must not be lower '
                                             'or equal 0')

                # @NOTE: everything is okey now, going to implement the output
                self._outputs = [Backend.placeholder(dtype=self._dtype,
                                                     shape=shape,
                                                     name=self._name)]
