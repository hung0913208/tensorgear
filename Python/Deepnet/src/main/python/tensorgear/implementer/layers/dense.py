from ..backend import Backend
from ..layer import Layer

class Dense(Layer):
    def __init__(self, units=None, name=None, activation=None,
                 weight_initializer='xavier', bias_initializer='zeros',
                 weight_regularizer=None, bias_regularizer=None,
                 name_of_variables=None, **kwargs):
        super(Dense, self).__init__(name=name, clsname='dense', **kwargs)
        if isinstance(weight_initializer, str):
            self._weight_initializer = Backend.initializer(weight_initializer)
        elif isinstance(weight_initializer, dict):
            self._weight_initializer = Backend.initializer(**weight_initializer)
        else:
            self._weight_initializer = None

        if isinstance(bias_initializer, str):
            self._bias_initializer = Backend.initializer(bias_initializer)
        elif isinstance(bias_initializer, dict):
            self._bias_initializer = Backend.initializer(**bias_initializer)
        else:
            self._bias_initializer = None

        if isinstance(weight_regularizer, str):
            self._weight_regularizer = {'op_name': weight_regularizer}
        elif isinstance(weight_regularizer, dict):
            self._weight_regularizer = weight_regularizer
        else:
            self._weight_regularizer = None

        if isinstance(bias_regularizer, str):
            self._bias_regularizer = {'op_name': bias_regularizer}
        elif isinstance(bias_regularizer, dict):
            self._bias_regularizer = bias_regularizer
        else:
            self._bias_regularizer = None

        self._name_of_variables = name_of_variables
        self._wrap_around = name_of_variables is None
        self._activation = Backend.activation(activation)
        self._units = units

    def implement(self, is_training=False, batch_size=-1):
        if len(self._previous) == 0:
            raise AssertionError('please add input before doing anything')
        elif len(self._previous) != 1:
            raise AssertionError('no support with more than one input')

        if self._wrap_around:
            with Backend.scope(self._name or self._clsname, reuse=True):
                self.__build__(is_training, batch_size)
        else:
            self.__build__(is_training, batch_size)

    def __shape__(self, index=None, runtime=False):
        if index > 0:
            raise IndexError('`index` is out of range')
        elif len(self._outputs) == 0:
            if runtime is True:
                return [self._previous[0].shape(index=0,
                                                dims=[0],
                                                runtime=runtime),
                        Backend.cast(self._units)]
            else:
                return [None, self._units]
        else:
            return Backend.shape(self._outputs[0], runtime)

    def __build__(self, is_training, batch_size):
        previous_layer = self._previous[0]
        weight_shape = []

        # @NOTE: fetching input_tensor from output of previous layer
        if isinstance(previous_layer, Layer):
            input_tensor = previous_layer.outputs[0]
            input_shape = previous_layer.shape(0)
        else:
            raise AssertionError('`previous_layer` must be a Layer object')

        # @NOTE: verify the input shape would be usefull or not
        if input_shape[0] is None and is_training is True:
            if batch_size < 0:
                raise AssertionError('batch_size mustn\'t be negative')
            elif batch_size == 0:
                raise AssertionError('batch_size mustn\'t be zero')

        # @NOTE: calculate variable `weight`
        weight_shape.append(input_shape[len(input_shape) - 1])
        weight_shape.append(self._units)

        if not self._weight_regularizer is None and is_training and self.trainable:
            weight_regularizer = Backend.regularizer(**self._weight_regularizer)
        else:
            weight_regularizer = None

        if self._name_of_variables is None or not 'weights' in self._name_of_variables:
            weights = Backend.variable(name='weights_of_%s' % self._name,
                                       initializer=self._weight_initializer(),
                                       regularizer=weight_regularizer,
                                       shape=weight_shape,
                                       trainable=self._trainable,
                                       dtype=Backend.dtype(input_tensor))
        else:
            weights = Backend.variable(name=self._name_of_variables['weights'],
                                       initializer=self._weight_initializer(),
                                       regularizer=weight_regularizer,
                                       shape=weight_shape,
                                       trainable=self._trainable,
                                       dtype=Backend.dtype(input_tensor))
        self._weights.append(weights)

        # @NOTE: apply everything to backend
        if self._activation is None and self._bias_initializer is None and self._wrap_around is False:
            with Backend.scope(self._name or self._clsname):
                # @NOTE: implement `matmul`

                matmul = Backend.implement('matmul', left=input_tensor, right=weights)
        else:
            matmul = Backend.implement('matmul', left=input_tensor, right=weights)

        if not self._bias_initializer is None:
            bias_shape = [self._units]

            # @NOTE: calculate variable `bias`
            if not self._bias_regularizer is None and is_training and self.trainable:
                bias_regularizer = Backend.regularizer(**self._bias_regularizer)
            else:
                bias_regularizer = None

            if self._name_of_variables is None or not 'bias' in self._name_of_variables:
                bias = Backend.variable(name='bias_of_%s' % self._name,
                                        initializer=self._bias_initializer(),
                                        regularizer=bias_regularizer,
                                        shape=bias_shape,
                                        trainable=self._trainable,
                                        dtype=Backend.dtype(input_tensor))
            else:
                bias = Backend.variable(name=self._name_of_variables['bias'],
                                        initializer=self._bias_initializer(),
                                        regularizer=bias_regularizer,
                                        shape=bias_shape,
                                        trainable=self._trainable,
                                        dtype=Backend.dtype(input_tensor))
            self._weights.append(bias)

            if self._wrap_around is False:
                with Backend.scope(self._name or self._clsname):
                    add = Backend.implement('add', left=matmul, right=bias)

                    if self._activation is None:
                        output = add
                    else:
                        output = self._activation(add)
            else:
                add = Backend.implement('add', left=matmul, right=bias)

                if self._activation is None:
                    output = add
                else:
                    output = self._activation(add)
        else:
            if self._wrap_around is False:
                with Backend.scope(self._name or self._clsname):
                    if self._activation is None:
                        output = matmul
                    else:
                        output = self._activation(matmul)
            else:
                if self._activation is None:
                    output = matmul
                else:
                    output = self._activation(matmul)

        self._outputs = [output]
