import numpy as np

from ..layer import Layer
from ..backend import Backend

class MaxPool2D(Layer):
    def __init__(self, ksize=[2, 2], strides=[2, 2], padding='same', 
                 data_format='channel_last', name=None, **kwargs):
        super(MaxPool2D, self).__init__(name=name, clsname='maxpool', **kwargs)
        self._channel_position = 3 if data_format == 'channel_last' else 0
        self._padding = padding
        self._strides = strides
        self._ksize = ksize

    def __shape__(self, index=None, runtime=False):
        if index > 0:
            raise IndexError('`index` is out of range')

        if self._channel_position == 3:
            input_n, input_h, input_w, input_c = \
                    self._previous[0].shape(0, dims=[0, 1, 2, 3],
                                            runtime=runtime)
        else:
            input_n, input_c, input_h, input_w = \
                    self._previous[0].shape(0, dims=[0, 1, 2, 3],
                                            runtime=runtime)
        if runtime:
            output_h = Backend.cast(input_h, np.float32)
            output_w = Backend.cast(input_w, np.float32)

            if self._padding.lower() == 'same':
                output_h = Backend.implement('div', left=output_h,
                                             right=Backend.cast(self._strides[0]))
                output_w = Backend.implement('div', left=output_w,
                                             right=Backend.cast(self._strides[1]))
                output_h = Backend.implement('round', x=output_h)
                output_w = Backend.implement('round', x=output_w)
            else:
                output_h = Backend.implement('sub', left=output_h,
                                             right=Backend.cast(self._ksize[0]))
                output_w = Backend.implement('sub', left=output_w,
                                             right=Backend.cast(self._ksize[1]))
                output_h = Backend.implement('div', left=output_h,
                                             right=Backend.cast(self._strides[0]))
                output_w = Backend.implement('div', left=output_w,
                                             right=Backend.cast(self._strides[1]))
            
        else:
            fixed = 0.0 if self._padding.lower() == 'same' else 1.0
            output_h = math.round((input_h - fixed*self._ksizes[0])/self._strides[0] \
                                  + fixed)
            output_w = math.round((input_w - fixed*self._ksizes[1])/self._strides[1] \
                                  + fixed)

        if self._channel_position == 3:
            return [input_n, output_h, output_w, input_c]
        else:
            return [input_n, input_c, output_h, output_w]

    def implement(self, is_training=False, batch_size=-1):
        if len(self._previous) == 0:
            raise AssertionError('please add input before doing anything')
        elif len(self._previous) != 1:
            raise AssertionError('no support with more than one input')
        else:
            previous_layer = self._previous[0]

        if isinstance(previous_layer, Layer):
            input_tensor = previous_layer.outputs[0]
        elif isinstance(previous_layer, tf.Tensor):
            input_tensor = previous_layer

        ksize = [1, self._ksize[0], self._ksize[1], 1]
        strides = [1, self._strides[0], self._strides[1], 1]
        data_format = 'NHWC' if self._channel_position == 3 else 'NCHW'

        self._outputs = [Backend.implement('max_pool', name=self._name,
                                          input=input_tensor,
                                          ksize=ksize, strides=strides,
                                          padding=self._padding.upper(),
                                          data_format=data_format)]
