class Train(object):
    def __init__(self, backend, optimizer, **kwargs):
        self._optimizer = optimizer
        self._backend = backend
        self._kwargs = kwargs
        self._saver = None

    def __enter__(self):
        self._backend.__start_train__()
        if 'checkpoint' in self._kwargs:
            self._saver = self._backend.__load_saver__(self._kwargs['checkpoint'], self,
                                                       self._kwargs['saver'],
                                                       self._kwargs['variables'])

        if callable(self._optimizer):
            self._backend.__start_train__(self._optimizer().variables())
        return self

    def __exit__(self, type, value, tb):
        if 'checkpoint' in self._kwargs:
            checkpoint = self._kwargs['checkpoint']

            if 'saver' in self._kwargs:
                kwargs = self._kwargs['saver']

            if isinstance(kwargs, dict):
                self._backend.__dump_saver__(saver=self._saver,
                                             checkpoint=checkpoint,
                                             **kwargs)

    def run(self, fetches, feed_dict=[], **kwargs):
        return self._backend.__run__(fetches=fetches, feed_dict=feed_dict,
                                     **kwargs)

class Backend(object):
    _instance = None

    def __new__(clss, *args, **kwargs):
        if kwargs.get('backend') is None:
            if clss._instance is None:
                clss._instance = super(Backend, clss).__new__(clss)
        else:
            clss._instance = kwargs.get('backend')
        return clss._instance

    def __init__(self):
        super(Backend, self).__init__()
        self._session = None
        self._objects = {}

    def __enter__(self):
        if Backend._instance is None:
            raise AssertionError('please install a new backend before doing anything')
        Backend._instance.__reset__()
        return Backend._instance

    def __exit__(self, type, value, tb):
        if Backend._instance is None:
            raise AssertionError('please install a new backend before doing anything')
        return Backend._instance.__deinit__()

    def __deinit__(self):
        pass

    def __reset__(self, **kwargs):
        raise AssertionError('this is a virtual class')

    def __start_train__(self, variables=None):
        raise AssertionError('this is a virtual class')

    def __activation__(self, name, **kwargs):
        raise AssertionError('this is a virtual class')

    def __initializer__(self, name, **kwargs):
        raise AssertionError('this is a virtual class')

    def __regularizer__(self, op_name, **kwargs):
        raise AssertionError('this is a virtual class')

    def __implement__(self, op_name, **kwargs):
        raise AssertionError('this is a virtual class')

    def __variable_scope__(self, name, **kwargs):
        raise AssertionError('this is a virtual class')

    def __name_scope__(self, name, **kwargs):
        raise AssertionError('this is a virtual class')

    def __load_saver__(self, checkpoint, callbacks):
        raise AssertionError('this is a virtual class')

    def __dump_saver__(self, checkpoint, **kwargs):
        raise AssertionError('this is a virtual class')

    def __variable__(self, **kwargs):
        raise AssertionError('this is a virtual class')

    def __placeholder__(self, **kwargs):
        raise AssertionError('this is a virtual class')

    def __reshape__(self, tensor, **kwargs):
        raise AssertionError('this is a virtual class')

    def __transpose__(self, tensor, **kwargs):
        raise AssertionError('this is a virtual class')

    def __run__(self, fetches, feed_dict):
        raise AssertionError('this is a virtual class')

    def __shape__(self, tensor, runtime, dims):
        raise AssertionError('this is a virtual class')

    def __summaries__(self, op_name, **kwargs):
        raise AssertionError('this is a virtual class')

    def __dtype__(self, tensor):
        raise AssertionError('this is a virtual class')

    def __cast__(self, tensor, type):
        raise AssertionError('this is a virtual class')

    def __type__(self, name):
        raise AssertionError('this is a virtual class')

    def __global_step_incrementer__(self):
        raise AssertionError('this is a virtual class')

    def __restore__(self, checkpoint, is_artifact=False):
        raise AssertionError('this is a virtual class')

    def __name__(self, object, shorten=False):
        raise AssertionError('this is a virtual class')

    def __write_graph__(self, output, gates, as_text=False):
        raise AssertionError('this is a virtual class')

    def __gradient_helper__(self, optimizer, loss_function, variable_list):
        raise AssertionError('this is a virtual class')

    @staticmethod
    def activation(name, **kwargs):
        if Backend._instance is None:
            raise AssertionError('Please provide a backend before '
                                 'doing anything')
        return Backend._instance.__activation__(name, **kwargs)

    @staticmethod
    def initializer(name, **kwargs):
        if Backend._instance is None:
            raise AssertionError('Please provide a backend before '
                                 'doing anything')
        return Backend._instance.__initializer__(name, **kwargs)

    @staticmethod
    def regularizer(op_name, **kwargs):
        if Backend._instance is None:
            raise AssertionError('Please provide a backend before '
                                 'doing anything')
        return Backend._instance.__regularizer__(op_name, **kwargs)

    @staticmethod
    def implement(op_name, **kwargs):
        if Backend._instance is None:
            raise AssertionError('Please provide a backend before '
                                 'doing anything')
        return Backend._instance.__implement__(op_name, **kwargs)

    @staticmethod
    def scope(name, **kwargs):
        if Backend._instance is None:
            raise AssertionError('Please provide a backend before '
                                 'doing anything')
        if 'reuse' in kwargs:
            return Backend._instance.__variable_scope__(name=name, **kwargs)
        else:
            return Backend._instance.__name_scope__(name=name, **kwargs)

    @staticmethod
    def variable(name=None, **kwargs):
        if Backend._instance is None:
            raise AssertionError('Please provide a backend before '
                                 'doing anything')
        return Backend._instance.__variable__(name=name, **kwargs)

    @staticmethod
    def placeholder(name=None, **kwargs):
        if Backend._instance is None:
            raise AssertionError('Please provide a backend before '
                                 'doing anything')
        return Backend._instance.__placeholder__(name=name, **kwargs)

    @staticmethod
    def reshape(tensor, **kwargs):
        if Backend._instance is None:
            raise AssertionError('Please provide a backend before '
                                 'doing anything')
        return Backend._instance.__reshape__(tensor, **kwargs)

    @staticmethod
    def transpose(tensor, perm, **kwargs):
        if Backend._instance is None:
            raise AssertionError('Please provide a backend before '
                                 'doing anything')
        return Backend._instance.__transpose__(tensor, perm=perm, **kwargs)

    @staticmethod
    def shape(tensor, runtime=False, dims=None):
        if Backend._instance is None:
            raise AssertionError('Please provide a backend before '
                                 'doing anything')
        result = Backend._instance.__shape__(tensor, runtime=runtime, dims=dims)
        if result is None:
            return None
        elif dims is None:
            return result
        else:
            return result[0] if len(result) == 1 else result

    @staticmethod
    def type(name):
        if Backend._instance is None:
            raise AssertionError('Please provide a backend before '
                                 'doing anything')
        return Backend._instance.__type__(name)

    @staticmethod
    def get(name):
        if Backend._instance is None:
            raise AssertionError('Please provide a backend before '
                                 'doing anything')
        if name in Backend._instance._objects:
            return Backend._instance._objects[name]
        else:
            return None

    @staticmethod
    def update(name, value):
        if Backend._instance is None:
            raise AssertionError('Please provide a backend before '
                                 'doing anything')
        elif name in Backend._instance._objects:
            if isinstance(Backend._instance._objects[name], list):
                Backend._instance._objects[name].append(value)
            else:
                Backend._instance._objects[name] = [value]
        else:
            Backend._instance._objects[name] = [value]

    @staticmethod
    def set(name, value):
        if Backend._instance is None:
            raise AssertionError('Please provide a backend before '
                                 'doing anything')
        else:
            Backend._instance._objects[name] = value

    @staticmethod
    def session():
        if Backend._instance is None:
            raise AssertionError('Please provide a backend before '
                                 'doing anything')
        else:
            return Backend._instance._session

    @staticmethod
    def do_fiting(checkpoint, saver, optimizer=None, **kwargs):
        if Backend._instance is None:
            raise AssertionError('Please provide a backend before '
                                 'doing anything')
        return Train(Backend._instance,
                     checkpoint=checkpoint,
                     optimizer=optimizer,
                     saver=saver,
                     **kwargs)

    @staticmethod
    def summaries(op_name, **kwargs):
        if Backend._instance is None:
            raise AssertionError('Please provide a backend before '
                                 'doing anything')
        return Backend._instance.__summaries__(op_name, **kwargs)

    @staticmethod
    def dtype(tensor):
        if Backend._instance is None:
            raise AssertionError('please install a new backend before doing anything')
        return Backend._instance.__dtype__(tensor)

    @staticmethod
    def cast(tensor, type):
        if Backend._instance is None:
            raise AssertionError('please install a new backend before doing anything')
        return Backend._instance.__cast__(tensor, type)

    @staticmethod
    def global_step_incrementer():
        if Backend._instance is None:
            raise AssertionError('please install a new backend before doing anything')
        return Backend._instance.__global_step_incrementer__()

    @staticmethod
    def restore(checkpoint, artifact=False):
        if Backend._instance is None:
            raise AssertionError('please install a new backend before doing anything')
        return Backend._instance.__restore__(checkpoint, artifact)

    @staticmethod
    def write_graph(output, gates, as_text=False):
        if Backend._instance is None:
            raise AssertionError('please install a new backend before doing anything')
        return Backend._instance.__write_graph__(output, gates, as_text=as_text)

    @staticmethod
    def evaluate(fetcher, feed_dict=[]):
        if Backend._instance is None:
            raise AssertionError('please install a new backend before doing anything')
        return Backend._instance.__run__(fetcher, feed_dict)

    @staticmethod
    def gradient_helper(optimizer, loss_function, variable_list=None):
        if Backend._instance is None:
            raise AssertionError('please install a new backend before doing anything')
        return Backend._instance.__gradient_helper__(optimizer, loss_function,
                                                     variable_list)

    @staticmethod
    def use(backend, instance=None):
        if Backend._instance is None:
            if isinstance(backend, Backend):
                Backend._instance = backend

                if callable(instance):
                    instance()
            else:
                raise AssertionError('`backend` must be inherited '
                                     'from `Instance`')
        elif backend != Backend._instance:
            raise AssertionError('Please close the previouse `backend`'
                                 ' before use another')
        return Backend._instance

    @staticmethod
    def name(object, shorten=False):
        if Backend._instance is None:
            raise AssertionError('There is nothing to do since backend has '
                                 'been closed recently')
        else:
            return Backend._instance.__name__(object, shorten)

    @staticmethod
    def reset(back_to_default=False, **kwargs):
        if Backend._instance is None:
            raise AssertionError('There is nothing to do since backend has '
                                 'been closed recently')
        else:
            Backend._instance.__reset__(back_to_default=back_to_default, **kwargs)

    @staticmethod
    def done():
        if Backend._instance is None:
            raise AssertionError('There is nothing to do since backend has '
                                 'been closed recently')
        else:
            Backend._instance.__deinit__()
            Backend._instance = None
