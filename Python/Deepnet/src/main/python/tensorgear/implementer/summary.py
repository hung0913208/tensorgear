from .logits import Logits
from .layer import Layer
from .objective import Objective
from .backend import Backend
from .utils import is_input_layer

import numpy as np


class Summary(object):
    def __init__(self, estimator):
        super(Summary, self).__init__()
        self._estimator = estimator
        self._predicted_indices = []
        self._beholder = None
        self._visualizers = {
            'verify': [],
            'scalar': [],
            'tracking': [],
            'debug': None
        }

    def summary(self, obj, type=None):
        with Backend.scope(scope, use_default_graph=True):
            if isinstance(obj, Layer):
                for output in obj.outputs:
                    Backend.summaries('tensor', input=weight)

                for weight in obj.weights:
                    Backend.summaries('tensor', input=weight)
            elif isinstance(obj, Objective):
                for output in obj.outputs:
                    Backend.summaries('scalar', input=output)
            elif isinstance(obj, Backend.type('tensor')):
                if type == 'audio':
                    Backend.summaries('audio', input=output)
                elif type == 'image':
                    Backend.summaries('image', input=output)
                elif type == 'text':
                    Backend.summaries('text', input=output)
                else:
                    Backend.summaries('tensor', input=output)

    @property
    def debug(self):
        return self._visualizers['debug']

    @property
    def predicted_indices(self):
        return self._predicted_indices

    @property
    def estimator(self):
        return self._estimator

    def reset(self):
        self._predicted_indices = []

    def does_need_to_invoke(self, type):
        if type in self._visualizers:
            return len(self._visualizers[type]) > 0
        else:
            return False

    def implement(self, scope, loss_formular, where_to_write=None):
        evaluations = []

        with Backend.scope(scope, use_default_graph=True):
            entropies = []

            # @NOTE: get objective from optimizer and load approviated predicters
            for layer in self._estimator.optimizer.layers:
                if isinstance(layer, Objective):
                    entropies.append(layer)

            # @NOTE: calculate accuracy of Logits' outputs
            for index, predict in enumerate(self._estimator.logits.outputs):
                objective_ = None

                if len(entropies) == 0:
                    for layer in self._estimator.optimizer.layers:
                        if is_input_layer(layer) and layer.name == predict.name:
                            objective_ = self._estimator.optimizer.objective
                            break
                    else:
                        raise AssertionError('not found `%s`' % predict.name)
                else:
                    for objective in entropies:
                        if objective.ground_truth is None:
                            continue
                        elif objective.ground_truth.name == predict.name:
                            objective_ = objective
                            break
                    else:
                        raise AssertionError('not found `%s`' % predict.name)

                # @NOTE: load ground_truth layers from optimizer
                for layer in self._estimator.optimizer.layers:
                    if is_input_layer(layer) and layer.name == predict.name:
                        ground_truth = layer.outputs[0]
                        break

                # @NOTE: everything about estimate the quality of our model will be granded
                # to the objective
                correct_prediction, predicted_indices = \
                        objective_.estimate(predict.outputs[0], ground_truth)

                if self._estimator.model_type == 'classification':
                    # @NOTE: store _predicted_indices to use later
                    self._predicted_indices.append(predicted_indices)

                    # @NOTE: make a comparision between predict and ground truth
                    casted = Backend.cast(correct_prediction, np.float32)
                    reduce_mean = Backend.implement('reduce_mean', input=casted)

                    # @NOTE: store them to a list
                    evaluations.append(100.0*reduce_mean)

                    # @NOTE: watch result of evaluations
                    Backend.summaries('scalar', name='accuracy-%s' % predict.name,
                                      input=evaluations[index])
                elif self._estimator.model_type == 'regression':
                    # @TODO: please implement this feature
                    pass
                elif self._estimator.model_type == 'sequence':
                    for error_rate in predicted_indices:
                        if isinstance(error_rate, Backend.type('tensor')):
                            Backend.summaries('scalar', name=Backend.name(error_rate),
                                              input=error_rate)
                            evaluations.append(error_rate)

                    if len(evaluations) < len(predicted_indices):
                        if isinstance(predicted_indices, list):
                            callbacks = predicted_indices
                        else:
                            callbacks = [predicted_indices]

                        def wrapping(writer, objective, ypreds, ytruths, global_step):
                            if objective != objective_:
                                return
                            else:
                                callback = callbacks[index]

                            if isinstance(callback, Backend.type('tensor')):
                                return
                            elif isinstance(callback, tuple):
                                tag, callback = callback
                            else:
                                tag = callback.__name__

                            value=callback(ypreds, ytruths)
                            value = Backend.implement('value', tag=tag,
                                                      value=callback(ypreds, ytruths))
                            writer.add_summary(tf.Summary(value))
                        self._visualizers['scalar'].append(wrapping)

            # @NOTE: watch result of loss function
            Backend.summaries('scalar', name='total loss', input=loss_formular)

            # @NOTE: if some loss function implement more than 2 output, it would
            # mean it has another way to messure how far between predicting and
            # real values
            for index, layer in enumerate(self._estimator.optimizer.layers):
                if isinstance(layer, Objective):
                    for loss in layer.outputs[1:]:
                        mean = Backend.implement('reduce_mean', input=loss)

                        Backend.summaries('scalar',
                                          name='{}::{}'.format(layer.name, loss.name),
                                          input=mean)

            # @NOTE: some tracking object must be implemented before doing
            # anything
            for tracking in self._visualizers['tracking']:
                try:
                    tracking.implement()
                except Exception:
                    pass

        merged = Backend.summaries('merge_all', scope=scope)

        # @NOTE: define writers which were used to store result from summaries
        if not where_to_write is None:
            if isinstance(where_to_write, dict):
                writers = {}

                for name in where_to_write:
                    writers[name] = Backend.summaries('FileWriter',
                                                      path=where_to_write[name],
                                                      write_graph=True)
            elif isinstance(where_to_write, str):
                writers = Backend.summaries('FileWriter', path=where_to_write)
            else:
                raise AssertionError('`where_to_write` must be a list, tuple or str')
        else:
            writers = None
        return merged, evaluations, writers, None

    def logtrace(self, where_to_write):
        # @NOTE: use beholder to watch how our model has been trained

        if self._beholder is None:
            self._beholder = Backend.summaries('beholder', savepoint=where_to_write)

            if self._beholder:
                self._visualizers['tracking'].append(self.__beholder__)

    def visualize(self, type, visualizer):
        if type in self._visualizers:
            if type == 'verify' and self._estimator.model_type != 'classification':
                raise AssertionError('model %s don\'t support `verify` method')

            if callable(visualizer):
                self._visualizers[type].append(visualizer)
            else:
                self._visualizers[type] = visualizer
        else:
            raise AssertionError('no support type `%s`' % type)

    def __beholder__(self, list_of_np_arrays=None, two_dimensional_np_array=None):
        if not self._beholder is None:
            self._beholder.update(session=Backend.session(),
                                  arrays=list_of_np_arrays,
                                  frame=two_dimensional_np_array)

    def __call__(self, type, **kwargs):
        if type in self._visualizers:
            for callback in self._visualizers[type]:
                if callable(callback):
                    callback(**kwargs)
        else:
            raise AssertionError('no support type `%s`' % type)
