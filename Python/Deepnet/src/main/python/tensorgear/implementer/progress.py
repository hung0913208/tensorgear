class Task(object):
    def __init__(self, total, context, prefix):
        super(Task, self).__init__()
        self._containers = {}
        self._callbacks = []
        self._finsish = 0
        self._total = total
        self._prefix = prefix
        self._context = context

    def __del__(self):
        self._context._callbacks = self._callbacks
        self._context._containers = self._containers

    @property
    def total(self):
        return self._total

    def inc(self):
        self._finsish += 1
        self._context.render(percent=100.0*self._finsish/self._total)

    def dec(self):
        self._finsish -= 1
        self._context.render(percent=100.0*self._finsish/self._total)


class Progress(object):
    def __init__(self):
        super(Progress, self).__init__()
        self._containers = {}
        self._callbacks = []
        self._silence = False

    @property
    def silence(self):
        return self._silence

    @silence.setter
    def silence(self, value):
        self._silence = value

    def reset(self, task=None):
        if not task is None:
            import copy

            task._containers = self._containers.copy()
            task._callbacks = copy.copy(self._callbacks)

        if len(self._callbacks) > 0:
            print('\n\r')

        self._containers = {}
        self._callbacks = []

    def __del__(self):
        if len(self._callbacks) > 0:
            print('\n\r')

    def rotate(self, prefix=None):
        if len(self._callbacks) != 0:
            raise AssertionError('`rotate` must run first, please use suffix')

        @self.hook
        def wrapper(**kwargs):
            if 'rotate_renders' in kwargs:
                renders = kwargs['rotate_renders']
            else:
                renders = ['|', '/', '-', '\\', '|', '/', '-', '\\']

            if 'rotate_step' in self._containers:
                if self._containers['rotate_step'] < 7:
                    self._containers['rotate_step'] += 1
                else:
                    self._containers['rotate_step'] = 0
            else:
                self._containers['rotate_step'] = 0

            if not 'prefix' in kwargs:
                return ' [%s] %s:' % (renders[self._containers['rotate_step']],
                                      prefix)
            elif prefix is None:
                return ' [%s]' % renders[self._containers['rotate_step']]
            else:
                if prefix is None:
                    return ' [%s] %s:' % (renders[self._containers['rotate_step']],
                                        kwargs['prefix'])
                else:
                    return ' [%s] %s, %s:' % (renders[self._containers['rotate_step']],
                                            prefix, kwargs['prefix'])

    def percent(self, keyword='percent'):
        @self.hook
        def wrapper(**kwargs):
            if keyword == 'percent':
                return ' %.2f%%' % kwargs[keyword]
            elif not keyword in kwargs:
                return ''
            elif isinstance(kwargs[keyword], list):
                result = ''
                count = 0

                for item in kwargs[keyword]:
                    if not item is None:
                        result += ' %.2f%%' % item
                        count += 1
                else:
                    if count > 1:
                        return ', %s: [%s ]' % (keyword, result)
                    else:
                        return ', %s: %s' % (keyword, result)
                return ''
            else:
                return ', %s: %.2f%%' % (keyword, kwargs[keyword])

    def value(self, keyword):
        @self.hook
        def wrapper(**kwargs):
            if not keyword in kwargs:
                return ''
            elif isinstance(kwargs[keyword], list):
                result = ''
                count = 0

                for item in kwargs[keyword]:
                    if not item is None:
                        result += '%.4f' % item
                        count += 1
                else:
                    if count > 1:
                        return ', %s: [%s]' % (keyword, result)
                    else:
                        return ', %s: %s' % (keyword, result)
            else:
                return ', %s: %.4f' % (keyword, kwargs[keyword])

    def bar(self, length, keyword='percent', fill='#', empty='-'):
        @self.hook
        def wrapper(**kwargs):
            percent = kwargs[keyword]
            remaining = int(length * (1.0 - percent/100.0))
            passed = length - remaining

            if keyword == 'percent':
                return ' |%s|' % (fill * passed + remaining * empty)
            else:
                return ', %s: |%s|' % (keyword, (fill * passed + remaining * empty))

    def prefix(self, prefix):
        if len(self._callbacks) != 0:
            raise AssertionError('`prefix` must run first, please use suffix')

        @self.hook
        def wrapper(**kwargs):
            if not 'prefix' in kwargs:
                return ' %s:' % prefix
            else:
                return ' %s:' % kwargs['prefix']

    def suffix(self, suffix):
        if len(self._callbacks) == 0:
            raise AssertionError('`suffix` never runs first, please use prefix')

        @self.hook
        def wrapper(**kwargs):
            if not 'suffix' in kwargs:
                return ' %s' % suffix
            else:
                return ' %s' % kwargs['suffix']

    def time_track(self):
        @self.hook
        def wrapper(**kwargs):
            import time

            if 'start_time' in self._containers:
                time_per_step = time.time() - self._containers['start_time']
            else:
                time_per_step = None

            self._containers['start_time'] = time.time()
            if time_per_step is None:
                return ''
            else:
                return ', %.2f step/s' % (1.0/time_per_step)

    def progress(self, total_step, prefix=None):
        return Task(total_step, context=self, prefix=prefix)

    def render(self, **kwargs):
        progress = ''

        for callback in self._callbacks:
            progress += callback(**kwargs)
        else:
            import sys

            if self._silence is False:
                sys.stdout.write('\r>> %s' % progress)
                sys.stdout.flush()

    def hook(self, function):
        def wrapper(*args, **kwargs):
            return function(*args, **kwargs)

        self._callbacks.append(function)
        return wrapper
