from ...backend import *

import tensorflow as tf
import numpy as np

class Tensorflow(Backend):
    def __init__(self, parallelism_threads=None):
        super(Tensorflow, self).__init__()
        self._session = None

        if not parallelism_threads is None:
            self._config = tf.ConfigProto(intra_op_parallelism_threads=parallelism_threads)
        else:
            self._config = None

        if self._config is None:
            self._session = tf.InteractiveSession()
        else:
            self._session = tf.InteractiveSession(config=self._config)
        self.__reset__()

    def __on_gpu__(self):
        for dev in self._session.list_devices():
            if dev.device_type.lower() == 'gpu':
                return True
        else:
            return False

    def __on_cpu__(self):
        for dev in self._session.list_devices():
            if dev.device_type.lower() != 'cpu':
                return False
        else:
            return True

    def __deinit__(self):
        if not self._session is None:
            self._session.close()
            self._session = None
        tf.reset_default_graph()

    def __reset__(self, back_to_default=False, debug=None):
        if back_to_default or isinstance(debug, str):
            self.__deinit__()

        if self._session is None:
            if self._config is None:
                sess = tf.InteractiveSession()

                if isinstance(debug, str):
                    try:
                        from tensorflow.python import debug as tfdbg
                        self._session = tfdbg.TensorBoardDebugWrapperSession(sess, debug)
                    except Exception:
                        self._session = sess
                else:
                    self._session = sess
            else:
                sess = tf.InteractiveSession(config=self._config)

                if isinstance(debug, str):
                    try:
                        from tensorflow.python import debug as tfdbg
                        self._session = tfdbg.TensorBoardDebugWrapperSession(sess, debug)
                    except Exception:
                        self._session = sess
                else:
                    self._session = sess

    def __start_train__(self, variables=None):
        if variables is None:
            self._session.run(tf.global_variables_initializer())
        else:
            self._session.run(tf.variables_initializer(variables))

    def __activation__(self, name, **kwargs):
        if name is None:
            return None
        elif name == 'relu':
            return tf.nn.relu
        elif name == 'relu6':
            return tf.nn.relu6
        elif name == 'leaky_relu':
            def wrapper(features):
                return tf.nn.leaky_relu(**kwargs)
            return wrapper
        elif name == 'elu':
            return tf.nn.elu
        elif name == 'tanh':
            return tf.nn.tanh
        elif name == 'sigmoid':
            return tf.nn.sigmoid
        elif name == 'softmax':
            return tf.nn.softmax
        else:
            raise AssertionError('no support `%s`' % name)

    def __initializer__(self, name, **kwargs):
        if name == 'xavier':
            return tf.contrib.layers.xavier_initializer
        elif name == 'glorot_normal':
            return tf.glorot_normal_initializer
        elif name == 'glorot_uniform':
            return tf.glorot_uniform_initializer
        elif name == 'he_normal':
            return tf.contrib.layers.variance_scaling_initializer
        elif name == 'he_uniform':
            pass
        elif name == 'random_uniform':
            return tf.random_uniform_initializer
        elif name == 'random_normal':
            return tf.random_normal_initializer
        elif name == 'zeros' or name is None:
            return tf.zeros_initializer
        elif name == 'truncated_normal':
            def truncated_normal():
                return tf.truncated_normal_initializer(**kwargs)
            return truncated_normal
        else:
            return None

    def __regularizer__(self, op_name, **kwargs):
        if op_name == 'l1':
            return tf.contrib.layers.l1_regularizer(scale=(kwargs.get('scale') or 0.1))
        elif op_name == 'l2':
            return tf.contrib.layers.l2_regularizer(scale=(kwargs.get('scale') or 0.1))
        else:
            return None

    def __implement__(self, op_name, name=None, **kwargs):
        if op_name == 'placeholder':
            def_value = kwargs.get('default_value')
            shape = kwargs.get('shape')
            dtype = kwargs.get('dtype')

            if def_value is None:
                if dtype is None:
                    raise AssertionError('`dtype` mustn\'t be None')

                return tf.placeholder(dtype=dtype, shape=shape, name=name)
            else:
                return tf.placeholder_with_default(input=def_value, shape=shape,
                                                   name=name)
        elif op_name == 'identity':
            input = kwargs.get('input')

            if input is None:
                raise AssertionError('`input` mustn\'t be None')
            return tf.identity(input=input, name=name)
        elif op_name == 'pad':
            input = kwargs.get('input')
            paddings = kwargs.get('paddings')

            if input is None:
                raise AssertionError('`input` mustn\'t be None')
            elif paddings is None:
                raise AssertionError('`paddings` mustn\'t be None')

            return tf.pad(tensor=input, paddings=paddings, name=name)
        elif op_name == 'variables':
            cell = kwargs.get('cell')

            if not cell is None:
                return cell.variables
            else:
                return []
        elif op_name == 'slice':
            tensor = kwargs.get('tensor')
            begin = kwargs.get('begin')
            size = kwargs.get('size')

            if tensor is None:
                raise AssertionError('`tensor` mustn\'t be None')
            elif begin is None:
                raise AssertionError('`begin` mustn\'t be None')
            elif size is None:
                raise AssertionError('`size` mustn\'t be None')

            return tf.slice(tensor, begin=begin, size=size, name=name)
        elif op_name == 'softmax':
            logits = kwargs.get('logits')

            if logits is None:
                raise AssertionError('`logits` mustn\'t be None')
            return tf.nn.softmax(logits, name=name)
        elif op_name == 'sparse_softmax_cross_entropy':
            labels = kwargs.get('labels')
            logits = kwargs.get('logits')

            if labels is None:
                raise AssertionError('`labels` mustn\'t be None')
            elif logits is None:
                raise AssertionError('`logits` mustn\'t be None')
            else:
                return tf.losses.sparse_softmax_cross_entropy(labels=labels,
                                                              logits=logits)
        elif op_name == 'argmax':
            input = kwargs.get('input')
            axis = kwargs.get('axis') or 0

            if input is None:
                raise AssertionError('`input` mustn\'t be None')

            return tf.argmax(input, axis=axis)
        elif op_name == 'equal':
            left = kwargs.get('left')
            right = kwargs.get('right')

            if left is None:
                raise AssertionError('`left` mustn\'t be None')
            elif right is None:
                raise AssertionError('`right` mustn\'t be None')
            return tf.equal(x=left, y=right)
        elif op_name == 'add':
            left = kwargs.get('left')
            right = kwargs.get('right')

            if left is None:
                raise AssertionError('`left` mustn\'t be None')
            elif right is None:
                raise AssertionError('`right` mustn\'t be None')
            else:
                return tf.add(left, right, name=name)
        elif op_name == 'sub':
            left = kwargs.get('left')
            right = kwargs.get('right')

            if left is None:
                raise AssertionError('`left` mustn\'t be None')
            elif right is None:
                raise AssertionError('`right` mustn\'t be None')
            else:
                return tf.subtract(left, right, name=name)
        elif op_name == 'maximum':
            left = kwargs.get('left')
            right = kwargs.get('right')

            if left is None:
                raise AssertionError('`left` mustn\'t be None')
            elif right is None:
                raise AssertionError('`right` mustn\'t be None')

            return tf.maximum(left, right)
        elif op_name == 'conv2d':
            input = kwargs.get('input')
            filter = kwargs.get('filter')
            strides = kwargs.get('strides') or [1, 1, 1, 1]
            padding = (kwargs.get('padding') or 'valid').upper()
            dilations = kwargs.get('dilations') or [1, 1, 1, 1]
            data_format = kwargs.get('data_format') or 'NHWC'

            if input is None:
                raise AssertionError('`input` mustn\'t be None')
            elif filter is None:
                raise AssertionError('`filter` mustn\'t be None')
            else:
                return tf.nn.conv2d(input=input, filter=filter,
                                    strides=strides, padding=padding,
                                    dilations=dilations, data_format=data_format,
                                    name=name)
        elif op_name == 'matmul':
            left = kwargs.get('left')
            right = kwargs.get('right')

            if left is None:
                raise AssertionError('`left` mustn\'t be None')
            elif right is None:
                raise AssertionError('`right` mustn\'t be None')
            else:
                last_dim = right.get_shape().as_list()[-1]
                rank = len(left.shape)

            if rank > 2:
                # Broadcasting is required for the inputs.
                outputs = tf.tensordot(left, right, [[rank - 1], [0]])

                # Reshape the output back to the original ndim of the input.
                if not tf.executing_eagerly():
                    shape = left.get_shape().as_list()
                    output_shape = shape[:-1] + [last_dim]
                    outputs.set_shape(output_shape)

                return outputs
            else:
                return tf.matmul(left, right, name=name)
        elif op_name == 'dropout':
            input = kwargs.get('input')
            keep_prob = kwargs.get('keep_prob')

            if input is None:
                raise AssertionError('`input` mustn\'t be None')
            elif keep_prob is None:
                raise AssertionError('`keep_prob` mustn\'t be None')
            else:
                return tf.nn.dropout(input, keep_prob=keep_prob, name=name)
        elif op_name == 'max_pool':
            input = kwargs.get('input')
            ksize = kwargs.get('ksize')
            strides = kwargs.get('strides') or [1, 1, 1, 1]
            padding = (kwargs.get('padding') or 'valid').upper()
            dilations = kwargs.get('dilations') or [1, 1, 1, 1]
            data_format = kwargs.get('data_format') or 'NHWC'

            if input is None:
                raise AssertionError('`input` mustn\'t be None')
            elif ksize is None:
                raise AssertionError('`ksize` mustn\'t be None')
            else:
                return tf.nn.max_pool(input, ksize=ksize, strides=strides,
                                      padding=padding, data_format=data_format,
                                      name=name)
        elif op_name == 'log':
            input = kwargs.get('x')

            if input is None:
                raise AssertionError('`x` mustn\'t be None')
            else:
                return tf.log(input, name=name)
        elif op_name == 'div':
            left = kwargs.get('left')
            right = kwargs.get('right')

            if left is None:
                raise AssertionError('`left` mustn\'t be None')
            elif right is None:
                raise AssertionError('`right` mustn\'t be None')
            else:
                return tf.div(left, right, name=name)
        elif op_name == 'mult':
            left = kwargs.get('left')
            right = kwargs.get('right')

            if left is None:
                raise AssertionError('`left` mustn\'t be None')
            elif right is None:
                raise AssertionError('`right` mustn\'t be None')
            else:
                return tf.multiply(left, right, name=name)
        elif op_name == 'concat':
            input = kwargs.get('input')
            axis = kwargs.get('axis') or 0

            if input is None:
                raise AssertionError('`input` mustn\'t be None')
            else:
                return tf.concat(input, axis=axis, name=name)
        elif op_name == 'reduce_prod':
            input = kwargs.get('input')
            axis = kwargs.get('axis') or 0
            keepdims = kwargs.get('keepdims')

            if input is None:
                raise AssertionError('`input` mustn\'t be None')
            else:
                return tf.reduce_prod(input, axis=axis, name=name,
                                     keepdims=keepdims)
        elif op_name == 'reduce_sum':
            input = kwargs.get('input')
            axis = kwargs.get('axis') or 0
            keepdims = kwargs.get('keepdims')

            if input is None:
                raise AssertionError('`input` mustn\'t be None')
            else:
                return tf.reduce_sum(input, axis=axis, name=name,
                                     keepdims=keepdims)
        elif op_name == 'reduce_mean':
            input = kwargs.get('input')
            axis = kwargs.get('axis') or 0

            if input is None:
                raise AssertionError('`input` mustn\'t be None')
            else:
                return tf.reduce_mean(input, axis=axis, name=name)
        elif op_name == 'batch_norm':
            inputs = kwargs.get('inputs')
            axis = kwargs.get('axis') or -1
            momentum = kwargs.get('momentum') or 0.99
            epsilon = kwargs.get('epsilon') or 0.001
            center = kwargs.get('center') or True
            scale = kwargs.get('scale') or True
            beta_initializer = kwargs.get('beta_initializer')
            gamma_initializer = kwargs.get('gamma_initializer')
            moving_mean_initializer = kwargs.get('moving_mean_initializer')
            moving_variance_initializer = kwargs.get('moving_variance_initializer')
            beta_regularizer = kwargs.get('beta_regularizer')
            gamma_regularizer = kwargs.get('gamma_regularizer')
            beta_constraint = kwargs.get('beta_constraint')
            gamma_constraint = kwargs.get('gamma_constraint')
            training = kwargs.get('training') or False
            trainable = kwargs.get('trainable') or True
            renorm = kwargs.get('renorm') or False
            renorm_clipping = kwargs.get('renorm_clipping')
            renorm_momentum = kwargs.get('renorm_momentum') or 0.99
            fused = kwargs.get('fused')
            virtual_batch_size = kwargs.get('virtual_batch_size')
            adjustment = kwargs.get('adjustment')

            if inputs is None:
                raise AssertionError('`input` mustn\'t be None')
            else:
                return tf.layers.batch_normalization(inputs, axis=axis,
                    momentum=momentum, epsilon=epsilon, center=center, scale=scale,
                    beta_initializer=beta_initializer, gamma_initializer=gamma_initializer,
                    moving_mean_initializer=moving_mean_initializer,
                    moving_variance_initializer=moving_variance_initializer,
                    beta_regularizer=beta_regularizer, gamma_regularizer=gamma_regularizer,
                    beta_constraint=beta_constraint, gamma_constraint=gamma_constraint,
                    training=training, trainable=trainable, name=name,
                    renorm=renorm, renorm_momentum=renorm_momentum, renorm_clipping=renorm_clipping,
                    fused=fused, virtual_batch_size=virtual_batch_size, adjustment=adjustment)
        elif op_name == 'adagrad':
            learning_rate = kwargs.get('learning_rate')
            initial_accumulator = kwargs.get('initial_accumulator') or 0.1

            if learning_rate is None:
                raise AssertionError('`learning_rate` mustn\'t be None')
            else:
                return tf.train.AdagradOptimizer(learning_rate=learning_rate,
                                                 initial_accumulator_value=initial_accumulator)
        elif op_name == 'adam':
            learning_rate = kwargs.get('learning_rate')
            beta1 = kwargs.get('beta1') or 0.99
            beta2 = kwargs.get('beta2') or 0.999
            epsilon = kwargs.get('epsilon') or 1e-08

            if learning_rate is None:
                raise AssertionError('`learning_rate` mustn\'t be None')
            else:
                return tf.train.AdamOptimizer(learning_rate=learning_rate,
                                              beta1=beta1,
                                              beta2=beta2,
                                              epsilon=epsilon)
        elif op_name == 'rmsprop':
            learning_rate = kwargs.get('learning_rate')
            decay = kwargs.get('decay') or 0.9
            epsilon = kwargs.get('epsilon') or 1e-10
            momentum = kwargs.get('momentum') or 0.0

            if learning_rate is None:
                raise AssertionError('`learning_rate` mustn\'t be None')
            else:
                return tf.train.RMSPropOptimizer(learning_rate=learning_rate,
                                                 decay=decay,
                                                 epsilon=epsilon,
                                                 momentum=momentum)
        elif op_name == 'sgd':
            learning_rate = kwargs.get('learning_rate')

            if learning_rate is None:
                raise AssertionError('`learning_rate` mustn\'t be None')
            else:
                return tf.train.GradientDescentOptimizer(learning_rate)
        elif op_name == 'mean_square_error':
            labels = kwargs.get('labels')
            weight = kwargs.get('weight') or 1.0
            logits = kwargs.get('logits')

            if ground_truth is None:
                raise AssertionError('`ground_truth` mustn\'t be None')
            elif ypred is None:
                raise AssertionError('`ypred` mustn\'t be None')

            return tf.losses.mean_squared_error(
                labels=labels,
                predictions=logits,
                weights=1.0,
            )
        elif op_name == 'round':
            x = kwargs.get('x')

            if x is None:
                raise AssertionError('`x` mustn\'t be None')
            else:
                return tf.round(x, name=name)
        elif op_name == 'square':
            x = kwargs.get('x')

            if x is None:
                raise AssertionError('`x` mustn\'t be None')
            else:
                return tf.square(x, name=name)
        elif op_name == 'abs':
            x = kwargs.get('x')

            if x is None:
                raise AssertionError('`x` mustn\'t be None')
            else:
                return tf.abs(x, name=name)
        elif op_name == 'regularization_loss':
            return tf.losses.get_regularization_loss(name=(name or 'total_regularization_loss'))
        elif op_name == 'pad':
            mode = kwargs.get('mode') or 'CONSTANT'
            input = kwargs.get('input')
            paddings = kwargs.get('paddings')
            constant_value = kwargs.get('constant_value') or 0

            if input is None:
                raise AssertionError('`input` mustn\'t be None')
            elif paddings is None:
                raise AssertionError('`paddings` mustn\'t be None')

            return tf.pad(input, paddings=paddings, name=name,
                          constant_value=constant_value, mode=mode)
        elif op_name == 'static_rnn':
            cell = kwargs.get('cell')
            inputs = kwargs.get('inputs')
            scope = kwargs.get('scope')
            dtype = kwargs.get('dtype') or inputs.dtype
            sequence_length = kwargs.get('sequence_length')

            if cell is None:
                raise AssertionError('`cell` mustn\'t be None')
            elif inputs is None:
                raise AssertionError('`inputs` mustn\'t be None')

            return tf.nn.static_rnn(cell=cell, inputs=inputs, scope=scope,
                                    sequence_length=sequence_length,
                                    dtype=dtype)
        elif op_name == 'dynamic_rnn':
            cell = kwargs.get('cell')
            inputs = kwargs.get('inputs')
            scope = kwargs.get('scope')
            dtype = kwargs.get('dtype') or inputs.dtype
            sequence_length = kwargs.get('sequence_length')

            if cell is None:
                raise AssertionError('`cell` mustn\'t be None')
            elif inputs is None:
                raise AssertionError('`inputs` mustn\'t be None')

            return tf.nn.dynamic_rnn(cell=cell, inputs=inputs, scope=scope,
                                     sequence_length=sequence_length,
                                     dtype=dtype)
        elif op_name == 'bidirectional_dynamic_rnn':
            cell_fw = kwargs.get('forward')
            cell_bw = kwargs.get('backward')
            inputs = kwargs.get('inputs')
            scope = kwargs.get('scope')
            dtype = kwargs.get('dtype') or inputs.dtype
            sequence_length = kwargs.get('sequence_length')

            if cell_fw is None:
                raise AssertionError('`forward` mustn\'t be None')
            elif cell_bw is None:
                raise AssertionError('`backward` mustn\'t be None')
            elif inputs is None:
                raise AssertionError('`inputs` mustn\'t be None')

            return tf.nn.bidirectional_dynamic_rnn(cell_fw=cell_fw,
                                                   cell_bw=cell_bw,
                                                   inputs=inputs,
                                                   scope=scope,
                                                   sequence_length=sequence_length,
                                                   dtype=dtype)
        elif op_name == 'bidirectional_static_rnn':
            cell_fw = kwargs.get('forward')
            cell_bw = kwargs.get('backward')
            inputs = kwargs.get('inputs')
            scope = kwargs.get('scope')
            dtype = kwargs.get('dtype') or inputs.dtype

            if cell_fw is None:
                raise AssertionError('`forward` mustn\'t be None')
            elif cell_bw is None:
                raise AssertionError('`backward` mustn\'t be None')
            elif inputs is None:
                raise AssertionError('`inputs` mustn\'t be None')

            return tf.nn.bidirectional_static_rnn(cell_fw=cell_fw,
                                                  cell_bw=cell_bw,
                                                  inputs=inputs,
                                                  scope=scope,
                                                  dtype=dtype)
        elif op_name == 'rnn':
            cell = kwargs.get('cell')

            if cell is None:
                raise AssertionError('`cell` mustn\'t be None')
            else:
                num_units = cell.num_units

            if isinstance(num_units, int):
                return tf.contrib.rnn.BasicRNNCell(num_units=num_units,
                                                   name=name)
            else:
                raise AssertionError('no support {}'.format(type(num_units)))
        elif op_name in ['gru', 'gru_cell']:
            dtype = kwargs.get('dtype')
            num_units = kwargs.get('num_units')
            activation = kwargs.get('activation') or None

            if not isinstance(num_units, int):
                raise AssertionError('`num_units` must be an int')

            return tf.nn.rnn_cell.GRUCell(num_units, name=name, dtype=dtype)
        elif op_name in ['lstm', 'basic_lstm', 'lstm_cell']:
            dtype = kwargs.get('dtype')
            num_units = kwargs.get('num_units')
            num_layers = kwargs.get('num_layers') or 1
            activation = kwargs.get('activation') or None

            if not isinstance(num_units, int):
                raise AssertionError('`num_units` must be an int')

            # @NOTE: implement specific LSTMCell to optimize  training performance
            if self.__on_cpu__():
                return tf.contrib.rnn.LSTMBlockCell(num_units, name=name,
                                                    dtype=dtype,
                                                    state_is_tuple=True)
            #elif self.__on_gpu__():
            #    return tf.contrib.cudnn_rnn.CudnnLSTM(num_layers, num_units,
            #                                          name=name, dtype=dtype)
            else:
                return tf.nn.rnn_cell.LSTMCell(num_units, name=name, dtype=dtype,
                                               state_is_tuple=True)
        elif op_name == 'log':
            x = kwargs.get('x')

            if x is None:
                raise AssertionError('`x` mustn\'t be None')

            return tf.log(x=x, name=name)
        elif op_name == 'clip_by_value':
            t = kwargs.get('t')
            clip_value_min = kwargs.get('clip_value_min')
            clip_value_max = kwargs.get('clip_value_max')

            if t is None:
                raise AssertionError('`t` mustn\'t be None')
            elif clip_value_min is None:
                raise AssertionError('`clip_value_min` mustn\'t be None')
            elif clip_value_max is None:
                raise AssertionError('`clip_value_max` mustn\'t be None')

            return tf.clip_by_value(t, clip_value_min, clip_value_max,
                                    name=name)
        elif op_name == 'negative':
            x = kwargs.get('x')

            if x is None:
                raise AssertionError('`x` mustn\'t be None')
            return -x
        elif op_name == 'softmax_cross_entropy_with_logits_v2':
            labels = kwargs.get('labels')
            logits = kwargs.get('logits')

            if labels is None:
                raise AssertionError('`labels` mustn\'t be None')
            elif logits is None:
                raise AssertionError('`logits` must\'t be None')
            return tf.nn.softmax_cross_entropy_with_logits_v2(labels=labels, logits=logits)
        elif op_name == 'floordiv':
            left = kwargs.get('left')
            right = kwargs.get('right')

            if left is None:
                raise AssertionError('`left` mustn\'t be None')
            elif right is None:
                raise AssertionError('`right` mustn\'t be None')

            return tf.floordiv(x=left, y=right, name=name)
        elif op_name == 'squeeze':
            input = kwargs.get('input')

            if input is None:
                raise AssertionError('`input` mustn\'t be None')
            return tf.squeeze(input, name=name)
        elif op_name == 'dense_to_sparse':
            input = kwargs.get('input')

            if input is None:
                raise AssertionError('`input` mustn\'t be None')
            return tf.contrib.layers.dense_to_sparse(input)
        elif op_name == 'sparse_to_dense':
            input = kwargs.get('input')

            if input is None:
                raise AssertionError('`input` mustn\'t be None')
            return tf.sparse_to_dense(input, name=name)
        elif op_name == 'edit_distance':
            hypothesis = kwargs.get('hypothesis')
            truth = kwargs.get('truth')

            if hypothesis is None:
                raise AssertionError()
            elif truth is None:
                raise AssertionError()

            if name is None:
                name = 'edit_distance'

            return tf.edit_distance(hypothesis=hypothesis, truth=truth,
                                    name=name)
        elif op_name == 'ctc_label_dense_to_sparse':
            labels = kwargs.get('labels')
            label_length = kwargs.get('label_length')

            if labels is None:
                raise AssertionError('`labels` mustn\'t be None')
            elif label_length is None:
                raise AssertionError('`label_length` mustn\'t be None')

            return tf.keras.backend.ctc_label_dense_to_sparse(labels, label_length)
        elif op_name == 'ctc_beam_search_decoder':
            logits = kwargs.get('inputs')
            input_length = kwargs.get('input_length')

            if logits is None:
                raise AssertionError('`inputs` mustn\'t be None')
            elif input_length is None:
                raise AssertionError('`input_length` mustn\'t be None')

            decoder, loss = tf.nn.ctc_beam_search_decoder(logits, input_length)
            return decoder[0], loss
        elif op_name == 'ctc_greedy_decoder':
            logits = kwargs.get('inputs')
            input_length = kwargs.get('input_length')

            if logits is None:
                raise AssertionError('`inputs` mustn\'t be None')
            elif input_length is None:
                raise AssertionError('`input_length` mustn\'t be None')

            decoder, loss = tf.nn.ctc_greedy_decoder(logits, input_length)
            return decoder[0], loss
        elif op_name == 'ctc_loss':
            labels = kwargs.get('labels')
            inputs = kwargs.get('inputs')
            sequence_length = kwargs.get('sequence_length')

            if labels is None:
                raise AssertionError('`labels` mustn\'t be None')
            elif inputs is None:
                raise AssertionError('`inputs` mustn\'t be None')
            elif sequence_length is None:
                raise AssertionError('`sequence_length` mustn\'t be None')

            return tf.nn.ctc_loss(labels=labels, inputs=inputs,
                                  sequence_length=sequence_length)
        elif op_name == 'expand_dims':
            input = kwargs.get('input')
            axis = kwargs.get('axis')

            if input is None:
                raise AssertionError('`input` mustn\'t be None')
            elif axis is None:
                raise AssertionError('`axis` mustn\'t be None')

            return tf.expand_dims(input=input, axis=axis)
        else:
            raise AssertionError('don\'t support object `%s`' % op_name)

    def __summaries__(self, op_name, **kwargs):
        if op_name == 'value':
            tag = kwargs.get('tag')
            value = kwargs.get('value')

            if tag is None:
                raise AssertionError('`tag` mustn\'t be None')
            elif value is None:
                raise AssertionError('`value` mustn\'t be None')

            return tf.Summary(value=[tf.Summary.Value(tag=tag,
                                                      simple_value=value)])
        elif op_name == 'scalar':
            name = kwargs.get('name')
            input = kwargs.get('input')

            if input is None:
                raise AssertionError('`input` mustn\'t be None')
            elif name is None:
                name = input.name

            return tf.summary.scalar(name=name, tensor=input)
        elif op_name == 'image':
            name = kwargs.get('name')
            input = kwargs.get('input')

            if input is None:
                raise AssertionError('`input` mustn\'t be None')
            elif name is None:
                name = input.name

            return tf.summary.image(name=name, tensor=input)
        elif op_name == 'audio':
            name = kwargs.get('name')
            input = kwargs.get('input')

            if input is None:
                raise AssertionError('`input` mustn\'t be None')
            elif name is None:
                name = input.name

            return tf.summary.image(name=name, tensor=input)
        elif op_name == 'text':
            name = kwargs.get('name')
            input = kwargs.get('input')

            if input is None:
                raise AssertionError('`input` mustn\'t be None')
            elif name is None:
                name = input.name

            return tf.summary.text(name=name, tensor=input)
        elif op_name == 'tensor':
            name = kwargs.get('name')
            input = kwargs.get('input')

            if input is None:
                raise AssertionError('`input` mustn\'t be None')
            elif name is None:
                name = input.name

            return tf.summary.tensor_summary(name=name, tensor=input)
        elif op_name == 'FileWriter':
            path = kwargs.get('path')
            write_graph = kwargs.get('write_graph') or False

            if path is None:
                raise AssertionError('`path` mustn\'t be None')
            if write_graph:
                return tf.summary.FileWriter(logdir=path, graph=self._session.graph)
            else:
                return tf.summary.FileWriter(logdir=path)
        elif op_name == 'merge_all':
            scope = kwargs.get('scope')

            if scope is None:
                raise AssertionError('`scope` mustn\'t be None')
            return tf.summary.merge_all(scope=scope)
        elif op_name == 'beholder':
            try:
                from tensorboard.plugins.beholder import Beholder

                savepoint = kwargs.get('savepoint')
                if savepoint is None:
                    raise AssertionError('`savepoint` mustn\'t be None')

                return Beholder(logdir=savepoint)
            except Exception:
                return None
        elif op_name == 'histogram':
            name = kwargs.get('name')
            values = kwargs.get('values')

            if values is None:
                raise AssertionError('`values` mustn\'t be None')
            elif name is None:
                name = values.name
            return tf.summary.histogram(name=name, values=values)
        else:
            raise AssertionError('don\'t support object `%s`' % op_name)

    def __gradient_helper__(self, optimizer, loss_function, variable_list):
        try:
            from tensorboard.plugins.beholder import Beholder

            gradients, trainer = Beholder.gradient_helper(optimizer, loss_function,
                                                          variable_list)
            return [gradients, trainer]
        except Exception:
            return optimizer.minimize(loss_function, var_list=variable_list)

    def __dump_saver__(self, checkpoint, saver, **kwargs):
        # @NOTE: finish training, we must save a checkpoint if

        if not checkpoint is None:
            saver.save(self._session, checkpoint, **kwargs)

    def __load_saver__(self, checkpoint, trainer, callbacks, variables=None):
        import glob
        import os

        # @NOTE: save everything to tf.train.Saver
        if not checkpoint is None:
            if variables is None:
                saver = tf.train.Saver(tf.global_variables())
            else:
                saver = tf.train.Saver(variables)

            max_index = -1

            for path in glob.glob('%s-*.meta' % checkpoint):
                index = int(path[len(checkpoint) + 1: len(path) - len('.meta')])

                if index > max_index:
                    max_index = index
            else:
                if max_index > 0:
                    saver.restore(self._session, '%s-%d' % (checkpoint, max_index))

                    if not callbacks is None:
                        for name in callbacks:
                            callbacks[name] = callbacks[name].eval(session=self._session)
            return saver
        else:
            return None

    def __variable_scope__(self, name, reuse=False, **kwargs):
        if reuse:
            return tf.variable_scope(name, reuse=tf.AUTO_REUSE, **kwargs)
        else:
            return tf.variable_scope(name, **kwargs)

    def __name_scope__(self, name, use_default_graph=False, **kwargs):
        if use_default_graph:
            return tf.get_default_graph().name_scope(name, **kwargs)
        else:
            return tf.name_scope(name, **kwargs)

    def __variable__(self, **kwargs):
        return tf.get_variable(**kwargs)

    def __placeholder__(self, **kwargs):
        return tf.placeholder(**kwargs)

    def __reshape__(self, tensor, **kwargs):
        return tf.reshape(tensor, **kwargs)

    def __transpose__(self, tensor, perm, **kwargs):
        return tf.transpose(tensor, perm=perm, **kwargs)

    def __name__(self, object, shorten=False):
        if isinstance(object, tf.Tensor):
            if shorten:
                fullname = object.name.split(':')[0].split('/')[0]

                if len(fullname.split('_')) > 2:
                    return '_'.join(fullname.split('_')[0:-1])
                else:
                    try:
                        if len(fullname.split('_')) > 1 and int(fullname.split('_')[1]) >= 0:
                            return '_'.join(fullname.split('_')[0:-1])
                        else:
                            return fullname
                    except ValueError:
                        return fullname
            else:
                return object.name.split(':')[0]
        else:
            return None

    def __shape__(self, tensor, runtime, dims=None):
        if runtime:
            shape = tf.shape(tensor)
        else:
            shape = tensor.get_shape().as_list()

        def wrapping(dim):
            return tf.shape(tensor)[dim]

        if dims is None:
            return shape
        elif isinstance(shape, tf.Tensor):
            return [wrapping(dim) for dim in dims]
        else:
            return [shape[dim] for dim in dims]

    def __dtype__(self, tensor):
        return tensor.dtype

    def __cast__(self, tensor, to_type=tf.Tensor):
        if isinstance(tensor, tf.Tensor) or isinstance(tensor, tf.SparseTensor):
            if to_type == 'int32':
                return tf.to_int32(tensor)
            elif to_type == 'int64':
                return tf.to_int64(tensor)
            elif to_type == 'float':
                return tf.to_float(tensor)
            else:
                return tf.cast(tensor, to_type)
        elif to_type == tf.Tensor:
            return tf.constance(tensor)

    def __type__(self, obj):
        if isinstance(obj, str):
            if obj == 'tensor':
                return tf.Tensor
            elif obj == 'sparse':
                return tf.SparseTensor
            elif obj == 'shape':
                return tf.TensorShape
            else:
                return None
        elif isinstance(obj, tf.Tensor):
            return obj.dtype

    def __run__(self, fetches, feed_dict, **kwargs):
        return self._session.run(fetches=fetches, feed_dict=feed_dict, **kwargs)

    def __global_step_incrementer__(self):
        # @NOTE: define global step here

        with tf.name_scope('step'):
            global_step = tf.train.get_or_create_global_step()
            return global_step, tf.assign(global_step, global_step + 1)

    def __restore__(self, checkpoint, is_artifact):
        try:
            if is_artifact:
                with tf.gfile.GFile(checkpoint, 'rb') as f:
                    graph_def = tf.GraphDef()
                    graph_def.ParseFromString(f.read())

                    tf.import_graph_def(graph_def, name='')
            else:
                self.__load_saver__(checkpoint, trainer=None, callbacks=None)
            return True
        except Exception as error:
            print(error)
            return False

    def __find__(self, name=None, type=None):
        mapping = {
            'input': 'Placeholder',
            'softmax': 'Softmax',
            'const': 'Const'
        }

        if type is None and name is None:
            raise AssertionError('`type` or `name` mustn\'t be None at the '
                                 'same time')
        else:
            graph = self._session.graph
            result = []

            if not type is None:
                if type in mapping:
                    type = mapping[type]

            for op in graph.get_operations():
                type_okey = True if type is None else op.type == type
                name_okey = True if name is None else op.name == name

                if type_okey and name_okey:
                    result.append(graph.get_tensor_by_name(op.name))
            else:
                return result

    def __write_graph__(self, output, gates, as_text=False):
        import os

        outputs = []
        for tensor in gates:
            outputs.append(tensor.name.split(':')[0])

        if len(outputs) == 0:
            return False

        try:
            frozen_graph_def = \
                tf.graph_util.convert_variables_to_constants(self._session,
                                                             self._session.graph_def,
                                                             outputs)
            tf.train.write_graph(frozen_graph_def,
                                 os.path.dirname(output),
                                 os.path.basename(output),
                                 as_text=as_text)
            return True
        except Exception as error:
            return False
