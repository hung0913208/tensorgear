from .layer import Layer
from .objective import Objective
from .backend import Backend

def configure_to_layers(configure, context=None):
    # @NOTE: when configure is set, we will use this to define our model
    # automatically from json or yaml

    layers = {}
    inputs = []
    outputs = []

    def implement_layer(name, layer, information):
        params = information.get('parameters')

        if params is None or len(params) == 0:
            implemented = Layer.create(layer, name)
        else:
            implemented = Layer.create(layer, name, **params)

        if implemented is None:
            raise AssertionError('there was an error during create `%s`' % name)
        layers[name] = implemented

    def implement_entropy(name, entropy, information):
        params = information.get('parameters')

        if not params is None:
            # @NOTE: hacking way to reduce duplicated parameters

            if not 'ground_truth' in params:
                if 'inputs' in information:
                    params['ground_truth'] = information['inputs'][0]
            elif not 'inputs' in information:
                information['inputs'] = [params['ground_truth']]

        if params is None or len(params) == 0:
            implemented = Objective.create(entropy, name)
        else:
            implemented = Objective.create(entropy, name, **params)

        if implemented is None:
            raise AssertionError('there was an error during create `%s`' % name)
        layers[name] = implemented

    # @NOTE: step 1: implement layers from begin to end
    for name in configure:
        information = configure[name]

        if not 'class' in information:
            raise AssertionError('your configuration is missing a layer name')

        clss = information.get('class')
        index_of_layer = 'layers.%s' % clss
        index_of_entropy = 'entropies.%s' % clss

        if not Backend.get(index_of_layer) is None:
            implement_layer(name, clss, information)
        elif not Backend.get(index_of_entropy) is None:
            implement_entropy(name, clss, information)
    else:
        if len(layers) == 0:
            raise AssertionError('There was nothing to do')

    # @NOTE: step 2: connect layers together
    for name in configure:
        information = configure[name]
        target = layers[name]

        if not 'inputs' in information:
            # @NOTE: entropy allways requires inputs so we need to avoid it

            if isinstance(target, Layer):
                inputs.append(target)
        else:
            for input_name in information['inputs']:
                if not input_name in layers:
                    raise AssertionError('not found layer `%s`' % input_name)

                # @NOTE: with layer is a Layer object, there is simple
                # just add `gap` as the previous layers
                target.inhearit_from(layers[input_name], context)

                # @NOTE: and add this layer as the next layer of its
                # preceded layers 
                if not target in layers[input_name]._next:
                    layers[input_name].next_to(target)
            else:
                if 'output' in information:
                    outputs.append(target)

    # @NOTE: step 3: mount all of the to context from inputs down to outputs
    if not context is None:
        for layer in inputs:
            context.add(layer)

        for name in layers:
            if not layers[name] in inputs:
                context.add(layers[name])

    if len(outputs) > 0:
        context.outputs.extend(sorted(outputs, 
                                      key=lambda k: configure[k.name]['output']))

    return layers

def structure_to_layers(structure, input, context=None):
    if isinstance(input, list):
        gaps = input
    else:
        gaps = [] if input is None else [input]

    for layer in structure:
        # @NOTE: at the begining we must check type of each layer

        if isinstance(layer, list):
            # @NOTE: list must indicate that you have to define a 
            # sub-sequence run inside the main-sequence
            layer = structure_to_layers(layer, input=input, context=context)

            # @NOTE: then add it to `gaps` since we will use it as a 
            # combined layers
            if not layer is None:
                gaps.append(layer)
            else:
                raise AssertionError('there was an error during implement '
                                         'a sub sequence')
            continue
        elif isinstance(layer, Layer):
            # @NOTE: with layer is a Layer object, there is simple
            # just add `gap` as the previous layers
            layer.inhearit_from(gaps, context)

            # @NOTE: and add this layer as the next layer of its
            # preceded layers 
            for dep in layer.previous:
                if not layer in dep._next:
                    dep.next_to(layer)

            # @NOTE: and then mount Logits to work as its context
            layer.mount(context)

            # @NOTE: and replace `gaps` with the new one
            gaps = [layer]
        elif isinstance(layer, Objective):
            # @NOTE: with layer is a Objective object, there is simple
            # just add `gap` as the previous layers
            layer.inhearit_from(gaps, context)

            # @NOTE: and add this layer as the next layer of its
            # preceded layers 
            for dep in layer.previous:
                if not layer in dep._next:
                    dep.next_to(layer)

            # @NOTE: and then mount Logits to work as its context
            layer.mount(context)

            # @NOTE: and replace `gaps` with the new one
            gaps = [layer]
        input = layer
    else:
        if not (isinstance(structure[-1], Layer) or isinstance(structure[-1], Objective)):
            raise AssertionError('The lastest element must be a `Layer`')
        else:
            return structure[-1]
            
