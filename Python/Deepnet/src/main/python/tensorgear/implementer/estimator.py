from .backend import Backend
from .objective import Objective
from .iterator import Iterator
from .progress import Progress
from .summary import Summary
from .logits import Logits
from .layer import Layer
from .optimizer import Optimizer
from .utils import is_input_layer
from .parser import structure_to_layers, configure_to_layers

try:
    from queue import Queue
except ImportError:
    from Queue import Queue

import numpy as np

MODEL_TYPE=['classification', 'regression', 'sequence']

class Estimator(object):
    def __init__(self, model_type, name=None, provider=None, logits=None, optimizer=None,
                 loss_function=None, progress=None):
        super(Estimator, self).__init__()

        # @NOTE: define our private parameters
        self._functions = {
            'entropy': None,
            'evaluation': None,
            'summary': None,
            'optimizer': None
        }

        if not model_type in MODEL_TYPE:
            raise AssertionError('`model_type` must be one of %s' % MODEL_TYPE)

        self._progress = progress or Progress()
        self._model_type = model_type
        self._mapping = {}
        self._seq_length = {}
        self._checkpoint = None
        self._optimizer = None
        self._provider = None
        self._summary = None
        self._logits = None
        self._name = name or 'noname'

        # @NOTE: use setter as a convenient way to install our estimator
        self.provider = provider

        # @NOTE: logits might be a Logits object or a dict that is used to
        # define a logic of our neural network
        if isinstance(logits, Logits):
            self.logits = logits
        elif isinstance(logits, dict):
            self.logits = Logits(configure=logits)
        elif isinstance(logits, list):
            self.logits = Logits(structure=logits)
        elif isinstance(logits, Layer):
            unscan_layers = Queue()
            layers = [logits]

            # @NOTE: put the lastest layer
            unscan_layers.put(logits)

            # @NOTE; scan throught from bottom to top
            while unscan_layers.qsize() > 0:
                scanning = unscan_layers.get()

                for layer in scanning.previous:
                    if len(layer.previous) > 0 and (not layer in layers):
                        unscan_layers.put(layer)

                    if not layer in layers:
                        layers.append(layer)
            else:
                layers.reverse()
                self.logits = Logits(mount=layers)

        mounted_loss_function = None
        if isinstance(loss_function, Layer) or isinstance(loss_function, Objective) \
                and not isinstance(optimizer, Optimizer):
            unscan_layers = Queue()
            layers = [loss_function]

            # @NOTE: put the lastest layer
            unscan_layers.put(loss_function)

            # @NOTE; scan throught from bottom to top
            while unscan_layers.qsize() > 0:
                scanning = unscan_layers.get()

                for layer in scanning.previous:
                    if len(layer.previous) > 0 and (not layer in layers):
                        unscan_layers.put(layer)

                    if not layer in layers:
                        layers.append(layer)

                    if isinstance(layer, Objective) and (not layer.ground_truth is None):
                        ground_truth = layer.ground_truth

                        if len(ground_truth.previous) > 0 and (not ground_truth in layers):
                            unscan_layers.put(ground_truth)

                        if not ground_truth in layers:
                            layers.append(ground_truth)

            else:
                mounted_loss_function = layers

        # @NOTE: optimizer might be an Optimizer object or a string that
        # indicate the name of our optimizer, with that we will use the default
        # parameter of this optimizer
        if isinstance(optimizer, Optimizer):
            if not loss_function is None:
                if isinstance(loss_function, dict):
                    optimizer.entropy = (None, loss_function, None)
                elif isinstance(loss_function, list):
                    optimizer.entropy = (None, None, loss_function)
                else:
                    optimizer.entropy = (loss_function, None, None)

            self.optimizer = optimizer
        elif isinstance(optimizer, str):
            if mounted_loss_function is None:
                self.optimizer = Optimizer.create(optimizer, loss_function=loss_function)
            else:
                self.optimizer = Optimizer.create(optimizer, mount=mounted_loss_function,
                                                  loss_function=loss_function)
        elif isinstance(optimizer, dict):
            if mounted_loss_function is None:
                self.optimizer = Optimizer.create(loss_function=loss_function,
                                                  **optimizer)
            else:
                self.optimizer = Optimizer.create(loss_function=loss_function,
                                                  mount=mounted_loss_function,
                                                  **optimizer)

    @property
    def model_type(self):
        return self._model_type

    def feed_to_model(self, mode, feed_dict, batch, learning_rate=None):
        if feed_dict is None:
            if mode in ['train', 'valid', 'test']:
                result = self._optimizer.feed(mode=mode, batch=batch,
                                              learning_rate=learning_rate)
            elif mode == 'predict':
                result = self._logits.feed(batch=batch)

        if callable(feed_dict):
            result = feed_dict(feed_context=result,
                               learning_rate=learning_rate,
                               batch=batch)
        return result

    def prepare_new_batch(self, generator, batch_size):
        batch = []
        inputs_length = []

        for _ in range(batch_size):
            try:
                record = next(generator)

                if len(batch) == 0:
                    for item in record:
                        if isinstance(item, np.ndarray):
                            if len(item.shape) > 1:
                                for i in range(item.shape[0]):
                                    batch.append([item[i]])
                                    inputs_length.append([])
                            else:
                                batch.append([item])
                                inputs_length.append([])
                        elif isinstance(item, list) or isinstance(item, tuple):
                            batch.append([list(item)])
                            inputs_length.append([])
                        else:
                            batch.append([item])
                            inputs_length.append([])
                else:
                    for index, item in enumerate(record):
                        if isinstance(item, np.ndarray):
                            if len(item.shape) > 1:
                                for i in range(item.shape[0]):
                                    batch[index].append(item[i])
                            else:
                                batch[index].append(item)
                        elif isinstance(item, list) or isinstance(item, tuple):
                            batch[index].append(list(item))
                        else:
                            batch[index].append(item)
            except (StopIteration, RuntimeError) as error:
                raise AssertionError('Iterator shouldn\'t be corrupted by the '
                                     'exception `StopIterator`')
        else:
            # @NOTE: concatenate our record to a single batch

            for icol, column in enumerate(batch):
                if isinstance(column[0], np.ndarray):
                    shape = [len(column)]

                    for i in range(0, len(column[0].shape)):
                        max_dim = max(column, key=lambda item: item.shape[i]).shape[i]
                        shape.append(max_dim)
                    else:
                        padding = np.zeros(shape)

                    for irow, item in enumerate(column):
                        conversion = []

                        if len(shape) > 2:
                            inputs_length[icol].append(list(item.shape))
                        else:
                            inputs_length[icol].append(item.shape[0])

                        for index, dim in enumerate(item.shape):
                            if index + 1 > len(shape):
                                continue
                            else:
                                conversion.append((0, shape[index + 1] - dim))
                        else:
                            padding[irow] = np.pad(item, conversion, 'constant')
                    else:
                        inputs_length[icol] = np.array(inputs_length[icol])
                        batch[icol] = padding
                elif isinstance(column[0], list):
                    padding = np.zeros([len(column),
                                        len(max(column, key=lambda item: len(item)))])

                    for irow, item in enumerate(column):
                        padding[irow, 0:len(item)] = item
                        inputs_length[icol].append(len(item))
                    else:
                        batch[icol] = padding
                        inputs_length[icol] = np.array(inputs_length[icol])
                else:
                    batch[icol] = np.array(batch[icol])
                    inputs_length[icol] = np.array([1 for _ in batch[icol]])
            return batch, inputs_length

    @property
    def predicter(self):
        return self._predicter

    @property
    def seq_length(self):
        return self._seq_length

    @predicter.setter
    def predicter(self, value):
        self._predicter = value

    @property
    def optimizer(self):
        return self._optimizer

    @optimizer.setter
    def optimizer(self, value):
        if isinstance(value, Optimizer):
            self._optimizer = value

            if not self._logits is None:
                self._optimizer.logits = self._logits

            # @NOTE: we always adapt implementers to self._functions
            self._functions['optimizer']  = self._optimizer.implement
            if not self._optimizer.entropy is None:
                self._functions['entropy'] = self._optimizer.entropy.implement

        if not self._optimizer is None and not self._logits is None:
            self._summary = Summary(self)

    @property
    def provider(self):
        return self._provider

    @provider.setter
    def provider(self, value):
        if isinstance(value, list):
            self._provider = Iterator(value)
        else:
            self._provider = value

    @property
    def logits(self):
        return self._logits

    @logits.setter
    def logits(self, value):
        if isinstance(value, Logits):
            self._logits = value

            # @NOTE: we always adapt implementers to self._functions
            self._functions['evaluation'] = self._logits.implement

        if not self._optimizer is None and not self._logits is None:
            self._summary = Summary(self)

    @property
    def summary(self):
        return self._summary

    def fit(self, provider=None, optimizer=None, logits=None, summaries_dir=None,
            feed_dict=None, learning_rate=0.0001, checkpoint=None, epochs=10,
            steps_per_epoch=1000, batch_size=32, validate_batch_size=None,
            validate_steps_per_epoch=1, use_histogram=True, freezed_layers=None,
            hooks=None, debugger=None, **kwargs):
        list_input_length_placeholders = []
        list_input_gates = []
        list_variables = []
        debug = []
        weights = None
        phrases = {
            'train': [],
            'valid': [],
            'test': []
        }

        if not self._summary.debug is None:
            # @NOTE: reset summary
            if not self._summary is None:
                self._summary.reset()

            # @NOTE: reset layers of logits
            for layer in self.logits.layers:
                layer.reset()

            # @NOTE: reset layers of optimizer
            for layer in self.optimizer.layers:
                layer.reset()

            # @NOTE: apply debug info from self._summary to backend
            Backend.reset(debug=self._summary.debug)

        # @NOTE: update provider if it needs
        if not provider is None:
            self.provider = provider
        elif self._provider is None:
            raise AssertionError('Please install an interator before '
                                 'doing anything')

        # @NOTE: update logits if it needs
        if not logits is None and isinstance(logits, Logits):
            self.logits = logits
        elif self._logits is None:
            raise AssertionError('Please install a logits before doing anything')

        # @NOTE: freeze layers when we don't want to train it anymore
        if not freezed_layers is None:
            for layer in self.logits.layers:
                layer.trainable = not layer.name in freezed_layers

            for layer in self.optimizer.layers:
                if isinstance(layer, Objective) and not layer.ground_truth is None:
                    if isinstance(layer.ground_truth, Layer):
                        layer.trainable = not layer.ground_truth.name in freezed_layers
                    else:
                        layer.trainable = not layer.ground_truth in freezed_layers
                else:
                    layer.trainable = not layer.name in freezed_layers

        # @NOTE: update optimizer if it needs
        if not optimizer is None:
            self.optimizer = optimizer
        elif self._optimizer is None:
            raise AssertionError('Please install a optimizer before doing anything')

        self._progress.reset()
        self._progress.rotate(prefix=self._logits.name or self._name)
        self._progress.percent()
        self._progress.suffix(suffix='completed')
        self._progress.time_track()
        self._progress.value('entropy')

        if self._model_type in ['classification', 'regression']:
            self._progress.percent('accuracy')
        elif self._model_type == 'sequence':
            self._progress.value('error_rate')

        # @NOTE: if the model_type is `sequence` we must define the real input
        # length of batch records
        for layer in self._optimizer.layers:
            if isinstance(layer, Objective):
                layer.estimator = self

        if self._model_type == 'sequence':
            # @NOTE: create `seq_length` according with Input layers from
            # Logits
            for layer in self._logits.layers:
                if layer in self._seq_length:
                    continue
                elif is_input_layer(layer):
                    list_input_gates.append(layer.gate)
                    list_input_length_placeholders.append(
                        Backend.implement('placeholder', dtype=np.int64,
                                          shape=[None], name='%s_name' % layer.name))
                    self._seq_length[layer] = list_input_length_placeholders[-1]

            # @NOTE: create `seq_length` according with Input layers from
            # Optimizer
            for layer in self._optimizer.layers:
                if layer in self._seq_length:
                    continue
                elif is_input_layer(layer):
                    list_input_gates.append(layer.gate)
                    list_input_length_placeholders.append(
                        Backend.implement('placeholder', dtype=np.int64,
                                          shape=[None], name='%s_length' % layer.name))
                    self._seq_length[layer] = list_input_length_placeholders[-1]
                elif isinstance(layer, Objective) and layer.ground_truth:
                    list_input_gates.append(layer.ground_truth.gate)
                    list_input_length_placeholders.append(
                        Backend.implement('placeholder', dtype=np.int64,
                                          shape=[None], name='%s_length' % layer.ground_truth.name))
                    self._seq_length[layer.ground_truth] = list_input_length_placeholders[-1]

        # @NOTE: define evaluation machinery in the graph
        if self._functions.get('evaluation') is None:
            raise AssertionError('please provide function `evaluation()`')
        else:
            result = self._functions['evaluation'](batch_size=batch_size)

        if isinstance(result, tuple) or isinstance(result, list):
            evaluater, adapters = result[0:2]
        else:
            evaluater, adapters = result, ['train', 'valid', 'test']

        # @NOTE: load evaluater to adapters
        for adapter in adapters:
            if adapter in phrases:
                phrases[adapter].append(evaluater)

        # @NOTE: load cross_entropy
        with Backend.scope('loss'):
            if self._functions.get('entropy') is None:
                raise AssertionError('please provide function `cross_entropy()`')
            else:
                result = self._functions['entropy'](ypreds=evaluater, batch_size=batch_size)

            if isinstance(result, tuple) or isinstance(result, list):
                loss_formular, adapters = result[0:1]
            else:
                loss_formular, adapters = result, ['train', 'valid', 'test']

            # @NOTE: some loss function don't produce a scalar so we must do
            # it manually
            if len(Backend.shape(loss_formular)) > 0:
                loss_formular = Backend.implement('reduce_mean',
                                                  input=loss_formular)

            # @NOTE: load loss_formular to adapters
            for adapter in adapters:
                if adapter in phrases:
                    phrases[adapter].append(loss_formular)

        # @NOTE: load optimizer
        with Backend.scope('training'):
            if self._functions.get('optimizer') is None:
                raise AssertionError('please provide function `optimizer()`')
            else:
                if callable(learning_rate):
                    learning_rate_placeholder = Backend.implement('placeholder',
                                                                  shape=[],
                                                                  default_value=0.0001)
                    result = self._functions['optimizer'](learning_rate=learning_rate_placeholder)
                else:
                    result = self._functions['optimizer'](learning_rate=learning_rate)

            if isinstance(result, tuple) or isinstance(result, list):
                optimizer = optimizer[0]
            else:
                optimizer = result

            # @NOTE: load optimizer to pharse 'train'
            if loss_formular is None:
                raise AssertionError('check again, `loss_formular` is None recently')
            else:
                list_variables = []
                weights = []

                # @NOTE: implement gradient of the whole neural network from
                # input <-> logits <-> loss_function
                for layer in self.logits.layers:
                    weights.extend(layer.weights)

                    if layer.trainable and is_input_layer(layer) is False:
                        list_variables.extend(layer.weights)
                else:
                    phrases['train'].extend(Backend.gradient_helper(optimizer, loss_formular,
                                                                    variable_list=list_variables))

        ground_truth = []
        for layer in self._optimizer.layers:
            if is_input_layer(layer):
                ground_truth.append(layer.outputs[0])

        # @NOTE: implement global step which will be used to tracking training
        # step throught many sessions
        global_step, increment_global_step = Backend.global_step_incrementer()
        writer = None

        if not summaries_dir is None:
            training_summaries_dir = summaries_dir + '/train/'
            validating_summaries_dir = summaries_dir + '/validate/'
            logtrace_dir = summaries_dir

            # @NOTE: implement summary' operators
            summaries_dir = {
                'train': training_summaries_dir,
                'validate': validating_summaries_dir
            }
            summaries, accuracy, writer, extends = \
                self._summary.implement('summary', loss_formular=loss_formular,
                                        where_to_write=summaries_dir)

        if not writer is None:
            training_writer, validating_writer = writer['train'], writer['validate']

        # @NOTE: add hooks to phrases
        if isinstance(hooks, dict):
            for phrase_name in phrases:
                if phrase_name in hooks:
                    for hook in hooks[phrase_name]:
                        if hook is None:
                            continue
                        else:
                            phrases[phrase_name].append(hook)

        if callable(debugger):
            for layer in self._logits.layers:
                if isinstance(layer._debug, list):
                    debug.extend(layer._debug)

            for layer in self._optimizer.layers:
                if isinstance(layer._debug, list):
                    debug.extend(layer._debug)

        # @NOTE: add custom tasks to phrases
        if isinstance(extends, list):
            for extend in extends:
                define, positions = extend

                if isinstance(positions, str):
                    # @NOTE: we have only one place to be added

                    if positions in phrases:
                        phrases[positions].append(define)
                elif isinstance(positions, tuple) or isinstance(positions, list):
                    # @NOTE: we have multiple position to be added

                    for position in positions:
                        if position in phrases:
                            phrases[position].append(define)


        # @NOTE: do we need to have tracking callbacks or not?
        if self._functions.get('train') is None and self._functions.get('validate') is None:
            if epochs <= 0:
                raise AssertionError('`epochs must be greater than 0`')

        callback = {'global_step': increment_global_step}
        weights.append(global_step)

        with Backend.do_fiting(checkpoint=checkpoint, saver=callback, variables=weights) as trainer:
            global_step = Backend.evaluate(global_step, feed_dict={})

            generator = None
            feed_dict_ = {}
            epoch_index = 0

            # @NOTE: add default tasks to run
            phrases['train'].append(increment_global_step)
            phrases['train'].insert(0, summaries)
            phrases['train'].insert(1, accuracy)
            phrases['valid'].insert(0, summaries)
            phrases['valid'].insert(1, accuracy)
            phrases['test'].insert(0, summaries)
            phrases['test'].insert(1, accuracy)

            # @NOTE: add decoders since we can use it to decode the result from logits, only working
            # on model_type `sequence`
            decoders = []

            if self.model_type == 'sequence':
                for layer in self._optimizer.layers:
                    if isinstance(layer, Objective) and layer.decoder:
                        decoders.append(layer.decoder[0])

            # @FIXME: BeHolder got an unexpected bug, waiting tensorboard team fix it
            # if not summaries_dir is None:
            #    self._summary.logtrace(logtrace_dir)

            # @NOTE: everything is Okey now, perform training and testing instantly
            while epochs < 0 or epoch_index < epochs:
                self._provider.use_dataset('train')

                if generator is None:
                    generator = self._provider.generate()

                if callable(learning_rate):
                    feed_dict_[self.optimizer.learning_rate] = learning_rate(global_step)

                # @NOTE: loop and perform training with data from self._provider
                for step in range(steps_per_epoch):
                    if len(phrases['train']) < 3:
                        return False
                    else:
                        batch, inputs_length = self.prepare_new_batch(generator, batch_size)

                        # @NOTE: build our feed_dict
                        for index, gate in enumerate(list_input_gates):
                            feed_dict_[list_input_length_placeholders[index]] = inputs_length[gate]
                        else:
                            feed_dict_.update(self.feed_to_model('train', feed_dict,
                                                                 learning_rate=learning_rate,
                                                                 batch=batch))

                        if not feed_dict_ is None:
                            prefix = 'epoch %d' % (epoch_index + 1)

                            # @NOTE: some deeplearning library use defined graph to implement the whole system
                            # and it's very hard to debug even if we have provided many way to make statistic
                            # or debugger on visualize, so these lines will solve this problem by printing result
                            # from separated parts of the graph
                            if len(debug) > 0:
                                packed = {}

                                for index, result in enumerate(trainer.run(debug, feed_dict=feed_dict_)):
                                    packed[debug[index]] = result
                                else:
                                    debugger(packed)

                            # @NOTE: calculate the whole graph now
                            result = trainer.run(phrases['train'] + [weights] + [decoders], feed_dict=feed_dict_)

                            if not (isinstance(result, list) or isinstance(result, tuple)):
                                raise result
                            else:
                                summary, accuracy_, ypreds, entropy, gradient, _, global_step = result[0:7]

                                if self._model_type == 'sequence' and len(decoders) > 0:
                                    ypreds = result[len(phrases['train']) + 1]

                            # @NOTE: load `tracking` object and `scalar` object
                            if not writer is None:
                                self._summary('tracking')

                                if self.model_type == 'sequence':
                                    for layer in self._optimizer.layers:
                                        # @NOTE: load ytruths base on ground_truth of entropies

                                        if isinstance(layer, Objective):
                                            if not layer.ground_truth is None:
                                                accuracy_.append(self._summary('scalar', writer=training_writer,
                                                                               objective=layer,
                                                                               ypreds=ypreds,
                                                                               ytruths=feed_dict_[layer.ground_truth.outputs[0]],
                                                                               global_step=global_step))

                                training_writer.add_summary(summary, global_step)
                                training_writer.flush()

                            if accuracy is None or len(accuracy) == 0 and len(accuracy_) == 0:
                                self._progress.render(percent=100.0*(step + 1)/steps_per_epoch,
                                                      prefix=prefix, entropy=entropy)
                            elif self._model_type in ['classification', 'regression']:
                                self._progress.render(percent=100.0*(step + 1)/steps_per_epoch,
                                                      prefix=prefix, entropy=entropy,
                                                      accuracy=accuracy_)
                            elif self._model_type == 'sequence':
                                self._progress.render(percent=100.0*(step + 1)/steps_per_epoch,
                                                      prefix=prefix, entropy=entropy,
                                                      error_rate=accuracy_)

                            if 'train' in self._functions:
                                # @NOTE: we will use function `train` to estimate
                                # does we need to further performing training or not

                                if self._functions['train'](*result) is True:
                                    break
                        else:
                            raise AssertionError('result from `feed_dict` '
                                                 'mustn\'t be None but not')

                # @NOTE: finish training an epoch, perform validation test
                if len(phrases['valid']) < 3:
                    return False
                else:
                    keep = True
                    self._provider.use_dataset('valid')

                    if validate_steps_per_epoch is None:
                        validate_steps_per_epoch = steps_per_epoch

                    if validate_batch_size is None:
                        validate_batch_size = batch_size

                    validate_step = global_step - steps_per_epoch

                    for step in range(validate_steps_per_epoch):
                        batch, inputs_length = self.prepare_new_batch(generator, batch_size)

                        # @NOTE: build our feed_dict
                        for index, gate in enumerate(list_input_gates):
                            feed_dict_[list_input_length_placeholders[index]] = inputs_length[gate]
                        else:
                            feed_dict_.update(self.feed_to_model('valid', feed_dict, batch=batch))

                        validate_step += 1
                        if not feed_dict_ is None:
                            prefix = 'validate epoch %d' % (epoch_index + 1)
                            result = trainer.run(phrases['valid'], feed_dict=feed_dict_)
                            summary, accuracy_, _, entropy = result[0:4]

                        if not writer is None:
                            validating_writer.add_summary(summary, validate_step)
                            validating_writer.flush()

                            if accuracy is None or len(accuracy) == 0:
                                self._progress.render(percent=100.0*(step + 1)/steps_per_epoch,
                                                      prefix=prefix, entropy=entropy)
                            elif self._model_type in ['classification', 'regression']:
                                self._progress.render(percent=100.0*(step + 1)/steps_per_epoch,
                                                      prefix=prefix, entropy=entropy,
                                                      accuracy=accuracy_)
                            elif self._model_type == 'sequence':
                                self._progress.render(percent=100.0*(step + 1)/steps_per_epoch,
                                                      prefix=prefix, entropy=entropy,
                                                      error_rate=accuracy_)

                            if 'validate' in self._functions:
                                # @NOTE: we will use function `validate` to estimate
                                # does we need to further performing training or not.
                                # This is the way to adapt EarlyStop to prevent overfiting

                                if self._functions['validate'](*result) is True:
                                    keep = False
                                    break

                    # @NOTE: run verification from visuallizing callbacks
                    if self._summary.does_need_to_invoke('verify'):
                        if self._model_type == 'sequence' and len(decoders) > 0:
                            ypreds = trainer.run(decoders, feed_dict=feed_dict_)
                        else:
                            ypreds = trainer.run(self._summary._predicted_indices,
                                                 feed_dict=feed_dict_)

                        ytruths = []
                        for layer in self._optimizer.layers:
                            # @NOTE: load ytruths base on ground_truth of entropies

                            if isinstance(layer, Objective):
                                if not layer.ground_truth is None:
                                    ground_truth = feed_dict_[layer.ground_truth.outputs[0]]

                                    ytruths.append(ground_truth)
                                    self._summary('scale', writer=validating_writer,
                                                  objective=layer,
                                                  ypreds=ypreds,
                                                  ytruths=ground_truth,
                                                  global_step=validate_step)
                        else:
                            if len(ytruths) == 0:
                                # @NOTE: sometimes, we use a single entropy and earn an empty ytruths
                                # this for-loop will solve this problem

                                for layer in self._optimizer.layers:
                                    if is_input_layer(layer):
                                        ytruths.append(feed_dict_[layer.outputs[0]])

                            # @NOTE: load verify object if it's defined
                            self._summary('verify', writer=training_writer,
                                          ypreds=ypreds,
                                          ytruths=ytruths,
                                          global_step=global_step)
                            validating_writer.flush()

                    epoch_index += 1
                    if keep is False:
                        break
            else:
                self._checkpoint = checkpoint
                self._seq_length = None
                return True
            return False

    def freeze(self, output, export_names={}, logits=None, as_text=False):
        # @NOTE: update logits if it needs

        Backend.reset(back_to_default=True, debug=self._summary.debug)
        if self._logits is None:
            if not logits is None and isinstance(logits, Logits):
                self.logits = logits
            else:
                raise AssertionError('Please install a logits before doing '
                                     'anything')

        # @NOTE: unimplement logits' layers
        for layer in self._logits.layers:
            layer.unimplement()

        # @NOTE: build header using generators
        header = None
        for generator in self._provider.generators:
            implemented = generator.freeze(header)

            if not implemented is None:
                header = implemented

        # @NOTE: build body
        if self._functions.get('evaluation') is None:
            raise AssertionError('please provide function `evaluation()`')
        elif header is None:
            body = self._functions['evaluation'](is_training=False)
        else:
            body = self._functions['evaluation'](use=header, is_training=False)

        # @NOTE: build footer
        output_gates = []
        if not self._optimizer is None:
            for output_tensor in body:
                output_name = Backend.name(output_tensor, shorten=True)

                # @NOTE: since some Layer would produce output inside a scope
                # with the scope name is Layer's name, we must take this now
                output_name = output_name.split('/')[0]

                # @NOTE: search entirely
                for layer in self._optimizer.layers:
                    if isinstance(layer, Objective) is True:
                        if layer.ground_truth is None:
                            continue
                        elif layer.ground_truth.name == output_name:
                            export_name = export_names.get(output_name) or output_name

                            # @NOTE: freeze output_tensor
                            output_gates.append(layer.freeze(output_tensor, export_name))
                            break
            else:
                total_entropy = self._optimizer.entropy

                if len(output_gates) == 0 and not total_entropy.ground_truth is None:
                    for output_tensor in body:
                        output_name = Backend.name(output_tensor, shorten=True)

                        # @NOTE: since some Layer would produce output inside a scope
                        # with the scope name is Layer's name, we must take this now
                        output_name = output_name.split('/')[0]

                        if output_name == total_entropy.ground_truth.name:
                            export_name = export_names.get(output_name) or output_name

                            # @NOTE: freeze output_tensor
                            output_gates.append(total_entropy.freeze(output_tensor, export_name))
                            break
        else:
            output_gates = body

        # @NOTE: restore checkpoint and save back everything to a graph_def
        if Backend.restore(self._checkpoint):
            return Backend.write_graph(output, output_gates, as_text=as_text)
        else:
            return False

    def evaluate(self, receiver, provider=None, artifact=None):
        Backend.reset(back_to_default=True)

        if callable(receiver) is False:
            raise AssertionError('`receiver` must be callable')

        # @NOTE: load provider if it needs
        if provider is None:
            if self._provider is None:
                raise AssertionError('`provider` mustn\'t be None')
            else:
                provider = self._provider

        use_artifact = not artifact is None

        # @NOTE: if we don't use artifact as the graph_def, we must load our
        # own graph from the check checkpoint we have and reconfigure
        # everything
        if not use_artifact:
            if self._functions.get('evaluation') is None:
                raise AssertionError('`artifact` mustn\'t be None')
            else:
                self._functions['evaluation'](batch_size=-1)

        # @NOTE: restore computing graph with the artifact or the checkpoint,
        # bases on input parameters
        Backend.restore(artifact or self._checkpoint, is_artifact=use_artifact)

        if use_artifact:
            evaluater = Backend.find(type='input')

        # @NOTE: loop through each input from provider and calculate result from
        # them, we will use a callback `receiver` to watch the result of the
        # whole progress
        generator = provider.generate()
        keep = True

        while keep:
            batch = self.prepare_new_batch(generator, batch_size=1)

            if use_artifact:
                feed_dict_ = {}

                for index, placeholder in enumerate(evaluater):
                    feed_dict_[placeholder] = batch[index]
            else:
                feed_dict_ = self.feed_to_model('test', feed_dict, batch=batch)

            keep = receiver(Backend.evaluate(fetcher=evaluater,
                                             feed_dict=feed_dict_),
                            batch)

    def finetune(self, configures, root=None):
        import os

        for configure in configures:
            if not 'action' in configure or configure['action'] in ['fit', 'modify']:
                configure['checkpoint'] = '%s/%s' % (root or os.getcwd(),
                                                     self._name or self._logits.name)
                if 'action' in configure and configure['action'] == 'modify':
                    if 'logits' in configure:
                        if isinstance(configure['logits'], dict):
                            configure_to_layers(configure['logits'],
                                                context=self._logits)
                        elif isinstance(configure['logits'], list):
                            structure_to_layers(configure['logits'],
                                                input=self._logits.outputs,
                                                context=self._logits)

                    if 'loss_function' in configure:
                        if isinstance(configure['loss_function'], dict):
                            self._optimizer.entropy = (None, configure['loss_function'], None)
                        elif isinstance(configure['loss_function'], list):
                            self._optimizer.entropy = (None, None, configure['loss_function'])

                # @NOTE: reset summary
                if not self._summary is None:
                    self._summary.reset()

                # @NOTE: reset layers of logits
                for layer in self.logits.layers:
                    layer.reset()

                # @NOTE: reset layers of optimizer
                for layer in self.optimizer.layers:
                    layer.reset()

                # @NOTE: print title
                if 'title' in configure:
                    print(configure['title'])

                # @NOTE: reset everything to default graph
                Backend.reset(back_to_default=True)

                if self.fit(**configure) is False:
                    return False
        else:
            return True
