from .backend import Backend


class Objective(object):
    def __init__(self, clsname=None, ground_truth=None, name=None, **kwargs):
        super(Objective, self).__init__()
        self._trainable = kwargs.get('trainable') or True
        self._ground_truth = ground_truth
        self._name = name
        self._debug = None
        self._clsname = clsname
        self._decoder = None
        self._context = None
        self._switcher = None
        self._estimator = None
        self._outputs = []

        # @NOTE: define previous and next layers
        self._next = []

        if ground_truth is None or isinstance(ground_truth, str) is True:
            self._previous = []
        else:
            self._previous = [ground_truth]

        if not clsname is None:
            index = 'entropies.%s' % clsname

            if Backend.get(index) is None:
                Backend.set(index, self.__class__)

    def __call__(self, ypreds):
        raise AssertionError('this is a virtual class')

    def estimate(self, ypreds, ground_truth):
        raise AssertionError('this is a virtual class')

    def switch(self, mode):
        pass

    def freeze(self, tensor, freeze_name):
        return tensor

    @property
    def decoder(self):
        return self._decoder

    @decoder.setter
    def decoder(self, value):
        self._decoder = value

    @property
    def estimator(self):
        return self._estimator

    @estimator.setter
    def estimator(self, value):
        self._estimator = value

    @property
    def name(self):
        return self._name

    @property
    def ground_truth(self):
        return self._ground_truth

    @property
    def previous(self):
        return self._previous

    @property
    def next(self):
        return self._next

    @property
    def outputs(self):
        return self._outputs

    @property
    def is_mounted(self):
        return self._context != None

    @property
    def is_implemented(self):
        return len(self._outputs) > 0 or (not self._trainable)

    @property
    def trainable(self):
        return self._trainable

    @property
    def switcher(self):
        return self._switcher

    @outputs.setter
    def outputs(self, value):
        self._outputs = value

    @ground_truth.setter
    def ground_truth(self, value):
        self._ground_truth = value

    @trainable.setter
    def trainable(self, value):
        self._trainable = value

    def mount(self, context):
        self._context = context

        if self._name is None:
            names = [layer.name for layer in context.layers]
            added = self._clsname is None
            index = 0

            # @NOTE: entropies don't have their own name so we must
            # generate their name base on index and its class name
            while not added and self._name is None:
                abs_name = '%s_%d' % (self._clsname, index)

                if not abs_name in names:
                    if context.add(self) is False:
                        raise AssertionError('There was an error during adding '
                                             'entropy `%s`' % abs_name)
                    else:
                        self._name = abs_name
                        added = True
                else:
                    index += 1
            else:
                if not self._ground_truth is None:
                    if isinstance(self._ground_truth, str):
                        for layer in context.layers:
                            if layer.name == self._ground_truth:
                                if not layer in self._previous:
                                    self._previous.insert(0, layer)
                                self._ground_truth = layer
                                break
                        else:
                            raise AssertionError('not found ground_truth '
                                                 '`%s`' % self._ground_truth)
                    elif self._ground_truth.is_mounted is False:
                        self._ground_truth.mount(context)
        elif not self._name in self._context.layers:
            if context.add(self) is False:
                raise AssertionError('There was an error during adding '
                                     'layer `%s`' % self._name)

    def reset(self):
        self._switcher = None
        self._outputs = []

    def shape(self, index=0):
        if index == 0:
            return ()
        else:
            raise AssertionError('Objective only support single output')

    def implement(self, ypreds, batch_size=-1):
        self._outputs = []

        if isinstance(self._ground_truth, str):
            for layer in self._context.layers:
                if layer.name == self._ground_truth:
                    if not layer in self._previous:
                        self._previous.append(layer)
                    self._ground_truth = layer
                    break
            else:
                raise AssertionError('not found ground_truth '
                                     '`%s`' % self._ground_truth)
        elif not self._ground_truth is None:
            if self._ground_truth.is_implemented is False:
                self._ground_truth.implement(is_training=True, batch_size=batch_size)

        if self._trainable:
            loss = self.__call__(ypreds)

            if isinstance(loss, tuple) or isinstance(loss, list):
                self._outputs = list(loss)
            else:
                self._outputs = [loss]
        else:
            self._outputs = []

        return self._outputs[0] if len(self._outputs) > 0 else None

    def feed(self, feed_dict, expects, **kwargs):
        if feed_dict is None:
            raise AssertionError('`feed_dict` must\'t be None')
        if not isinstance(feed_dict, dict):
            raise AssertionError('`feed_dict` must be a dict')

        if isinstance(self._ground_truth, Layer):
            feed_dict[self._ground_truth.outputs[0]] = expects
        elif isinstance(self._ground_truth, list) or isinstance(self._ground_truth, tuple):
            if not (isinstance(expects, list) or isinstance(expects, tuple)):
                raise AssertionError('`expects` must be a list or tuple')
            elif len(expects) != len(self._ground_truth):
                raise AssertionError('len(`expects`) must be %d' % len(self._ground_truth))

            for index, expect in enumerate(expects):
                feed_dict[self._ground_truth.outputs[index]] = expect
        else:
            feed_dict[self._ground_truth] = expects
        return feed_dict

    def inhearit_from(self, layers, context):
        if isinstance(layers, list):
            self._previous.extend(layers)
        else:
            self._previous.append(layers)
        self.on_link_to_previous(layers, context)

    def next_to(self, layer):
        self._next.append(layer)
        self.on_link_to_next(layer)

    def on_link_to_next(self, layer):
        pass

    def on_link_to_previous(self, layers, context):
        pass

    @staticmethod
    def create(clsname, name=None, **kwargs):
        clss = Backend.get('entropies.%s' % clsname)

        if not clss is None:
            entropy = clss.__new__(clss)

            try:
                entropy.__init__(name=name, **kwargs)
                return entropy
            except Exception as error:
                raise error
        else:
            return None


class ObjectiveAdapter(Objective):
    def __init__(self, definition, sorting):
        super(ObjectiveAdapter, self).__init__(clsname=None)
        self._definition = definition
        self._sorting = sorting

    def implement(self, ypreds, batch_size=-1):
        from .utils import is_input_layer

        on_implementing = []
        on_waiting = []
        index = 0
        end = None

        for layer in self._definition:
            layer.outputs = []

        # @NOTE: find input layers and add them to list 'on_waiting'
        for layer in self._definition:
            if is_input_layer(layer):
                layer.only_for_optimize = True
                on_waiting.append(layer)
            elif len(layer.previous) == 0:
                on_waiting.append(layer)
        else:
            if self._sorting:
                def key(input):
                    return input.gate if is_input_layer(input) else -1

                on_waiting.sort(key=key)

        # @NOTE: implement our definition to final cross_entropy
        while len(on_waiting) > 0:
            on_waiting, on_implementing = on_implementing, on_waiting

            # @NOTE: we must check again to make sure that layer on implementing
            # state, their preceded layers are implemented
            need_to_wait_more = []

            for index_layer, layer in enumerate(on_implementing):
                for preceder in layer.previous:
                    if preceder.is_implemented is False:
                        need_to_wait_more.append(index_layer)
                        break
            else:
                need_to_wait_more.sort()

                # @NOTE: remove layer on list `need_to_wait_more` since it have
                # some layer under-implementing
                for i, index_layer in enumerate(need_to_wait_more):
                    on_waiting.append(on_implementing[index_layer - i])
                    del on_implementing[index_layer - i]

            # @NOTE: we will invoke each layer from `on_implementing` list
            # and do implementation with them
            for layer in on_implementing:
                if isinstance(layer, Objective):
                    if layer.ground_truth is None:
                        error = layer.implement(ypreds=None)
                    else:
                        error = layer.implement(ypreds=ypreds[index], batch_size=batch_size)
                        index += 1
                else:
                    error = layer.implement(is_training=True, batch_size=batch_size)

                if isinstance(layer, Objective) or (error is None) \
                        or (error == 0) or (error is True):
                    # @NOTE: since we finish implement a new layer, we must
                    # add its next layer to 'on_waiting' list from now on

                    if layer.is_implemented is False:
                        raise AssertionError('layer %s has done but we can\'t '
                                             'see any outputs' % layer.name)
                    else:
                        for next in layer.next:
                            if not next in on_waiting:
                                on_waiting.append(next)
                        else:
                            if len(layer.next) == 0 and len(layer.outputs) > 0:
                                end = layer
                elif isinstance(error, str) and len(error) > 0:
                    raise AssertionError(error)
                elif isinstance(error, bool) and error == False:
                    raise AssertionError('there was an error during '
                                         'implementing layer `%s`' % layer.name)
                elif isinstance(error, int) and error != 0:
                    raise AssertionError('there was an error during '
                                         'implementing layer `%s`' % layer.name)
                else:
                    raise error
            else:
                on_implementing = []
        else:
            return end.outputs[0]
