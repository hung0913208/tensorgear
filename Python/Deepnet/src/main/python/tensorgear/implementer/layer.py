from .backend import Backend

class Layer(object):
    def __init__(self, clsname, name=None, **kwarg):
        super(Layer, self).__init__()

        # @NOTE: these parameters define how to build a layer
        self._clsname = clsname
        self._name = name
        self._context = None
        self._debug = None
        self._previous = []
        self._next = []
        self._weights = []
        self._outputs = []

        # @NOTE: these parameters define how to adapt this layer in specific
        # situations
        self._trainable = kwarg.get('trainable') or True
        self._batch_size = None

        if not clsname is None:
            index = 'layers.%s' % clsname

            if Backend.get(index) is None:
                Backend.set(index, self.__class__)

    def __call__(self, *layers):
        for index, layer in enumerate(layers):
            if isinstance(layer, Layer) is False:
                raise AssertionError('the parameter %d must '
                                     'be a Layer object' % index)
        else:
            for layer in layers:
                if not self in layer.next:
                    layer.next.append(self)
            else:
                self._previous.extend(layers)
                return self

    @staticmethod
    def create(layer, name=None, **kwargs):
        clss = Backend.get('layers.%s' % layer)

        if not clss is None:
            layer = clss.__new__(clss)

            try:
                layer.__init__(name=name, **kwargs)
                return layer
            except Exception as error:
                raise error
        else:
            return None

    @property
    def trainable(self):
        return self._trainable

    @trainable.setter
    def trainable(self, value):
        self._trainable = value

    @property
    def weights(self):
        return self._weights

    @property
    def batch_size(self):
        return self._batch_size

    @batch_size.setter
    def batch_size(self, value):
        self._batch_size = value

    @property
    def name(self):
        return self._name

    @property
    def previous(self):
        return self._previous

    @property
    def next(self):
        return self._next

    @property
    def outputs(self):
        return self._outputs

    @property
    def is_mounted(self):
        return not self._context is None

    @property
    def is_implemented(self):
        if not Backend.get('macros') is None:
            for type in Backend.get('macros'):
                if isinstance(self, type):
                    return True
        return len(self._outputs) > 0

    @previous.setter
    def previous(self, value):
        self._previous = value

    @outputs.setter
    def outputs(self, value):
        self._outputs = value

    @next.setter
    def next(self, value):
        self._next = value

    def switch(self, mode):
        pass

    def unimplement(self):
        self._outputs = []
        self._weights = []

    def shape(self, index=None, tensor=None, dims=None, runtime=False):
        # @NOTE: if we have implemented this layer, we can use it instead 
        # of recalculating again and again
        
        if not tensor is None:
            result = Backend.shape(tensor=tensor, runtime=runtime)
        elif not index is None and len(self._outputs) > index:
            result = Backend.shape(self._outputs[index], runtime=runtime)
        else:
            result = self.__shape__(index=index, runtime=runtime)

        # @NOTE: since we can use dims to define the safe way to get specific
        # dim from the shape 
        if dims is None:
            return result
        elif isinstance(result, Backend.type('tensor')):
            def wrapping(dim):
                Backend.implement('slice', tensor=result, begin=[dim], size=[1])

            if isinstance(result, Backend.type('tensor')):
                return [wrapping(dim) for dim in dims]
            else:
                return [result[dim] for dim in dims]
        else:
            return [result[dim] for dim in dims]

    def __shape__(self, index=None, tensor=None, runtime=False):
        raise AssertionError('This is a virtual class')

    def mount(self, context):
        self._context = context

        if self._name is None:
            names = [layer.name for layer in context.layers]
            index = 0

            # @NOTE: entropies don't have their own name so we must
            # generate their name base on index and its class name
            while self._name is None:
                abs_name = '%s_%d' % (self._clsname, index)

                if not abs_name in names:
                    if context.add(self) is False:
                        raise AssertionError('There was an error during adding '
                                             'entropy `%s`' % abs_name)
                    else:
                        self._name = abs_name
                else:
                    index += 1
        elif not self._name in self._context.layers:
            if context.add(self) is False:
                raise AssertionError('There was an error during adding '
                                     'layer `%s`' % self._name)

    def reset(self):
        self._weights = []
        self._outputs = []

    def inhearit_from(self, layers, context):
        if isinstance(layers, list):
            self._previous.extend(layers)
        else:
            self._previous.append(layers)
        self.on_link_to_previous(layers, context)

    def next_to(self, layer):
        self._next.append(layer)
        self.on_link_to_next(layer)

    def on_link_to_next(self, layer):
        pass

    def on_link_to_previous(self, layers, context):
        pass

    def implement(self, batch_size=None):
        raise AssertionError('this is a virtual class')
