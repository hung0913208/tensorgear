from ..generator import Generator

class Decode(Generator):
    def __init__(self, decoders=[], name=None, iterator=None):
        super(Decode, self).__init__(name, iterator)
        self._decoders = decoders
        self._resource = None

    def fetch(self):
        result = []

        if len(self._resource) != len(self._decoders):
            raise AssertionError('resource doesn\'t match size with decoders')

        for index, decoder in enumerate(self._decoders):
            if decoder is None:
                result.append(self._resource[index])
            elif isinstance(decoder, list):
                for callback in decoder:
                    result.append(callback(self._resource[index]))
            elif callable(decoder):
                result.append(decoder(self._resource[index]))
        else:
            self._resource = None
            return tuple(result)

    def end_of_iterator(self):
        return self._resource is None

    def config(self, resource):
        if isinstance(resource, tuple) or isinstance(resource, list):
            self._resource = resource
        else:
            self._resource = [resource]
