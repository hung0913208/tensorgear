from ..generator import Generator

class Compose(Generator):
    def __init__(self, name, iterator=None):
        super(Compose, self).__init__(name, iterator)

    def compose(self):
        raise AssertionError('This is a virtual class')

    def fetch(self):
        while self._iterator is None or self._iterator.is_safe_to_generate_data:
            return self.compose()
        else:
            raise AssertionError('Compose is dead when self._iterator is ended')

    def end_of_iterator(self):
        return not self._iterator.is_safe_to_generate_data

    def config(self, resource):
        return self
