from ..utils import safe
from .generator import Generator
from .generators import Compose

class Iterator(object):
    def __init__(self, generators=[]):
        super(Iterator, self).__init__()
        self._keep_going = True
        self._generators = []

        for generator in generators:
            self.append(generator)

    def __enter__(self):
        return self

    def __exit__(self, type, value, tb):
        def update():
            self._keep_going = False
        safe(update)

    @property
    def generators(self):
        return self._generators

    def append(self, generator):
        if isinstance(generator, Generator) is False:
            raise AssertionError('Only append a Generator')
        else:
            if generator.iterator is None:
                generator.iterator = self
            self._generators.append(generator)

    def use_dataset(self, name):
        def update():
            for generator in self._generators:
                generator.use_dataset(name)
        safe(update)

    @property
    def is_safe_to_generate_data(self):
        return safe(self._keep_going)

    def generate(self):
        while self.is_safe_to_generate_data:
            if len(self._generators) > 0:
                is_okey_to_continue = True
                iterators = []
                result = self

                # @NOTE: init our interators
                for idx, generator in enumerate(self._generators):
                    iterator = generator(result)

                    if iterator is None:
                        raise AssertionError('can\'t generate to an interator '
                                         'with %s' % str(generator))
                    elif idx + 1 < len(self._generators):
                        iterators.append(iterator)

                    try:
                        result = next(iterator)
                        if result is None:
                            raise AssertionError('result from iterator must not be None')
                    except (StopIteration, RuntimeError) as error:
                        raise AssertionError('can\'t collect data from generator')
                else:
                    yield result

                # @NOTE: loop throught all iterators from begin to end
                while is_okey_to_continue:
                    try:
                        # @NOTE: when we set flag self._keep_going is false, we must
                        # abandon everything from here now

                        if self.is_safe_to_generate_data is False:
                            raise StopIteration
                        yield next(iterator)
                    except (StopIteration, RuntimeError) as error:
                        provider_result = None

                        for reversed_begin, provider in enumerate(reversed(iterators)):
                            go_deeper = False
                            keep = True

                            if is_okey_to_continue is False:
                                break

                            while keep:
                                # @NOTE: try to fetch a record from provider

                                try:
                                    provider_result = next(provider)
                                except (StopIteration, RuntimeError) as error:
                                    # @NOTE: if it fails, we must go deeper, in
                                    # some case when we go too far, we will
                                    # collide the lastest one, take done
                                    # everything at this time

                                    if reversed_begin + 1 == len(iterators):
                                        is_okey_to_continue = False
                                    else:
                                        go_deeper = True
                                finally:
                                    keep = False
                            else:
                                # @NOTE: decide where to go next: take deeper,
                                # abandon or recovery our iterators base on the
                                # record we have earned

                                if is_okey_to_continue is False:
                                    break
                                elif go_deeper is False:
                                    # @NOTE: when we fetch a record completedly, we
                                    # must recreate new iterators base on this
                                    # record

                                    for reversed_idx in range(reversed_begin - 1, -1, -1):
                                        try:
                                            idx = len(iterators) - 1 - reversed_idx
                                            iterators[idx] = self._generators[idx](provider_result)
                                            provider_result = next(iterators[idx])
                                        except (StopIteration, RuntimeError) as error:
                                            is_okey_to_continue = False
                                            break
                                    else:
                                        iterator = self._generators[len(iterators)](provider_result)
                                        break
            else:
                raise AssertionError('don\'t have any generators to be used')
