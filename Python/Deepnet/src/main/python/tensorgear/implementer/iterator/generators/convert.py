from ..generator import Generator

class Convert(Generator):
    def __init__(self, name, clsname=None, iterator=None):
        super(Convert, self).__init__(name, clsname, iterator)
        self._resource = None

    def convert(self, resource):
        raise AssertionError('This is a virtual class')

    def fetch(self):
        if not self._resource is None:
            result = self.convert(self._resource)
            self._resource = None
        else:
            raise AssertionError('Please provide data to `Convert`')

        return result

    def end_of_iterator(self):
        return self._resource is None

    def config(self, resource):
        self._resource = resource
