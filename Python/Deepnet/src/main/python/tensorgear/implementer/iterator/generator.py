class Generator(object):
    def __init__(self, name, clsname=None, iterator=None):
        super(Generator, self).__init__()
        self._iterator = iterator
        self._clsname = clsname
        self._name = name

    @property
    def clsname(self):
        return self._clsname

    @property
    def name(self):
        return self._name

    @property
    def iterator(self):
        return self._iterator

    @iterator.setter
    def iterator(self, value):
        self._iterator = value

    def freeze(self, previous):
        return None

    def use_dataset(self, name):
        pass

    def fetch(self):
        raise AssertionError('this is a virtual class')

    def end_of_iterator(self):
        raise AssertionError('this is a virtual class')

    def config(self, resouce):
        raise AssertionError('this is a virtual class')

    def __call__(self, resource):
        self.config(resource)

        while self.end_of_iterator() is False:
            yield self.fetch()
        else:
            raise StopIteration




