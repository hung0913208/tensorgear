from ..generator import Generator

class Combine(Generator):
    def __init__(self, name, iterator=None):
        super(Combine, self).__init__(name, iterator)
        self._iterators = []
        self._generators = []

    def group_with(self, generator, provider):
        if provider is None or generator is None:
            return False
        elif isinstance(generator, Generator):
            if not generator in self._generators:
                self._generators.append((generator, provider))
            return True
        else:
            return False

    def fetch(self):
        if len(self._generators) > 0:
            result = []

            for index in len(self._iterators):
                if self._generators[index].end_of_iterator() is True:
                    generator, provider = self._generators[index]

                    if not provider is None:
                        self._iterators[index] = generator(provider())
                    else:
                        raise AssertionError('provider must not be None')
                result.append(next(self._iterators[index]))
            else:
                return result
        else:
            raise AssertionError('You must do adding a generator before doing '
                                 'anything')

    def end_of_iterator(self):
        result = False

        for index, (generator, provider) in enumerate(self._generators):
            if generator.end_of_iterator() is True:
                if not provider is None:
                    self._iterators[index] = generator(provider())
                else:
                    result = True
        else:
            return result

    def config(self, resource):
        raise AssertionError('Combine don\'t use legacy method to replenish itself')
