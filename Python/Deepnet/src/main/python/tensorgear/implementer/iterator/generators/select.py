from ..generator import Generator

class Select(Generator):
    def __init__(self, name, iterator=None):
        super(Select, self).__init__(name, iterator)
        self._iterators = []
        self._generators = []
        self._index = 0

    def group_with(self, generator, provider=None):
        if generator is None:
            return False
        elif isinstance(generator, Generator):
            if not generator in self._generators:
                self._generators.append((generator, provider))
            return True
        else:
            return False

    def fetch(self):
        if len(self._generators) > 0:
            for _ in range(len(self._generators)):
                # @NOTE: when the loop iterator reach the end of our generators
                # we must back to the first
                if self._index >= len(self._generators):
                    self._index = 0
                else:
                    self._index += 1

                # @NOTE: select our generator
                generator, provider = self._generators[self._index]
                if generator.end_of_iteartor() is False:
                    return self._iterators[self._index]
                elif not reloader is None:
                    self._iterators[self._index] = generator(provider())
            else:
                raise AssertionError('It seems your Iterator can\'t recovery '
                                     'my Selector')
        else:
            raise AssertionError('You must do adding a generator before doing '
                                 'anything')

    def end_of_iterator(self):
        result = True

        for index, (generator, provider) in enumerate(self._generators):
            if not provider is None:
                if generator.end_of_iteartor() is True:
                    self._iterators[index] = generator(provider())
                result = False
            elif generator.end_of_iterator() is False:
                return False
        else:
            return result

    def config(self, resources):
        for index, generator in enumerate(self._generators):
            if isinstance(generator, str):
                # @NOTE: convert name of generator to the generator object

                for target in self._iterator.generators:
                    if target.name == generator:
                        self._generators[index] = target
                        break

        if isinstance(resources, list) or isinstance(resources, tuple):
            if len(resources) != len(self._generators):
                raise AssertionError('len(resounce) must equal to %d' % len(self._generators))
            else:
                # @NOTE: usually we never use this method to replenish our
                # Selector since it will break our randomize structure

                for index, (generator, provider) in enumerate(self._generators):
                    if provider is None:
                        self._iterators[index] = generator(resources[index])
                    else:
                        raise AssertionError('Unsupport mixing the auto-replenish method '
                                             'with legacy method')
        else:
            raise AssertionError('Only support list or tuple')
        return self
