from ..optimizer import Optimizer
from ..backend import *
from ..layers import Input


class RMSProp(Optimizer):
    def __init__(self, decay=0.9, momentum=0.0, epsilon=1e-10, **kwargs):
        super(RMSProp, self).__init__(clsname='rmsprop', **kwargs)
        self._decay = decay
        self._momentum = momentum 
        self._epsilon = epsilon

    def implement(self, learning_rate=None):
        if learning_rate is None:
            if self._learning_rate is None:
                raise AssertionError('`learning_rate` mustn\'t be None')
        elif isinstance(self._learning_rate, Backend.type('tensor')):
            learning_rate = self._learning_rate
        elif isinstance(self._learning_rate, Input):
            if len(self._learning_rate.outputs) > 0:
                learning_rate = self._learning_rate.outputs[0]
        else:
            raise AssertionError('`learning_rate` must be implemented first')

        return Backend.implement('rmsprop', learning_rate=learning_rate, 
                                 decay=self._decay, momentum=self._momentum, 
                                 epsilon=self._epsilon)
