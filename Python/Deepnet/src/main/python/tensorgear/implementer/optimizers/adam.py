from ..optimizer import Optimizer
from ..backend import *
from ..layers import Input

class Adam(Optimizer):
    def __init__(self, beta1=0.9, beta2=0.999, epsilon=1e-08, **kwargs):
        super(Adam, self).__init__(clsname='adam', **kwargs)
        self._beta1 = beta1
        self._beta2 = beta2
        self._epsilon = epsilon

    def implement(self, learning_rate=None):
        if learning_rate is None:
            # @NOTE: verify learning rate before doing anything

            if self._learning_rate is None:
                raise AssertionError('`learning_rate` mustn\'t be None')
            elif isinstance(self._learning_rate, Backend.type('tensor')):
                learning_rate = self._learning_rate
            elif isinstance(self._learning_rate, Input):
                learning_rate = self._learning_rate
            else:
                raise AssertionError('learning_rate must be a value or tensor')

        return Backend.implement('adam', learning_rate=learning_rate, 
                                 beta1=self._beta1, beta2=self._beta2,
                                 epsilon=self._epsilon)
