from ..optimizer import Optimizer
from ..backend import *
from ..layers import Input

class Adagrad(Optimizer):
    def __init__(self, initial_accumulator_value=0.1, **kwargs):
        super(Adagrad, self).__init__(clsname='adagrad', **kwargs)
        self._initial_accumulator = initial_accumulator_value

    def implement(self, learning_rate=None):
        if learning_rate is None:
            # @NOTE: verify learning rate before doing anything

            if self._learning_rate is None:
                raise AssertionError('`learning_rate` mustn\'t be None')
            elif isinstance(self._learning_rate, Backend.type('tensor')):
                learning_rate = self._learning_rate
            elif isinstance(self._learning_rate, Input):
                learning_rate = self._learning_rate
            else:
                raise AssertionError('learning_rate must be a value or tensor')

        return Backend.implement('adagrad', learning_rate=learning_rate,
                                 initial_accumulator=self._initial_accumulator)
