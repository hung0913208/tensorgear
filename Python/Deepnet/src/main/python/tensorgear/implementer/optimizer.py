from .parser import structure_to_layers, configure_to_layers
from .utils import is_input_layer
from .objective import ObjectiveAdapter, Objective
from .layer import Layer
from .backend import Backend

import numpy as np


class Optimizer(object):
    def __init__(self, clsname, entropy=None, learning_rate=None,
                 configure=None, structure=None, mount=None, **kwargs):
        super(Optimizer, self).__init__()

        if not learning_rate is None:
            if is_input_layer(learning_rate):
                learning_rate.mount(self)

            self._learning_rate = learning_rate
        else:
            self._learning_rate = None

        # @NOTE: define our parameters
        self._entropy = entropy
        self._logits = None
        self._inputs = []
        self._layers = []

        # @NOTE: register this class as a template if it needs
        if not clsname is None:
            index = 'optimizers.%s' % clsname

            if Backend.get(index) is None:
                Backend.set(index, self.__class__)

        # @NOTE: customize our entropy function
        if isinstance(mount, list):
            # @NOTE: when mount is implemented we will use it as connected 
            # layers and wrapping around with ObjectiveAdapter

            if entropy is None:
                self._entropy = ObjectiveAdapter(definition=self._layers,
                                               sorting=(structure is None))
                self._entropy.mount(self)
            else:
                # @NOTE: since we connected layers before mounting to optimizer
                # we must configure everything manually
 
                for layer in mount:
                    if not layer in self._layers:
                        layer.mount(self)

                    if is_input_layer(layer) is True:
                        layer.only_for_optimize = True
        elif entropy is None:
            # @NOTE: convert to neural network definition before doing
            # anything

            if not configure is None:
                configure_to_layers(configure, context=self)
            elif not structure is None:
                structure_to_layers(structure, input=None, context=self)

            # @NOTE: okey, we got the entropy's definition, now we must
            # use `ObjectiveAdapter` to wrap around this definition
            if len(self._layers) > 0:
                self._entropy = ObjectiveAdapter(definition=self._layers,
                                               sorting=(structure is None))
                self._entropy.mount(self)
        else:
            if not entropy.ground_truth is None:
                if is_input_layer(entropy.ground_truth):
                    entropy.ground_truth.only_for_optimize = True
                    entropy.ground_truth.mount(self)

    @staticmethod
    def create(optimizer, loss_function=None, mount=None, **kwargs):
        clss = Backend.get('optimizers.%s' % optimizer)

        if not clss is None:
            optimizer = clss.__new__(clss)

            try:
                if mount is None:
                    if isinstance(loss_function, dict):
                        optimizer.__init__(configure=loss_function, **kwargs)
                    elif isinstance(loss_function, list):
                        optimizer.__init__(structure=loss_function, **kwargs)
                    else:
                        optimizer.__init__(entropy=loss_function, **kwargs)
                else:
                    optimizer.__init__(mount=mount, entropy=loss_function,
                                       **kwargs)
                return optimizer
            except Exception as error:
                 raise error
        else:
            return None

    @property
    def entropy(self):
        return self._entropy

    @entropy.setter
    def entropy(self, value):
        entropy, configure, structure = value

        if not entropy is None:
            self._entropy = value
        else:
            if not configure is None:
                configure_to_layers(configure, context=self)
            elif not structure is None:
                structure_to_layers(structure, input=None, context=self)

            # @NOTE: okey, we got the entropy's definition, now we must
            # use `ObjectiveAdapter` to wrap around this definition
            self._entropy = ObjectiveAdapter(definition=self._layers,
                                           sorting=(structure is None))
            self._entropy.mount(self)

    @property
    def learning_rate(self):
        return self._learning_rate

    @property
    def inputs(self):
        return self._inputs

    @property
    def logits(self):
        return self._logits

    @logits.setter
    def logits(self, value):
        current_gate = -1
        self._logits = value

        for layer in self._logits.layers:
            if is_input_layer(layer) and current_gate < layer.gate:
                current_gate = layer.gate
        else:
            current_gate += 1

            for layer in self._layers:
                if is_input_layer(layer) and layer.gate < current_gate:
                    is_okey_to_continue = True

                    # @NOTE: search if this gate has been assigned
                    while is_okey_to_continue:
                        for checker in self._layers:
                            if not is_input_layer(checker) or checker == layer:
                                continue
                            elif current_gate == checker.gate:
                                current_gate += 1
                                break
                        else:
                            is_okey_to_continue = False
                    else:
                        # @NOTE: okey apply this gate now

                        layer.gate = current_gate
                        current_gate += 1

    @property
    def layers(self):
        return self._layers

    def add(self, layer):
        if layer in self._layers:
            raise AssertionError('duplicate layer %s' % layer.name)
        else:
            if layer.is_mounted is True:
                if layer in self._layers:
                    raise AssertionError('layer %s is mounted' % layer.name)
                else:
                    self._layers.append(layer)
            elif layer.mount(self):
                self._layers.append(layer)
            else:
                return False
        return True

    def implement(self, learning_rate=None):
        raise AssertionError('this is a virtual class')

    def feed(self, mode, batch, learning_rate=None):
        if isinstance(self._learning_rate, Backend.type('tensor')):
            result = { self._learning_rate: learning_rate }
        else:
            result = {}

        # @NOTE: find inputs if it needs
        if len(self._inputs) == 0:
            # @NOTE: search inputs from the optimizer and logits

            for layer in (self.logits.layers + self.layers):
                if is_input_layer(layer):
                    self._inputs.append(layer)
                else:
                    implemented = layer.switch(mode)

                    if not implemented is None:
                        for placeholder in implemented:
                            result[placeholder] = implemented[placeholder]
            else:
                self._inputs.sort(key=lambda input: input.gate)

        # @NOTE: feed inputs with data from batch
        for input_layer in self._inputs:
            expected = input_layer.shape(0)
            index = input_layer.gate

            for i, dim in enumerate(expected):
                if dim is None:
                    expected[i] = -1
            else:
                if len(expected) > 1:
                    if len(expected) != len(batch[index].shape):
                        batch[index] = np.reshape(batch[index], expected)

            result[input_layer.outputs[0]] = batch[index]
        else:
            return result
