from .implementer import *

__objects__ = [
    Add,
    BatchNorm,
    Conv2D,
    Dense,
    Flatten,
    Input,
    Mult,
    Reshape,
    MaxPool2D,
    Dropout,
    ReduceSum,
    ReduceMean,
    Numberic,
    SpareSoftmax,
    CategoricalCrossEntropy,
    MeanSquare,
    CTCLoss,
    Regularizer,
    Adam,
    Adagrad,
    GradientDescent,
    RNN,
    LSTM,
    GRU,
    Pad
]

__backends__ = {
    'tensorflow': Tensorflow
}

def __build_instance__():
    for clss in __objects__:
        clss(name=None)

class Gear(object):
    def __init__(self, backend):
        super(Gear, self).__init__()
        Backend.use(__backends__[backend](), instance=__build_instance__)

    def __enter__(self):
        return self

    def __exit__(self, type, value, tb):
        Backend.done()

__all__ = ['Gear', 'Backend', 'Estimator', 'Logits', 'Optimizer', 'Objective',
           'Progress', 'Adam', 'Adagrad', 'GradientDescent', 'Iterator',
           'Generator', 'Combine', 'Select', 'Compose', 'Convert', 'Decode',
           'Add', 'BatchNorm', 'Conv2D', 'Dense', 'Flatten', 'Input', 'Mult',
           'RNN', 'GRU', 'LSTM', 'Reshape', 'Share', 'MaxPool2D', 'Dropout',
           'ReduceSum', 'ReduceMean', 'Pad', 'CategoricalCrossEntropy',
           'MeanSquare', 'Regularizer', 'CTCLoss', 'Pack', 'Replicate',
           'Numberic', 'SpareSoftmax', 'Tensorflow', 'confusion_matrix',
           'Histogram', 'to_categorical']
