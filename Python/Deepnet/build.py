from pybuilder.core import use_plugin, init

use_plugin("python.core")
use_plugin("python.unittest")
use_plugin("python.install_dependencies")
use_plugin("python.distutils")

default_task = "publish"

@init
def init(project):
    project.depends_on("six")
    project.depends_on("numpy")
    project.depends_on("scipy")
    project.depends_on('tensorflow')
    project.depends_on('tensorflow-plot')